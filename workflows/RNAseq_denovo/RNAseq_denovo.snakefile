{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def trimmomatic_inputs():
    return raw_reads

def fastqc_inputs():
    inputs = dict()
    if (config["SeOrPe"] == "SE"):
            inputs["reads"] = config["results_dir"]+"/"+config["trimmomatic_SE_output_dir"]+"/{sample}_trimmed.fq.gz"
        elif (config["SeOrPe"] == "PE"):
            inputs["read1"] = config["results_dir"]+"/"+config["trimmomatic_PE_output_dir"]+"/{sample}_forward_paired.fq.gz"
            inputs["read2"] = config["results_dir"]+"/"+config["trimmomatic_PE_output_dir"]+"/{sample}_reverse_unpaired.fq.gz"
        return inputs

def trinity_inputs():
    if (config["SeOrPe"] == "SE"):
            inputs["reads"] = config["results_dir"]+"/"+config["trimmomatic_SE_output_dir"]+"/{sample}_trimmed.fq.gz"
        elif (config["SeOrPe"] == "PE"):
            inputs["read1"] = config["results_dir"]+"/"+config["trimmomatic_PE_output_dir"]+"/{sample}_forward_paired.fq.gz"
            inputs["read2"] = config["results_dir"]+"/"+config["trimmomatic_PE_output_dir"]+"/{sample}_reverse_unpaired.fq.gz"
        return inputs

def transrate_inputs():
    inputs = dict()
    inputs["assembly"] = config["results_dir"] + "/" + config["trinity_output_dir"] + "/Trinity.fasta"
    return inputs

def transdecoder_inputs():
    inputs = dict()
    inputs["assembly"] = config["results_dir"] + "/" + config["trinity_output_dir"] + "/Trinity.fasta"
    return inputs

def interproscan_inputs():
    inputs = dict()
    inputs["pep"] = config["results_dir"] + "/" + config["transdecoder_output_dir"] + "/transcripts.fasta.transdecoder.pep",
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "trimming"):
        if (config[step] == "trimmomatic"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.trimmomatic_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.trimmomatic_SE.output,sample=SAMPLES)

    elif (step == "quality_check"):
        if (config[step] == "fastqc"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.fastqc_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.fastqc_SE.output,sample=SAMPLES)

    elif (step == "assembling"):
        if (config[step] == "trinity"):
            outputs = rules.trinity.output

    elif (step == "prot_domains_pred"):
        if (config[step] == "transdecoder"):
            outputs = rules.transdecoder.output

    elif (step == "funct_annot"):
        if (config[step] == "interproscan"):
            outputs = rules.interproscan.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}