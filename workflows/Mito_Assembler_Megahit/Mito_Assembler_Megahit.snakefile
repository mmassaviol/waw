{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def quality_check__fastqc_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def quality_check__fastqc_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def assembling__megahit_SE_inputs():
    inputs = dict()
    inputs["read"] = expand(raw_reads["read"],sample=SAMPLES)
    return inputs

def assembling__megahit_PE_inputs():
    inputs = dict()
    inputs["read"] = expand(raw_reads["read"],sample=SAMPLES)
    inputs["read2"] = expand(raw_reads["read2"],sample=SAMPLES)
    return inputs

def find_mitoscaf__mitoz_findmitoscaf_inputs():
    inputs = dict()
    if (config["SeOrPe"] == "PE"):
        inputs["fasta"] = rules.assembling__megahit_PE.output.contigs
    else:
        inputs["fasta"] = rules.assembling__megahit_SE.output.contigs
    return inputs

def annotate__mitoz_annotate_inputs():
    inputs = dict()
    inputs["fastafile"] = rules.find_mitoscaf__mitoz_findmitoscaf.output.mitogenome
    inputs["read"] = expand(raw_reads["read"],sample=SAMPLES)
    if (config["SeOrPe"] == "PE"):
        inputs["read2"] = expand(raw_reads["read2"],sample=SAMPLES)
    return inputs

def visualize__igv_visualize_inputs():
    inputs = dict()
    inputs["genome"] = rules.find_mitoscaf__mitoz_findmitoscaf.output.mitogenome
    if (config["SeOrPe"] == "PE"):
        inputs["circos_bam"] = rules.annotate__mitoz_annotate_PE.output.circos_bam
    else:
        inputs["circos_bam"] = rules.annotate__mitoz_annotate_SE.output.circos_bam
    return inputs


{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "quality_check"):
        if (config[step] == "fastqc"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.quality_check__fastqc_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.quality_check__fastqc_SE.output,sample=SAMPLES)

    if (step == "assembling"):
        if(config["SeOrPe"] == "PE"):
            outputs = rules.assembling__megahit_PE.output
        else:
            outputs = rules.assembling__megahit_SE.output
    
    if (step == "find_mitoscaf"):
        outputs = rules.find_mitoscaf__mitoz_findmitoscaf.output

    if (step == "annotate"):
        if(config["SeOrPe"] == "PE"):
            outputs = rules.annotate__mitoz_annotate_PE.output
        else:
            outputs = rules.annotate__mitoz_annotate_SE.output

    if (step == "visualize"):
        if(config["SeOrPe"] == "PE"):
            outputs = rules.visualize__igv_visualize.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}