{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

def get_individus():
    if (config["demultiplexing"] == "null"):
        return SAMPLES
    else:
        with open(config["demultiplexing__process_radtags_barcode_file"], mode="r") as infile:
            reader = csv.reader(infile,delimiter='\t')
            return [row[-1] for row in reader]

individus = get_individus()

# Tools inputs functions

def quality_check__fastqc_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def quality_check__fastqc_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def demultiplexing__process_radtags_inputs():
    inputs = dict()
    if (config["SeOrPe"] == "SE"):
        inputs["read"] = raw_reads['read']
    elif (config["SeOrPe"] == "PE"):
        inputs["read"] = raw_reads['read']
        inputs["read2"] = raw_reads['read2']
    return inputs

def ustacks__ustacks_inputs():
    inputs = dict()
    if (config["demultiplexing"] == "null"):
        if (config["SeOrPe"] == "SE"):
            inputs["read"] = raw_reads['read']
        elif (config["SeOrPe"] == "PE"):
            inputs["read"] = raw_reads['read']
            inputs["read2"] = raw_reads['read2']
    else:
        if (config["SeOrPe"] == "PE"):
            inputs["read"] = rules.demultiplexing__process_radtags_PE_merge.output.read.replace("{id}","{sample}") # Replace wildcard name for compatibility with other tools
            inputs["read2"] = rules.demultiplexing__process_radtags_PE_merge.output.read2.replace("{id}","{sample}")
        elif (config["SeOrPe"] == "SE"):
            inputs["read"] = rules.demultiplexing__process_radtags_SE_merge.output.read.replace("{id}","{sample}")
    return inputs

def cstacks__cstacks_inputs():
    inputs = dict()
    inputs["tags"] = expand(rules.ustacks__ustacks.output.tags, sample=individus)
    inputs["snps"] = expand(rules.ustacks__ustacks.output.snps, sample=individus)
    inputs["alleles"] = expand(rules.ustacks__ustacks.output.alleles, sample=individus)
    return inputs

def sstacks__sstacks_inputs():
    inputs = dict()
    inputs["tags"] = rules.cstacks__cstacks.output.tags
    inputs["snps"] = rules.cstacks__cstacks.output.snps
    inputs["alleles"] = rules.cstacks__cstacks.output.alleles
    inputs["ustacks_tags"] = expand(rules.ustacks__ustacks.output.tags, sample=individus)[0] # to get ustacks dir
    return inputs

def tsv2bam__tsv2bam_inputs():
    inputs = dict()
    inputs["tsv"] = rules.sstacks__sstacks.output.matches
    inputs["alleles"] = rules.cstacks__cstacks.output.alleles
    inputs["snps"] = rules.cstacks__cstacks.output.snps
    inputs["tags"] = rules.cstacks__cstacks.output.tags

    inputs["sample_alleles"] = rules.ustacks__ustacks.output.alleles
    inputs["sample_snps"] = rules.ustacks__ustacks.output.snps
    inputs["sample_tags"] = rules.ustacks__ustacks.output.tags
    if (config["SeOrPe"] == "PE"):
        inputs["PE_read2"] = rules.demultiplexing__process_radtags_PE_merge.output.read2.replace("{id}","{sample}")
    return inputs

def gstacks__gstacks_inputs():
    inputs = dict()
    inputs["bams"] = expand(rules.tsv2bam__tsv2bam.output.bam,sample=individus)
    return inputs

def populations__populations_inputs():
    inputs = dict()
    inputs["catalog_calls"] = rules.gstacks__gstacks.output.catalog_calls
    inputs["catalog_fa"] = rules.gstacks__gstacks.output.catalog_fa
    inputs["stats"] = rules.gstacks__gstacks.output.stats
    inputs["popmap"] = config["gstacks__gstacks_population_tsv"]
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "quality_check"):
        if (config[step] == "fastqc"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.quality_check__fastqc_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.quality_check__fastqc_SE.output,sample=SAMPLES)

    elif (step == "demultiplexing"):
        if (config[step] == "process_radtags"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.demultiplexing__process_radtags_PE_merge.output,id=individus)
            else:
                outputs = expand(rules.demultiplexing__process_radtags_SE_merge.output,id=individus)
        elif (config[step] == "null"):
            if(config["SeOrPe"] == "PE"):
                outputs = list(expand(raw_reads["read"],sample=individus))
                outputs.extend(expand(raw_reads["read2"],sample=individus))
            else:
                outputs = expand(raw_reads["read"],sample=individus)
            
    elif (step == "ustacks"):
        outputs.extend(expand(rules.ustacks__ustacks.output.tags,sample=individus))
        outputs.extend(expand(rules.ustacks__ustacks.output.snps,sample=individus))
        outputs.extend(expand(rules.ustacks__ustacks.output.alleles,sample=individus))
    
    elif (step == "cstacks"):
        outputs.append(rules.cstacks__cstacks.output.tags)
        outputs.append(rules.cstacks__cstacks.output.snps)
        outputs.append(rules.cstacks__cstacks.output.alleles)

    elif (step == "sstacks"):
        outputs.extend(expand(rules.sstacks__sstacks.output.matches,sample=individus))

    elif (step == "tsv2bam"):
        outputs.extend(expand(rules.tsv2bam__tsv2bam.output.bam,sample=individus))
    
    elif (step == "gstacks"):
        if (config[step] == "gstacks"):
            outputs = list(rules.gstacks__gstacks.output)

    elif (step == "populations"):
        if (config[step] == "populations"):
            outputs = list(rules.populations__populations.output)

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}