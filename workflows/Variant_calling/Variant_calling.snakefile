{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def reads():
    inputs = dict()
    if (config["SeOrPe"] == "SE"):
        inputs["read"] = raw_reads['read']
    elif (config["SeOrPe"] == "PE"):
        inputs["read"] = raw_reads['read']
        inputs["read2"] = raw_reads['read2']
    return inputs

def preprocess__fastp_inputs():
    return reads()

def mapping__bwa_mem_PE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_PE.output.R1
        inputs["read2"] =  rules.preprocess__fastp_PE.output.R2
    else:
        return reads()
    return inputs

def mapping__bwa_mem_SE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_SE.output.read
    else:
        return reads()
    return inputs

def mapping__bowtie_PE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_PE.output.R1
        inputs["read2"] =  rules.preprocess__fastp_PE.output.R2
    else:
        return reads()
    return inputs

def mapping__bowtie_SE_inputs():
    inputs = dict()
    if (config["preprocess"] == "fastp"):
        inputs["read"] = rules.preprocess__fastp_SE.output.read
    else:
        return reads()
    return inputs

def mark_duplicates__Picard_MarkDuplicates_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bwa_mem_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bwa_mem_SE.output.bam
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bowtie_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bowtie_SE.output.bam
    return inputs

def prepare_fasta__gatk_prepare_fasta_inputs():
    inputs = dict()
    inputs["fasta"] = config["genome_fasta"]
    return inputs

def indel_realign__gatk_IndelRealigner_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bwa_mem_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bwa_mem_SE.output.bam
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bowtie_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bowtie_SE.output.bam
    if (config["mark_duplicates"] == "Picard_MarkDuplicates"):
        inputs["bam"] = rules.mark_duplicates__Picard_MarkDuplicates.output.sorted_bam
    inputs["genome_fasta"] = rules.prepare_fasta__gatk_prepare_fasta.output.fasta
    return inputs

def variant_calling__gatk_haplotype_caller_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bwa_mem_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bwa_mem_SE.output.bam
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bowtie_PE.output.bam
        else:
            inputs["bam"] = rules.mapping__bowtie_SE.output.bam
    if (config["indel_realign"] == "gatk_IndelRealigner"):
        inputs["bam"] = rules.indel_realign__gatk_IndelRealigner.output.sorted_bam
    elif (config["mark_duplicates"] == "Picard_MarkDuplicates"):
        inputs["bam"] = rules.mark_duplicates__Picard_MarkDuplicates.output.sorted_bam
    inputs["genome_fasta"] = rules.prepare_fasta__gatk_prepare_fasta.output.fasta
    return inputs

def variant_calling__bcftools_mpileup_and_call_inputs():
    return variant_calling__gatk_haplotype_caller_inputs()

def variant_calling__deep_variant_inputs():
    return variant_calling__gatk_haplotype_caller_inputs()

def variant_calling__freebayes_inputs():
    return variant_calling__gatk_haplotype_caller_inputs()

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "preprocess"):
        if (config[step] == "fastp"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.preprocess__fastp_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.preprocess__fastp_SE.output,sample=SAMPLES)

    elif (step == "mapping"):
        if (config[step] == "bwa"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.mapping__bwa_mem_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.mapping__bwa_mem_SE.output,sample=SAMPLES)
        elif (config[step] == "bowtie"):
            if (config["SeOrPe"] == "PE"):
                outputs = expand(rules.mapping__bowtie_PE.output.bam,sample=SAMPLES)
            else:
                outputs = expand(rules.mapping__bowtie_SE.output.bam,sample=SAMPLES)

    elif (step == "mark_duplicates"):
        if (config[step] == "Picard_MarkDuplicates"):
            outputs = expand(rules.mark_duplicates__Picard_MarkDuplicates.output,sample=SAMPLES)

    elif (step == "indel_realign"):
        if (config[step] == "gatk_IndelRealigner"):
            outputs = expand(rules.indel_realign__gatk_IndelRealigner.output,sample=SAMPLES)

    elif (step == "variant_calling"):
        if (config[step] == "gatk_haplotype_caller"):
            outputs = expand(rules.variant_calling__gatk_haplotype_caller.output,sample=SAMPLES)
        elif (config[step] == "bcftools_mpileup"):
            outputs = expand(rules.variant_calling__bcftools_mpileup_and_call.output,sample=SAMPLES)
        elif (config[step] == "deep_variant"):
            outputs = expand(rules.variant_calling__deep_variant.output,sample=SAMPLES)
        elif (config[step] == "freebayes"):
            outputs = expand(rules.variant_calling__freebayes.output,sample=SAMPLES)

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}