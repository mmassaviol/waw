{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_read_dir = raw_read_dir(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_read_dir)

# Tools inputs functions
def tlex3_inputs():
	inputs = dict()
	inputs["read_dir"] = raw_read_dir["read_dir"]
	return inputs


{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
	outputs = list()
	if (step == "tlex3"):
		outputs = rules.tlex3.output
		
	if (step == "all"):
		outputs = list(rules.multiqc.output)

	return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
	outputs = list()
	outputs.extend(step_outputs(step))
	return outputs

#########
# Rules #
#########

{import rules}
{import global_rules}
