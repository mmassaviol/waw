{import global_imports}


##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

def get_individus():
    if (config["demultiplexing"] == "null"):
        return SAMPLES
    else:
        with open(config["process_radtags_barcode_file"], mode="r") as infile:
            reader = csv.reader(infile,delimiter='\t')
            return [row[-1] for row in reader]

individus = get_individus()

# Tools inputs functions

def quality_check__fastqc_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def quality_check__fastqc_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def demultiplexing__process_radtags_inputs():
    inputs = dict()
    if (config["SeOrPe"] == "SE"):
        inputs["read"] = raw_reads['read']
    elif (config["SeOrPe"] == "PE"):
        inputs["read"] = raw_reads['read']
        inputs["read2"] = raw_reads['read2']
    return inputs

def mapping__bwa_mem_SE_inputs():
    inputs = dict()
    if (config["demultiplexing"] == "null"):
        inputs["read"] = raw_reads['read']
    else:
        inputs["read"] = rules.demultiplexing__process_radtags_SE_merge.output.read.replace("{id}","{sample}")
    return inputs

def mapping__bwa_mem_PE_inputs():
    inputs = dict()
    if (config["demultiplexing"] == "null"):
        inputs["read"] = raw_reads['read']
        inputs["read2"] = raw_reads['read2']
    else:
        inputs["read"] = rules.demultiplexing__process_radtags_PE_merge.output.read.replace("{id}","{sample}") # Replace wildcard name for compatibility with other tools
        inputs["read2"] = rules.demultiplexing__process_radtags_PE_merge.output.read2.replace("{id}","{sample}")
    return inputs

def mapping__bowtie_SE_inputs():
    return mapping__bwa_mem_SE_inputs()

def mapping__bowtie_PE_inputs():
    return mapping__bwa_mem_PE_inputs()

def mapping_check__samtools_stats_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bwa_mem_PE.output.bam
        elif (config["SeOrPe"] == "SE"):
            inputs["bam"] = rules.mapping__bwa_mem_SE.output.bam
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bam"] = rules.mapping__bowtie_PE.output.bam
        elif (config["SeOrPe"] == "SE"):
            inputs["bam"] = rules.mapping__bowtie_SE.output.bam
    return inputs

def gstacks__gstacks_inputs():
    inputs = dict()
    if (config["mapping"] == "bwa"):
        if (config["SeOrPe"] == "PE"):
            inputs["bams"] = expand(rules.mapping__bwa_mem_PE.output.bam,sample=individus)
        elif (config["SeOrPe"] == "SE"):
            inputs["bams"] = expand(rules.mapping__bwa_mem_SE.output.bam,sample=individus)
    elif (config["mapping"] == "bowtie"):
        if (config["SeOrPe"] == "PE"):
            inputs["bams"] = expand(rules.mapping__bowtie_PE.output.bam,sample=individus)
        elif (config["SeOrPe"] == "SE"):
            inputs["bams"] = expand(rules.mapping__bowtie_SE.output.bam,sample=individus)
    return inputs

def populations__populations_inputs():
    inputs = dict()
    inputs["catalog_calls"] = rules.gstacks__gstacks.output.catalog_calls
    inputs["catalog_fa"] = rules.gstacks__gstacks.output.catalog_fa
    inputs["stats"] = rules.gstacks__gstacks.output.stats
    inputs["popmap"] = config["gstacks__gstacks_population_tsv"]
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "quality_check"):
        if (config[step] == "fastqc"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.quality_check__fastqc_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.quality_check__fastqc_SE.output,sample=SAMPLES)

    elif (step == "demultiplexing"):
        if (config[step] == "process_radtags"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.demultiplexing__process_radtags_PE_merge.output,id=individus)
            else:
                outputs = expand(rules.demultiplexing__process_radtags_SE_merge.output,id=individus)
        elif (config[step] == "null"):
            reads = raw_reads
            if(config["SeOrPe"] == "PE"):
                outputs = list(expand(reads["read"],sample=individus))
                outputs.extend(expand(reads["read2"],sample=individus))
            else:
                outputs = expand(reads["read"],sample=individus)
            
    elif (step == "mapping"):
        if (config[step] == "bwa"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.mapping__bwa_mem_PE.output,sample=individus)
            else:
                outputs = expand(rules.mapping__bwa_mem_SE.output,sample=individus)
        elif (config[step] == "bowtie"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.mapping__bowtie_PE.output,sample=individus)
            else:
                outputs = expand(rules.mapping__bowtie_SE.output,sample=individus)
    
    elif (step == "mapping_check"):
        if (config[step] == "samtools_stats"):
            if (config["mapping"] == "bwa"):
                outputs = expand(rules.mapping_check__samtools_stats.output,sample=individus)
    
    elif (step == "gstacks"):
        if (config[step] == "gstacks"):
            outputs = list(rules.gstacks__gstacks.output)

    elif (step == "populations"):
        if (config[step] == "populations"):
            outputs = list(rules.populations__populations.output)

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}