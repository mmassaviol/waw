{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def preprocessing__fastp_SE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    return inputs

def preprocessing__fastp_PE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    inputs["read2"] = raw_reads['read2']
    return inputs

def kmer_counting__jellyfish_count_inputs():
    inputs = dict()
    if (config["preprocessing"] == "fastp"):
        if (config["SeOrPe"] == "PE"):
            inputs["read"] = expand(rules.preprocessing__fastp_PE.output.read_preprocessed,sample=SAMPLES)
            inputs["read2"] = expand(rules.preprocessing__fastp_PE.output.read2_preprocessed,sample=SAMPLES)
        else:
            inputs["read"] = expand(rules.preprocessing__fastp_SE.output.read_preprocessed,sample=SAMPLES)
    else:
        inputs["read"] = expand(raw_reads["read"],sample=SAMPLES)
        if (config["SeOrPe"] == "PE"):
            inputs["read2"] = expand(raw_reads["read2"],sample=SAMPLES)
    return inputs

def kmer_histogram__jellyfish_histo_inputs():
    inputs = dict()
    if (config["kmer_counting"] == "jellyfish_count"):
        inputs["kmer_counts"] = rules.kmer_counting__jellyfish_count.output.kmer_counts
    return inputs

def kmer_analysis__genomescope_inputs():
    inputs = dict()
    if (config["kmer_analysis"] == "genomescope"):
        inputs["kmer_histo"] = rules.kmer_histogram__jellyfish_histo.output.kmer_histo
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()

    if (step == "preprocessing"):
        if (config[step] == "fastp"):
            if config["SeOrPe"] == "SE":
                outputs = expand(rules.preprocessing__fastp_SE.output, sample=SAMPLES)
            elif config["SeOrPe"] == "PE":
                outputs = expand(rules.preprocessing__fastp_PE.output, sample=SAMPLES)

    if (step == "kmer_counting"):
        if (config[step] == "jellyfish_count"):
            outputs = rules.kmer_counting__jellyfish_count.output

    if (step == "kmer_histogram"):
        if (config[step] == "jellyfish_histo"):
            outputs = rules.kmer_histogram__jellyfish_histo.output

    if (step == "kmer_analysis"):
        if (config[step] == "genomescope"):
            outputs = rules.kmer_analysis__genomescope.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}