{import global_imports}

##########
# Inputs #
##########

# Tools inputs functions

def interop_read_metrics__interop_inputs():
    inputs = dict()
    inputs["runinfo"] = config["interop_read_metrics__interop_analysis_dir"]+"/RunInfo.xml"
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "interop_read_metrics"):
        outputs = rules.interop_read_metrics__interop.output

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}