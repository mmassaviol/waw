def prepare_report_inputs():
    inputs = list()
    for step in STEPS:
        inputs.extend(step_outputs(step["name"]))
    return inputs

def prepare_report_scripts():
    scripts = list()
    for step in STEPS:
        tool = config[step["name"]]
        prefix = tool+".prepare.report."
        if type(PREPARE_REPORT_SCRIPTS) == type(""):
            if prefix in PREPARE_REPORT_SCRIPTS:
                scripts.append("/workflow/scripts/"+PREPARE_REPORT_SCRIPTS)
        else :        
            script = [s for s in PREPARE_REPORT_SCRIPTS if prefix in s]
            if (len(script)==1):
                scripts.append("/workflow/scripts/"+script[0])
    return scripts

def prepare_report_outputs():
    outputs = list()
    outputs.append(config["results_dir"] + "/outputs_mqc.csv")
    for step in STEPS:
        tool = config[step["name"]]
        if (tool in PREPARE_REPORT_OUTPUTS.keys()):
            if type(PREPARE_REPORT_OUTPUTS[tool]) == type(""):
                outputs.append(config["results_dir"]+"/"+tool+"/"+PREPARE_REPORT_OUTPUTS[tool])
            else:
                for output in PREPARE_REPORT_OUTPUTS[tool]:
                    outputs.append(config["results_dir"]+"/"+tool+"/"+output)
    return outputs

def multiqc_inputs():
    # Need prepare_report inputs and outputs in case prepare_reports has no outputs
    return prepare_report_outputs()