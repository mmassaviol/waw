{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

def get_individus():
    if (config["demultiplexing"] == "null"):
        return SAMPLES
    else:
        with open(config["demultiplexing__demultiplexing_astrid_cruaud_barcodes"], mode="r") as infile:
            reader = csv.reader(infile,delimiter='\t')
            next(reader, None) # remove header (first line)
            return [row[0] for row in reader]

INDIVIDUS = get_individus()

# Tools inputs functions
def quality_control__fastqc_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def quality_control__fastqc_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def qcfilter_adaptertrim__trimmomatic_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def qcfilter_adaptertrim__trimmomatic_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def merge_overlapps__flash_inputs():
	inputs = dict()
	inputs["read"] = rules.qcfilter_adaptertrim__trimmomatic_PE.output.readFP
	inputs["read2"] = rules.qcfilter_adaptertrim__trimmomatic_PE.output.readRP
	return inputs

def demultiplexing__demultiplexing_astrid_cruaud_inputs():
	inputs = dict()
	inputs["extended_frags"] = expand(rules.merge_overlapps__flash.output.extendedFrags, sample=SAMPLES)
	return inputs

def assembling__velvet_inputs():
	inputs = dict()
	inputs["merged_reads"] = config["results_dir"]+"/"+config["demultiplexing__demultiplexing_astrid_cruaud_output_dir"]+"/{sample}_fq"
	return inputs

def contigmaptouce__lastz_inputs():
	inputs = dict()
	inputs["contigs"] = rules.assembling__velvet.output.contigs
	return inputs

def mapping_check_bf__samtools_stats_inputs():
	inputs = dict()
	inputs["bam"] = rules.contigmaptouce__lastz.output.align_on_ref
	return inputs

def mapping_check_af__samtools_stats_inputs():
	inputs = dict()
	inputs["bam"] = rules.contigmaptouce__lastz.output.filtered
	return inputs

def convert_to_phylip__sam_to_phylip_inputs():
	inputs = dict()
	inputs["sams"] = expand(rules.contigmaptouce__lastz.output.filtered, sample=INDIVIDUS)
	return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
	outputs = list()
	if (step == "quality_control" and config['SeOrPe'] == 'PE' ):
		outputs = expand(rules.quality_control__fastqc_PE.output, sample=SAMPLES)
		
	if (step == "qcfilter_adaptertrim" and config['SeOrPe'] == 'PE' ):
		outputs = expand(rules.qcfilter_adaptertrim__trimmomatic_PE.output, sample=SAMPLES)
		
	if (step == "merge_overlapps"  ):
		outputs = expand(rules.merge_overlapps__flash.output, sample=SAMPLES)
		
	if (step == "demultiplexing"  ):
		outputs = rules.demultiplexing__demultiplexing_astrid_cruaud.output
		
	if (step == "assembling"  ):
		outputs = expand(rules.assembling__velvet.output, sample=INDIVIDUS)
		
	if (step == "contigmaptouce"  ):
		outputs = expand(rules.contigmaptouce__lastz.output, sample=INDIVIDUS)

	if (step == "mapping_check_bf"  ):
		outputs = expand(rules.mapping_check_bf__samtools_stats.output, sample=INDIVIDUS)

	if (step == "mapping_check_af"  ):
		outputs = expand(rules.mapping_check_af__samtools_stats.output, sample=INDIVIDUS)

	if (step == "convert_to_phylip"  ):
		outputs = rules.convert_to_phylip__sam_to_phylip.output
		
	if (step == "all"):
		outputs = list(rules.multiqc.output)

	return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
	outputs = list()
	outputs.extend(step_outputs(step))
	return outputs

#########
# Rules #
#########

{import rules}
{import global_rules}
