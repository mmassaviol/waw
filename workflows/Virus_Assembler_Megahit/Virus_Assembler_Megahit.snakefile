{import global_imports}

##########
# Inputs #
##########

# raw_inputs function call
raw_reads = raw_reads(config['results_dir'], config['sample_dir'], config['SeOrPe'])
config.update(raw_reads)
SAMPLES = raw_reads['samples']

# Tools inputs functions

def quality_check__fastqc_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def quality_check__fastqc_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def trimming__cutadapt_PE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    inputs["read2"] = raw_reads['read2']
    return inputs

def trimming__cutadapt_SE_inputs():
    inputs = dict()
    inputs["read"] = raw_reads['read']
    return inputs

def trimming__trimmomatic_PE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	inputs["read2"] = raw_reads["read2"]
	return inputs

def trimming__trimmomatic_SE_inputs():
	inputs = dict()
	inputs["read"] = raw_reads["read"]
	return inputs

def assembling__megahit_SE_inputs():
    inputs = dict()
    if config["trimming"] == "null":
        inputs["read"] = expand(raw_reads["read"],sample=SAMPLES)
    elif config["trimming"] == "cutadapt":
        inputs["read"] = expand(rules.trimming__cutadapt_SE.output.read_trimmed,sample=SAMPLES)
    elif config["trimming"] == "trimmomatic":
        inputs["read"] = expand(rules.trimming__trimmomatic_SE.output.read,sample=SAMPLES)
    return inputs

def assembling__megahit_PE_inputs():
    inputs = dict()
    if config["trimming"] == "null":
        inputs["read"] = expand(raw_reads["read"],sample=SAMPLES)
        inputs["read2"] = expand(raw_reads["read2"],sample=SAMPLES)
    elif config["trimming"] == "cutadapt":
        inputs["read"] = expand(rules.trimming__cutadapt_PE.output.read_trimmed,sample=SAMPLES)
        inputs["read2"] = expand(rules.trimming__cutadapt_PE.output.read2_trimmed,sample=SAMPLES)
    elif config["trimming"] == "trimmomatic":
        inputs["read"] = expand(rules.trimming__trimmomatic_PE.output.readFP,sample=SAMPLES)
        inputs["read2"] = expand(rules.trimming__trimmomatic_PE.output.readRP,sample=SAMPLES)
    return inputs

def blasting__blastn_inputs():
    inputs = dict()
    inputs["query"] = config["results_dir"] + "/" + config["assembling__megahit_"+config["SeOrPe"]+"_output_dir"] + "/assembly.contigs.fa"
    inputs["blastdb"] = ("/blast_db_virus/refseq_virus.nhr","/blast_db_virus/refseq_virus.nin","/blast_db_virus/refseq_virus.nsq")
    return inputs

def indexing__bwa_mem_index_inputs():
    inputs = dict()
    inputs["genome_fasta"] = rules.blasting__blastn.output.selected_contigs
    return inputs

def mapping__bwa_mem_PE_inputs():
    inputs = dict()
    if config["trimming"] == "null":
        inputs["read"] = raw_reads["read"]
        inputs["read2"] = raw_reads["read2"]
    elif config["trimming"] == "cutadapt":
        inputs["read"] = rules.trimming__cutadapt_PE.output.read_trimmed
        inputs["read2"] = rules.trimming__cutadapt_PE.output.read2_trimmed
    elif config["trimming"] == "trimmomatic":
        inputs["read"] = rules.trimming__trimmomatic_PE.output.readFP
        inputs["read2"] = rules.trimming__trimmomatic_PE.output.readRP
    inputs["index"] = rules.indexing__bwa_mem_index.output
    return inputs

def mapping__bwa_mem_SE_inputs():
    inputs = dict()
    if config["trimming"] == "null":
        inputs["read"] = raw_reads["read"]
    elif config["trimming"] == "cutadapt":
        inputs["read"] = rules.trimming__cutadapt_SE.output.read_trimmed
    elif config["trimming"] == "trimmomatic":
        inputs["read"] = rules.trimming__trimmomatic_SE.output.read
    inputs["index"] = rules.indexing__bwa_mem_index.output
    return inputs

{import global_functions}

###########
# Outputs #
###########

def step_outputs(step):
    outputs = list()
    if (step == "quality_check"):
        if (config[step] == "fastqc"):
            if(config["SeOrPe"] == "PE"):
                outputs = expand(rules.quality_check__fastqc_PE.output,sample=SAMPLES)
            else:
                outputs = expand(rules.quality_check__fastqc_SE.output,sample=SAMPLES)

    if (step == "trimming"):
        if config["SeOrPe"] == "SE":
            if config["trimming"] == "null":
                outputs = expand(raw_reads["read"],sample=SAMPLES)
            elif config["trimming"] == "cutadapt":
                outputs = expand(rules.trimming__cutadapt_SE.output.read_trimmed,sample=SAMPLES)
            elif config["trimming"] == "trimmomatic":
                outputs = expand(rules.trimming__trimmomatic_SE.output.read,sample=SAMPLES)
        if config["SeOrPe"] == "PE":
            if config["trimming"] == "null":
                outputs = expand(raw_reads["read"],sample=SAMPLES)
                outputs = expand(raw_reads["read2"],sample=SAMPLES)
            elif config["trimming"] == "cutadapt":
                outputs = expand(rules.trimming__cutadapt_PE.output.read_trimmed,sample=SAMPLES)
                outputs = expand(rules.trimming__cutadapt_PE.output.read2_trimmed,sample=SAMPLES)
            elif config["trimming"] == "trimmomatic":
                outputs = expand(rules.trimming__trimmomatic_PE.output.readFP,sample=SAMPLES)
                outputs = expand(rules.trimming__trimmomatic_PE.output.readRP,sample=SAMPLES)

    if (step == "assembling"):
        if(config["SeOrPe"] == "PE"):
            outputs = rules.assembling__megahit_PE.output
        else:
            outputs = rules.assembling__megahit_SE.output
    
    if (step == "blasting"):
        outputs = rules.blasting__blastn.output

    if (step == "indexing"):
        outputs = rules.indexing__bwa_mem_index.output

    if (step == "mapping"):
        if(config["SeOrPe"] == "PE"):
            outputs = expand(rules.mapping__bwa_mem_PE.output,sample=SAMPLES)
        else:
            outputs = expand(rules.mapping__bwa_mem_SE.output,sample=SAMPLES)

    elif (step == "all"):
        outputs = list(rules.multiqc.output)

    return outputs

# get outputs for each choosen tools
def workflow_outputs(step):
    outputs = list()
    outputs.extend(step_outputs(step))
    return outputs

#########
# Rules #
#########

{import rules}

{import global_rules}