#!/usr/bin/python3
# This script will take an tool and generate snakefile
# Accepts 1 parameter:
# Usage: ./generate_tool_snakefile.py tool_name

from tools import read_yaml
import sys

def generate(name, path_input = "./tools/"):

    result = "# File generate with generate_tool_snakefile.py\n\n"

    yaml = read_yaml(path_input+name+"/"+name+".yaml")

    result_dir = "config[\"results_dir\"]"

    # is there a sample wildcard in inputs or outputs
    sample_in = False
    sample_out = False

    if "commands" in yaml: 

        for cmd in yaml['commands']:

            indent = ''

            if "_SE" in cmd["name"]:
                result += "if config['SeOrPe'] == 'SE':\n\n"
                indent = '    '
            if "_PE" in cmd["name"]:
                result += "if config['SeOrPe'] == 'PE':\n\n"
                indent = '    '

            result += indent + "rule <step_name>__" + cmd['name'] + ":\n"

            rule_dir = "config[\"<step_name>__" + cmd['name'] + "_output_dir\"]"

            # input 

            if "inputs" in cmd: 

                result += indent + "    input:\n"

                cmd_inputs = cmd['inputs']

                result += indent + "        **<step_name>__" + cmd["name"] + "_inputs(),\n"

                for inputt in cmd_inputs:
                    if "file" in inputt.keys():
                        if '{sample}' in inputt["file"]:
                            sample_in = True

            if "outputs" in cmd: 

                result += indent + "    output: \n"

                for output in cmd["outputs"]:

                    output_name = ""

                    if "name" in output:
                        output_name = output['name'] + " = "

                    if "file" in output:

                        if '{sample}' in output['file']:
                            sample_out = True

                        if "name" in cmd:
                            
                            result += indent + "        " + output_name + result_dir + " + \"/\" + " + rule_dir + " + \"/" + output['file'] + "\",\n" 

                    if "directory" in output:

                        if "name" in cmd:

                            result += indent + "        directory(" + result_dir + " + \"/\" + " + rule_dir + " + \"/" + output['directory'] + "\"),\n" 

            # params

            result += indent + "    params: \n"

            result += indent + "        output_dir = " + result_dir + " + \"/\" + " + rule_dir + ",\n"

            if "command" in cmd:
                result += indent + "        command = config[\"<step_name>__" + cmd['name'] + "_command\"],\n"

            if "options" in cmd:
                
                cmd_options = cmd['options']

                for option in cmd_options:

                    if "type" in option and option["type"] != "input_file":

                        if "name" in option:

                            if "_threads" not in option['name']:

                                result += indent + "        " + option['name'].replace(cmd["name"]+"_","").replace(name+"_","") + " = " + "config[\"<step_name>__"+option['name']+"\"],\n"

            # log 

            if "name" in cmd:

                if sample_out:
                    sample = "{sample}_"
                else:
                    sample = ""

                result += indent + "    log: \n"

                result += indent + "        " + result_dir + " + \"/logs/\" + " + rule_dir + " + \"/" + sample + cmd['name'] + "_log.txt\"\n"

            # threads

            result += indent + "    threads: "

            count_thread = 0

            for option in cmd_options:

                if "name" in option: 

                    if "_threads" in option['name']:
                        result += " config[\"<step_name>__" + option['name'] + "\"]\n"
                        count_thread += 1

            if count_thread == 0:

                result += "1\n"

            # shell

            if "command" in cmd:

                cmd_shell = cmd['command']

                result += indent + "    shell: \n"

                #for shell in cmd_shell:
                #    result += "        \"" + shell + "\"\n"
                result += indent + "        \"{params.command} \"\n"

                for param in cmd['options']:
                    prefix = ""
                    if "prefix" in param.keys() and param["prefix"] != None:
                        prefix = str(param["prefix"]) + " "
                    if "threads" in param["name"]:
                        result += indent + "        \"" + prefix + "{threads} \"\n"
                    else:
                        result += indent + "        \"" + prefix + "{params." + param['name'].replace(cmd["name"]+"_","").replace(name+"_","") + "} \"\n"
            

            # End cmd

            result += "\n"

        # End loop cmd

    #print(result)

    path_snakefile = path_input + "/" + name + "/" + name + ".rule.snakefile"
    f = open(path_snakefile, "w")
    f.write(result)
    f.close()


def main():
    if len(sys.argv) == 2:
        generate(sys.argv[1])
    elif len(sys.argv) == 3:
        generate(sys.argv[1], sys.argv[2])
    else:
        exit("""Needs 1 argument: tool_name
Usage: ./generate_tool_snakefile.py tool_name """)


if __name__ == "__main__":
    # execute only if run as a script
    main()
