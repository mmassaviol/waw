# Génération du fichier de paramètre

Le fichier de paramètre est généré avec le script `generate_workflow.py`. Les informations des fichiers de description du workflow et des outils y sont reprises pour être utilisées à la fois par Snakemake et par l'application Shiny

Il contient plusieurs sections :

* `params` : paramètres du workflow et des différents outils
* `steps` : étapes du workflow et les outils disponibles
* `params_info` : informations sur les paramètres à afficher dans l'interface
* `outputs` : fichiers de sortie des différents outils
* `multiqc` : nom des modules mutliqc à appeler
* `prepare_report_scripts` : liste des scripts "prepare report" à exécuter
* `prepare_report_outputs` : sorties des scripts prepare report
