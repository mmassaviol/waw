# Local config

Le mécanisme de local config permet d'ajouter des informations pour la génération du workflow permettant d'ajouter des commandes dans le Dockerfile. Dans le dossier `local_configs` on trouve des fichiers yaml contenant un champ `docker` avec les champs `env` et `run`. Les infos comprises dans ces champs sont ajoutées au Dockerfile lors de la génération du workflow.

exemple :

``` yaml
docker:
  env: [
    "ENV LANG en_US.UTF-8",
    "ENV LANGUAGE en_US:en",
    "ENV LC_ALL en_US.UTF-8"
  ]
  run: [
    "#This part is necessary to run on ISEM cluster",
    "RUN mkdir -p /share/apps/bin \\",
    " && mkdir -p /share/apps/lib \\",
    " && mkdir -p /share/apps/gridengine \\",
    " && mkdir -p /share/bio \\",
    " && mkdir -p /opt/gridengine \\",
    " && mkdir -p /export/scrach \\",
    " && mkdir -p /usr/lib64 \\",
    " && ln -s /bin/bash /bin/mbb_bash \\",
    " && ln -s /bin/bash /bin/isem_bash \\",
    " && /usr/sbin/groupadd --system --gid 400 sge \\",
    " && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge"
  ]
```
