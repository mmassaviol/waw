# Génération du fichier de config SAG

L'outil SAG que nous utilisons pour générer une application Shiny qui va permettre de paramétrer un workflow, de l'exécuter et de voir le rapport d'exécution a besoin d'un fichier de config. Ce fichier va décrire les étapes des workflows, les outils et leurs paramètres, ainsi que des informations à afficher dans l'interface.

Il est généré à l'aide du script `generate_sag_yaml.py` qui prends en entrée le fichier de description d'un workflow.

Il suffit ensuite de le passer au programme SAG pour récupérer une application Shiny que l'on va copier dans le conteneur du workflow.
