# Génération du Dockerfile

Les workflow générés sont encapsulés dans un conteneur Docker. La recette de ce conteneur est générée par le script `generate_workflow.py` à partir d'un conteneur de base ainsi que des informations d'installation des différents outils du workflow.

## Conteneur de base

Le conteneur de base est une base commune à tous les workflows. Sa recette est disponible dans le dépôt WAW dans le dossier `Docker_base`. Ce conteneur permet notamment de factoriser l'installation des outils communs comme Snakemake, MultiQC, R Shiny, ... qui servent dans chacun des workflows.

Ce conteneur est basé sur le conteneur `rocker/r-ver:3.5.3` qui tourne avec debian stretch (9.13).

## Conteneur généré

Le conteneur généré à pour base le fichier Dockerfile.template à la racine du dépôt WAW. Ce fichier contient seulement le `FROM` du conteneur de base (vu précédemment) disponible sur le docker hub.

``` docker
FROM mbbteam/mbb_workflows_base:latest as alltools
```

Le script `generate_workflow.py` parcours les différents outils inclus dans le workflow et utilise les sections `install` de leurs fichiers de description pour inclure les lignes de commande d'installation des outils et de leurs dépendances. Ensuite on ajoute optionnellement des commandes avec le système de local configs (voir partie TODO).

On expose un port pour pouvoir accéder à l'application shiny depuis l'extérieur du conteneur (port 3838) et on donne la commande pour lancer le conteneur qui va démarrer l'application shiny associée au workflow.

La recette termine par la copie des autres fichiers générés (Snakefile, fichier de paramètres, appli shiny, ...) et optionnellement des données associées à un outil ou au workflow.
