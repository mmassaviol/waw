# Fonctionnement du framework

Ici nous allons détailler le fonctionnement des différents éléments du framework :

```eval_rst
.. toctree::
    :maxdepth: 1
    :caption: Table des matières:

    snakefile_generate
    paramfile_generate
    dockerfile_generate
    sagapp_generate
    raw_inputs
    local_config
```
