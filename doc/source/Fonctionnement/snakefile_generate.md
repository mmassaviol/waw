# Génération du Snakefile

Le Snakefile est généré avec le script `generate_workflow.py`.

On ajoute tout d'abord quelques `imports` nécessaires et on définit le `workdir` comme étant le dossier de résultat présent dans le fichier de paramètres.

On ouvre ensuite le fichier `workflow.snakefile` que nous avons généré et ajusté si besoin précédemment (voir étape [Ajouter un workflow](../ajout_workflow)). Dans ce fichier nous allons insérer des fichiers générique à tous les workflows : `global_imports.py`, `global_functions.py` et `global_rules.snakefile`.

Ensuite on parcours les étapes du workflow pour importer les règles des différents outils.

Pour terminer on remplace les paramètres marqués comme égaux dans le fichier de description du workflow.
