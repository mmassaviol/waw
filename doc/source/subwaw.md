# Aide à la création de workflow

En parallèle de ce framework nous avons développé une interface web permettant de créer des workflows de manière graphique. A l'aide d'un système de blocs représentant des outils, munis de ports d'entrées et de sorties à connecter à d'autres blocs, on peut dessiner son workflow.

## SUBWAW

Cet outil [SUBWAW](https://gitlab.mbb.univ-montp2.fr/jlopez/subwaw) permet de se passer de l'étape de rédaction d'un fichier de descritption yaml pour un workflow ce qui facilite et accélère grandement la création de workflow.

<center><h3>Utilisation de SUBWAW</h3></center>

![Utilisation de SUBWAW](../images/subwaw.png)
