# Ajouter un outil (pas à pas)

Ici on va ajouter un outil au framework pas à pas.

Nous avons choisi l'outil [GetOrganelle](https://github.com/Kinggerm/GetOrganelle) pour cette démonstration.

Tout d'abord nous créons un nouveau dossier dans le dossier tools de waw nommé `get_organelle`.

## get_organelle.yaml

Pour ajouter un outil dans WAW il faut tout d'abord rédiger le fichier de description au format yaml. Pour cela on peut se baser sur le template de base `tool_template.yaml` disponible dans le dossier `tools` de WAW.

``` yaml
{
  id: "",
  name: "",
  description: "",
  version: "",
  website: "",
  git: "",
  documentation: "",
  article: "", # DOI
  multiqc: "",
  commands:
    [
      {
        name: "",
        c_name: "",
        output_dir: "",
        category: "",
        command: "",
        inputs: [
          {
            name: "",
            file: "",
            type: "",
            description: ""
          }
        ],
        outputs: [
          {
            name: "",
            file: "",
            type: "",
            description: ""
          }
        ],
        options:
          [
            ...
          ],
      },
    ],
  install: {
    tool_name:
    [
      "",
    ]
  },
  citations: {
    tool_name:
    [
      ""
    ]
  },
}
```

On va maintenant remplir ce fichier `get_organelle.yaml` (a créer dans le dossier `tools/get_organelle`) étape par étape.

### Infos générales

On remplit la partie informations générales avec l'id de l'outil, son nom, une description, ...

Cet outil n'étant pas intégré dans MultiQC (voir outils supportés par MultiQC : [https://multiqc.info/#supported-tools](https://multiqc.info/#supported-tools))

``` yaml
{
  id: "get_organelle",
  name: "GetOrganelle",
  description: "This toolkit assemblies organelle genome from genomic skimming data.",
  version: "1.7.1",
  website: "https://github.com/Kinggerm/GetOrganelle",
  git: "https://github.com/Kinggerm/GetOrganelle",
  documentation: "https://github.com/Kinggerm/GetOrganelle/wiki",
  article: "10.1186/s13059-020-02154-5",
  multiqc: "custom",
```

### Installations

Dans le champ installation on trouve un dictionnaire prenant les différentes dépendances à installer ainsi que l'outil lui-même. Chaque outil est associé à une liste de commandes bash. On peut glisser dans cette liste une ligne qui commence par `ENV` qui sera intégré dans le dockerfile pour par exemple ajouter un outil au PATH.

Ici on installe spades, bowtie2, blast et get_organelle.

``` yaml
install: {
  spades:
  [
    "cd /opt/biotools",
    "wget http://cab.spbu.ru/files/release3.14.1/SPAdes-3.14.1-Linux.tar.gz",
    "tar -xvzf SPAdes-3.14.1-Linux.tar.gz",
    "ENV PATH $PATH:/opt/biotools/SPAdes-3.14.1-Linux/bin",
    "rm SPAdes-3.14.1-Linux.tar.gz"
  ],
  bowtie2: [
    "wget -O bowtie2-2.4.1-linux-x86_64.zip https://github.com/BenLangmead/bowtie2/releases/download/v2.4.1/bowtie2-2.4.1-linux-x86_64.zip",
    "unzip bowtie2-2.4.1-linux-x86_64.zip",
    "cp bowtie2-2.4.1-linux-x86_64/bowtie2* /usr/bin",
    "rm -rf bowtie2-2.4.1*"
  ],
  blast: [
    "cd /opt/biotools/",
    "wget -O ncbi-blast-2.12.0+.tar.gz https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.12.0/ncbi-blast-2.12.0+-x64-linux.tar.gz",
    "tar -xvzf ncbi-blast-2.12.0+.tar.gz",
    "ENV PATH $PATH:/opt/biotools/ncbi-blast-2.12.0+/bin"
  ],
  bandage:
  [
    "cd /opt/biotools",
    "wget https://github.com/rrwick/Bandage/releases/download/v0.8.1/Bandage_Ubuntu_static_v0_8_1.zip",
    "unzip -d bandage Bandage_Ubuntu_static_v0_8_1.zip",
    "mv bandage/Bandage bin/",
    "rm -r bandage Bandage_Ubuntu_static_v0_8_1.zip"
  ],
  get_organelle:
  [
    "cd /opt/biotools",
    "wget -O GetOrganelle-1.7.1.tar.gz https://github.com/Kinggerm/GetOrganelle/archive/1.7.1.tar.gz",
    "tar -xvzf GetOrganelle-1.7.1.tar.gz",
    "mv GetOrganelle-1.7.1 GetOrganelle",
    "pip3 instal ./GetOrganelle",
    "rm GetOrganelle-1.7.1.tar.gz",
    "get_organelle_config.py -a all"
  ]
  ...
```

### Citations

Dans le champ `citations` on retrouve les citations en lien avec l'outil et les dépendances. Elle seront reprises dans le rapport d'analyse en fin de workflow.

``` yaml
citations: {
  spades:
  [
    "Bankevich, A., S. Nurk, D. Antipov, A. A. Gurevich, M. Dvorkin, A. S. Kulikov, V. M. Lesin, S. I. Nikolenko, S. Pham, A. D. Prjibelski, A. V. Pyshkin, A. V. Sirotkin, N. Vyahhi, G. Tesler, M. A. Alekseyev and P. A. Pevzner. 2012. SPAdes: a new genome assembly algorithm and its applications to single-cell sequencing. Journal of Computational Biology 19: 455-477."
  ],
  bowtie2:
  [
    "Langmead, B. and S. L. Salzberg. 2012. Fast gapped-read alignment with Bowtie 2. Nature Methods 9: 357-359."
  ],
  blast:
  [
    "Camacho, C., G. Coulouris, V. Avagyan, N. Ma, J. Papadopoulos, K. Bealer and T. L. Madden. 2009. BLAST+: architecture and applications. BMC Bioinformatics 10: 421."
  ],
  bandage:
  [
    "Wick, R. R., M. B. Schultz, J. Zobel and K. E. Holt. 2015. Bandage: interactive visualization of de novo genome assemblies. Bioinformatics 31: 3350-3352."
  ],
  get_organelle:
  [
    "Jian-Jun Jin*, Wen-Bin Yu*, Jun-Bo Yang, Yu Song, Claude W. dePamphilis, Ting-Shuang Yi, De-Zhu Li. GetOrganelle: a fast and versatile toolkit for accurate de novo assembly of organelle genomes. Genome Biology 21, 241 (2020). https://doi.org/10.1186/s13059-020-02154-5"
  ]
```

### Commandes

Dans la partie `commands` on va retrouver les différentes commandes qui vont correspondre aux règles Snakemake.

Ici on aura deux commandes `get_organelle_SE` et `get_organelle_PE` afin de gérer les reads single end et paired end.

#### Informations sur la commande

Pour la commande `get_organelle_SE` on a donc les informations suivantes :

``` yaml
commands:
    [
      {
        name: "get_organelle_SE",
        c_name: "GetOrganelle SE",
        output_dir: "get_organelle_SE",
        category: "assembly",
        command: "get_organelle_from_reads.py",
        ...
```

#### Inputs et outputs de la commande

La commande `get_organelle_SE` prends en entrée des reads. On a donc une entrée read de type reads.

``` yaml
inputs: [
  {
    name: "read",
    type: "reads",
  },
]
```

``` Note:: Dans le cas de reads paired end on aurait en plus une deuxième entrée de type reads nommée read2
```

En output dans ce cas on choisit de mettre un fichier fasta que l'on va créer en y mettant les différentes sorties fasta de l'outil (correspondant par exemple à plusieurs mitochondries assemblées séparément)

``` yaml
outputs: [
  {
    name: "assembly",
    file: "assembly.fasta",
    type: "contigs",
    description: "Assembly fasta file"
  },
]
```

#### Options

Prenons l'exemple d'utilisation présent sur la doc de l'outil.

```
get_organelle_from_reads.py -1 Arabidopsis_simulated.1.fq.gz -2 Arabidopsis_simulated.2.fq.gz -t 1 -o Arabidopsis_simulated.plastome -F embplant_pt -R 10

# Flag	Value				Illustration
# -1	Arabidopsis_simulated.1.fq.gz	Input file with the forward paired-end reads (*.fq/.gz/.tar.gz)
# -2	Arabidopsis_simulated.2.fq.gz	Input file with the reverse paired-end reads (*.fq/.gz/.tar.gz)
# -t	1				Maximum threads to use. Default: 1
# -o	Arabidopsis_simulated.plastome	Output directory
# -F	embplant_pt			Target organelle genome type(s)
# -R	10				Maximum extension rounds
```

On y voit les options `-t`, `-F` et `-R` (les autres options étant seulement les inputs et outputs).
Si on retranscrit ces options dans le yaml on a :

``` yaml
options: [
  {
    name: get_organelle_SE_threads,
    prefix: -t,
    type: numeric,
    value: 4,
    min: 1,
    max: NA,
    step: 1,
    label: "Number of threads to use",
  },
  {
    name: get_organelle_SE_organelle_type,
    type: select,
    prefix: "-F",
    choices: [
      embryophyta plant plastome : "embplant_pt",
      non-embryophyta plant plastome : "other_pt",
      plant mitogenome : "embplant_mt",
      plant nuclear ribosomal RNA : "embplant_nr",
      animal mitogenome : "animal_mt",
      fungus mitogenome : "fungus_mt",
    ],
    value: "animal_mt",
    label: "Organelle type"
    },
  {
    name: get_organelle_SE_max_rounds,
    prefix: -R,
    type: numeric,
    value: 10,
    min: 0,
    max: NA,
    step: 1,
    label: "Maximum number of extending rounds (suggested: >=2)",
  },
]
```

## get_organelle.rule.snakefile

Le fichier snakefile associé à l'outil peut être pré-généré à partir du fichier yaml que nous venons d'écrire. Il suffit pour cela de se placer dans le dossier racine de waw et de lancer le script `generate_tool_snakefile.py` avec le nom de l'outil.

``` console
  $ python3 generate_tool_snakefile.py get_organelle
```

On obtient alors le fichier `get_organelle.rule.snakefile` qu'il faut alors compléter.

``` python
# File generate with generate_tool_snakefile.py

if config['SeOrPe'] == 'SE':

    rule <step_name>__get_organelle_SE:
        input:
            **<step_name>__get_organelle_SE_inputs(),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__get_organelle_SE_output_dir"] + "/assembly.fasta",
        params:
            output_dir = config["results_dir"] + "/" + config["<step_name>__get_organelle_SE_output_dir"],
            command = config["<step_name>__get_organelle_SE_command"],
            organelle_type = config["<step_name>__get_organelle_SE_organelle_type"],
            max_rounds = config["<step_name>__get_organelle_SE_max_rounds"],
        log:
            config["results_dir"] + "/logs/" + config["<step_name>__get_organelle_SE_output_dir"] + "/get_organelle_SE_log.txt"
        threads:  config["<step_name>__get_organelle_SE_threads"]
        shell:
            "{params.command} "
            "-t {threads} "
            "-F {params.organelle_type} "
            "-R {params.max_rounds} "
...
```

Ici la seule partie à compléter est la partie shell. Il nous faut ajouter l'input de l'outil qui est les reads. Ici on a en entrée une liste de reads (car notre output ne possède pas de wildcard sample, on utilise donc tous les fichiers pour produire un assemblage). D'après la doc de l'outil il faut séparer les fichiers par une virgule.

Pour cela on va rajouter un paramètre qui va crée cette liste à virgule.

``` python
input_line = lambda w, input: ",".join(input.read)
```

On utilise ensuite ce paramètre dans le shell ainsi que le dossier de sortie

``` python
shell:
    "{params.command} "
    "-u {params.input_line} "
    "-o {params.output_dir} "
    "-t {threads} "
    "-F {params.organelle_type} "
    "-R {params.max_rounds} "
```

Pour cet outil nous avons choisi de sortir un seul fichier fasta (`assembly.fasta`) or cet outil peut dans certains cas sortir plusieurs fichiers fasta (par exemple quand plusieurs mitochondries sont detectées). Nous allons donc ajouter un morceau de code bash pour sortir un seul fichier fasta reprenant les séquances des 'n' sorties fasta de l'outil.

``` python
shell:
    "{params.command} "
    "-u {params.input_line} "
    "-o {params.output_dir} "
    "-t {threads} "
    "-F {params.organelle_type} "
    "-R {params.max_rounds} ; "
    "for fasta in $(ls {params.output_dir}/*.path_sequence.fasta); "
    "do cat $fasta >> {output.assembly}; "
    "done"
```

On obtient donc la règle finale :

``` python
if config['SeOrPe'] == 'SE':

    rule <step_name>__get_organelle_SE:
        input:
            **<step_name>__get_organelle_SE_inputs(),
        output:
            assembly = config["results_dir"] + "/" + config["<step_name>__get_organelle_SE_output_dir"] + "/assembly.fasta",
        params:
            output_dir = config["results_dir"] + "/" + config["<step_name>__get_organelle_SE_output_dir"],
            command = config["<step_name>__get_organelle_SE_command"],
            organelle_type = config["<step_name>__get_organelle_SE_organelle_type"],
            max_rounds = config["<step_name>__get_organelle_SE_max_rounds"],
            input_line = lambda w, input: ",".join(input.read),
        log:
            config["results_dir"] + "/logs/" + config["<step_name>__get_organelle_SE_output_dir"] + "/get_organelle_SE_log.txt"
        threads:  config["<step_name>__get_organelle_SE_threads"]
        shell:
            "{params.command} "
            "-u {params.input_line} "
            "-o {params.output_dir} "
            "-t {threads} "
            "-F {params.organelle_type} "
            "-R {params.max_rounds} ; "
            "for fasta in $(ls {params.output_dir}/*.path_sequence.fasta); "
            "do cat $fasta >> {output.assembly}; "
            "done"
```
