# Ajouter un outil

Pour ajouter un outil dans WAW il faut tout d'abord rédiger le fichier de description au format yaml. Pour cela on peut se baser sur le template de base `tool_template.yaml` disponible dans le dossier `tools` de WAW.

## tool.yaml

Pour ajouter un outil dans WAW il faut tout d'abord rédiger le fichier de description au format yaml. Pour cela on peut se baser sur le template de base `tool_template.yaml` disponible dans le dossier `tools` de WAW.

### Infos générales

* L'`id` ne doit pas contenir d'espaces ou de tirets. Il peut cependant contenir des underscores.
* Le `name` lui est libre de contraintes et ne sera utilisé que dans l'interface graphique.
* La `description` est elle aussi libre de contraintes et reservé à l'interface.
* La `version` permet de garder la trace de la version de l'outil qui sera installée.
* Le `website` sera affiché dans l'interface.
* Le `git` correspond au lien vers le code source de l'outil (si disponible et pas nécessairement sur un git).
* La `documentation` correspond au lien vers la documentation de l'outil et sera visible dans l'interface pour potentiellement aider l'utilisateur à paramétrer l'outil selon ses besoins.
* L'`article` correspond au DOI de la publication de l'outil (si disponible) et un lien vers l'article sera visible dans l'interface.
* `multiqc` correspond à l'identifiant multiqc de l'outil quand il est disponible ([MultiQC supported tools](https://multiqc.info/#supported-tools)) sinon laisser `"custom"`.

``` yaml
{
  id: "my_tool",
  name: "My Tool",
  description: "My tool is a tool for ...",
  version: "1.0.0",
  website: "https://mytool.fr",
  git: "https://github.com/mytool",
  documentation: "https://mytool.fr/documentation",
  article: "10.1001/123456789",
  multiqc: "custom",
```

### Commandes

Dans la partie `commands` on va retrouver les différentes commandes qui vont correspondre aux règles Snakemake.
Les outils sont des briques qui vont être chainées entre elles, ils doivent donc correspondre à une tache sans forcément reprendre toutes les fonctions de l'outil. Par exemple pour un outil de mapping on sépare en 2 outils l'indexation du mapping (ce qui permet à l'utilisateur d'avoir le choix d'apporter son propre index ou de le construire). Les outils comprennent généralement une commande, ou deux quand par exemple il y a des différences entre la commande pour reads single end et reads paired end.

#### Informations sur la commande

* `name` : Nom de la commande, doit correspondre à l'id de l'outil. Dans le cas de commandes SE et PE doit être suffixé par `_SE` et `_PE`.
* `c_name` : Nom de la commande pour l'interface graphique.
* `output_dir` : Nom du dossier de sortie de la commande (généralement égal au nom de la commande).
* `category` : Catégorie de la commande (à choisir parmis les catégories du fichier `global.yaml` dans le dossier tools de WAW)
* `command` : Commande pour lancer l'outil (commande ou chemin vers la commande si l'outil n'est pas dans le PATH)

``` yaml
commands:
    [
      {
        name: "my_tool_SE",
        c_name: "My tool SE",
        output_dir: "my_tool_SE",
        category: "mapping",
        command: "/path_to_tool/my_tool",
        ...
```

#### Inputs et outputs de la commande

Les entrées et les sorties d'une commande sont listées et vont servir à faire le lien entre les différentes commandes d'un workflow. Pour chaque entrée et sortie on trouve :

* `name` : Le nom de l'input.
* `file` : Le nom de fichier de l'input (ou le nom supposé vu qu'il est inconnu).
* `type` : Le type de fichier (à choisir parmi la liste de data dans le fichier `global.yaml` dans le dossier tools de WAW).
* `description` : Une description du fichier.

``` yaml
inputs: [
  {
    name: "genome_fasta",
    file: "a_genome.fasta",
    type: "fasta",
    description: "Genome fasta file"
  },
  {
    name: "read",
    file: "{sample}.fastq.gz",
    type: "reads",
    description: "Reads"
  },
]
```

Dans le champs `file` il est utile de préciser si le wildard sample sera utilisé pour les fichiers en entrée et ceux en sortie. Cela permettra lors de la génération du workflow d'ajuster automatiquement les fonctions d'input et d'output avec des "expand" sur les samples si nécessaire (voir partie TODO).

``` yaml
outputs: [
  {
    name: "aligned_reads",
    file: "{sample}.bam",
    type: "bam",
    description: "Aligned reads"
  },
]
```

#### Options

Les options d'une commande correspondent aux différents paramètres de la commande. Il existe 10 types d'options :

* text (string)
* textArea (string)
* numeric (number)
* slider (number)
* select (choice in a list)
* radio (choice in a list)
* checkbox (boolean)
* input_file (file path)
* input_dir (directory path)
* output_dir (directory path)

et 2 types supplémentaires qui servent seulement à afficher une aide ou un lien dans l'interface graphique.

* help
* link

Les options ont chacune leurs attributs dont certains sont communs.

* `name` : le nom de l'option, il doit être formé du nom de la commande suivi d'un underscore et du nom de l'option.
* `type` : le type d'option
* `label` : le texte à afficher à côté de l'option dans l'interface.

Et d'autres attributs présent selon le type :

* `prefix` : le préfixe pour la ligne de commande.
* `min`, `max` et `step` qui sont présents dans les options numériques est peuvent être ignorés avec la valeur `NA`.
* `choices` : un dictionnaire de valeurs possibles avec en clé ce qui sera affiché dans l'interface et en valeur le paramètre.
* `value` : la valeur par défault de l'options.
* `href` : un lien à afficher dans le cas du type `link`.

exemple :

``` yaml
options: [
{
  name: "my_tool_SE_my_text",
  prefix: "",
  type: "text",
  value: "",
  label: "My text",
},
{
  name: "my_tool_SE_my_textarea",
  prefix: "",
  type: "textArea",
  value: "",
  label: "My text area",
},
{
  name: "my_tool_SE_threads",
  prefix: "-t",
  type: "numeric",
  value: 4,
  min: 1,
  max: NA,
  step: 1,
  label: "Numer of threads to use",
},
{
  name: "my_tool_SE_a_number",
  prefix: "",
  type: "slider",
  value: ,
  min: ,
  max: NA,
  step: NA,
  label: "",
},
{
  name: "my_tool_SE_my_choice",
  prefix: "",
  type: "select",
  choices:
    [
      choice A: "choice_A",
      choice B: "choice_B",
    ],
  value: "choice_A",
  label: "",
},
{
  name: "my_tool_SE_my_second_choice",
  prefix: "",
  type: "radio",
  choices:
    [
      choice A: "choice_A",
      choice B: "choice_B",
    ],
  value: "choice_A",
  label: "",
},
{
  name: "my_tool_SE_a_boolean",
  prefix: "",
  type: "checkbox",
  value: False,
  label: "",
},
{
  name: "my_tool_SE_a_file",
  type: "input_file",
  value: "",
  label: "",
},
{
  name: "my_tool_SE_a_directory",
  type: "input_dir",
  value: "",
  label: "",
},
{
  name: "my_tool_SE_another_directory",
  type: "output_dir",
  value: "",
  label: "",
},
{
  name: "my_tool_SE_a_help_message",
  type: "help",
  label: "",
},
{
  name: "my_tool_SE_a_link",
  type: "link",
  href: "",
  label: "",
},
]
```

#### Installations

Dans le champ installation on trouve un dictionnaire prenant les différentes dépendances à installer ainsi que l'outil lui-même. Chaque outil est associé à une liste de commandes bash. On peut glisser dans cette liste une ligne qui commence par `ENV` qui sera intégré dans le dockerfile pour par exemple ajouter un outil au PATH.

exemple :

``` yaml
install: {
  dependence_A:
  [
    "cd /opt/biotools",
    "wget http://depedence_A/dependence_A.tar.gz",
    "tar -xvzf dependence_A.tar.gz",
    "ENV PATH $PATH:/opt/biotools/dependence_A/bin",
    "rm dependence_A.tar.gz"
  ],
  tool_name:
  [
    "apt-get install my_tool",
  ]
  ...
```

#### Citations

Dans le champ `citations` on retrouve les citations en lien avec l'outil et les dépendances. Elle seront reprises dans le rapport d'analyse en fin de workflow.

exemple :

``` yaml
citations: {
  dependence_A:
  [
    "",
  ],
  tool_name:
  [
    "",
  ]
  ...
```

## tool.rule.snakefile

Le fichier snakefile d'un outil peut être prégénéré à l'aide du script `generate_tool_snakefile.py`. Le script doit être lancé depuis le dossier WAW et il suffit de lui passer l'id de l'outil (qui correspond au dossier de l'outil dans le dossier tools). A partir du fichier `tool.yaml` on écrit une partie de la ou les règles snakemake.

exemple :

``` console
    $ python3 generate_tool_snakefile.py fastp
```

Résultat :

``` python
if config['SeOrPe'] == 'PE':

    rule <step_name>__fastp_PE:
        input:
            **<step_name>__fastp_PE_inputs(),
        output:
            report_html = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/fastp_report_{sample}.html",
            report_json = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/fastp_report_{sample}.json",
            read_preprocessed = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_R1.fq.gz",
            read2_preprocessed = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_R2.fq.gz",
        params:
            output_dir = config["results_dir"] + "/" + config["<step_name>__fastp_PE_output_dir"],
            command = config["<step_name>__fastp_PE_command"],
            complexity_threshold = config["<step_name>__fastp_complexity_threshold"],
            report_title = config["<step_name>__fastp_report_title"],
            adapter_sequence = config["<step_name>__fastp_adapter_sequence"],
            adapter_sequence_R2_PE = config["<step_name>__fastp_adapter_sequence_R2_PE"],
            P = config["<step_name>__fastp_P"],
            correction = config["<step_name>__fastp_correction_PE"],
            low_complexity_filter = config["<step_name>__fastp_low_complexity_filter"],
            overrepresentation_analysis = config["<step_name>__fastp_overrepresentation_analysis"],
        log:
            config["results_dir"] + "/logs/" + config["<step_name>__fastp_PE_output_dir"] + "/{sample}_fastp_PE_log.txt"
        threads: config["<step_name>__fastp_threads"]
        shell:
            "{params.command} "
            "--thread {threads} "
            "--complexity_threshold {params.fastp_complexity_threshold} "
            "--report_title {params.fastp_report_title} "
            "--adapter_sequence {params.fastp_adapter_sequence} "
            "--adapter_sequence_r2 {params.fastp_adapter_sequence_R2_PE} "
            "-P {params.fastp_P} "
            "{params.fastp_correction} "
            "{params.fastp_low_complexity_filter} "
            "{params.fastp_overrepresentation_analysis} "
...
```

Le `rule_name` ainsi que la partie `input` sont totalement générés et n'ont normalement pas besoin d'ajustements.

La partie `output` peut nécessiter des ajustements, le but est de matcher les sorties de l'outil afin que Snakemake les détecte pour les passer à l'étape suivante. Ici on retrouve bien les sorties de l'outil fastp avec le wildcard sample qui permet de rendre générique cette règle (qui prendra donc des inputs avec le wildcard sample).

La partie `params` nécessite souvent des ajustements. En effet, le passage de paramètre étant différent pour chaque outil, il faut faire correspondre le paramètre qui sort de l'interface ou du fichier de config avec la ligne de commande. Pour cela il faut parfois utiliser des structures conditionnelles.

exemple:

``` python
# ligne générée
correction = config["<step_name>__fastp_correction_PE"],

# ligne adaptée
correction = "--correction " if config["<step_name>__fastp_correction_PE"] == True else "",
```

Dans ce cas le paramètre `fastp_correction_PE` est de type booléen. Or la ligne de commande de fastp nécessite la présence ou l'absence de `--correction` pour activer ou désactiver ce paramètre. On utilise alors la structure `A if (condition) else B` pour faire correspondre le paramètre et son passage en ligne de commande.

La partie `log` est normalement correcte. Il faut seulement vérifier la cohérence entre la présence ou l'abscence de wildcard dans les outputs et dans le log.

La partie `threads` ne nécessite pas d'intervention.

La partie `shell` à besoin quand à elle d'être vérifiée et ajustée à la main. Cette partie commence par `{params.command}` qui va reprendre la commande indiquée dans le fichier de description de l'outil. On retrouve ensuite chaque paramètre précédé de son préfixe lorsqu'il y en a un. Il faut alors vérifier la cohérence de cette ligne de commande par rapport à la documentation de l'outil et par rapport à la section `params` décrite précédemment. Il faut aussi ajouter les `inputs` et `outputs` à cette ligne de commande (selon la syntaxe de l'outil).
Enfin il faut gérer la sortie de l'outil et son log pour garder les informations d'exécution et les potentiels bug.

### Cas particuliers

#### Script

Dans le cas où l'on utilise un script, l'ensemble des paramètres de la règle (inputs, outputs, params, log et threads) seront transmis au script à travers une variable nommée `snakemake` dès lors que l'on remplace la partie `shell` par une partie `script` et qu'on donne l'adresse du script.

exemple :

``` python
script:
  config["<step_name>__mytool_script"]
```

#### Run

Dans certains cas il peut être pratique d'utiliser du python pour construire la ligne de commande de l'outil. On peut alors remplacer la partie `shell` par une partie `run` dans laquelle on utilise du python ainsi que la fonction `shell()` avec notre commande en attribut pour l'exectuer.

exemple :

``` python
run:
    my_command = "{params.command} "
    for file in input.files:
      my_command += "-i " + file

    shell(my_command)
```
