# Générer un workflow

Une fois qu'une brique workflow est prête (quand les fichiers de description et snakefile sont prêts, voir partie [Ajouter un workflow](ajout_workflow.md)) on peut générer le workflow.

Pour cela on peut utiliser le script `main.sh` à la racine de WAW. Ce script prends 5 arguments :

* `workflow yaml` : fichier yaml d'un workflow
* `output directory` : dossier dans lequel générer le workflow
* `SAG directory` : chemin vers l'outil SAG
* `WAW directory` : chemin vers l'outil WAW
* `local_config` optionnel : fichier local_config (voir partie [Local config](./Fonctionnement/local_config.md))

``` console
  $ bash main.sh workflows/Genome_Profile/Genome_Profile.yaml outputs/Genome_Profile/ ../sag/ .
```

Le script va s'occuper de lancer `generate_sag_yaml.py`, `generate_workflow.py` et SAG afin de générer tous les fichiers nécessaires à l'utilisation du workflow.
