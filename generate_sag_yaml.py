#!/usr/bin/python3
# This script will take a workflow yaml and generate SAG yaml configuration file
# Accepts two parameters: workflow yaml and waw_dir (needed if the script is not run within)
# Usage: ./generate_sag_yaml.py workflow_yaml [waw_dir]

from tools import *
import sys
import os

app_template = """
App:
  project: 
  general: { title: , skin: blue, menu_width: 230 }
  pages: []
  run:
    shiny_button: {
      name: RunPipeline,
      type: button,
      icon: play,
      class: btn btn-info,
      label: Run pipeline
    }
    program: snakemake
    options:
    - {name: '-s', type: value, value: '/workflow/Snakefile'}
    - {name: '--configfile', type: value, value: '/Results/params.yml'}
    - {name: '-d', type: value, value: /Results}
    - {name: '--cores', type: shiny, value: cores}
  RULEGRAPH:
  Report:
  download:
    shiny_button: {
      name: DownloadParams,
      type: button,
      class: btn btn-light,
      label: Download config file
    }
"""

tool_template = """
{
name: ,
title: , 
status: "success", 
content: []
}
"""

# Params to remove from the interface defined in the workflow yaml
def get_params_to_remove(pipeline, workFlowData = None):
    to_remove = list()
    workflow = workFlowData
    if "params_equals" in workflow.keys():
        params_equals = workflow["params_equals"]
        for line in params_equals:
            if (len(line) == 1):
                to_remove.append(line["remove"])
            if (len(line) == 2):
                to_remove.append(line["param_B"])
    return to_remove

def generate_tool(toolname, pipeline, step_name, waw_dir = "./", workFlowData = None):
    
    to_remove = get_params_to_remove(pipeline, workFlowData)

    template = yaml.load(tool_template, Loader=yaml.FullLoader)

    tool = read_yaml(waw_dir + "tools/" + toolname + "/" + toolname + ".yaml")

    template["name"] = tool["id"]
    template["title"] = tool["name"]

    for command in tool["commands"]:
        for option in command["options"]:
            option["name"] = step_name + "__" + option["name"]
            if option not in template["content"]:
                if option["name"] not in to_remove:
                    template["content"].append(option)

    template["content"].append(
        yaml.load(
            "{name: "
            + toolname
            + ",type: help, label: '"
            + template["title"]
            + ": "
            + tool["description"]
            + "'}"
            , Loader=yaml.FullLoader
        )
    )
    if tool["website"]:
        template["content"].append(
            yaml.load(
                "{name: "
                + toolname
                + ",type: link, label: 'Website : ', href: '"
                + tool["website"]
                + "'}"
                , Loader=yaml.FullLoader
            )
        )
    if tool["documentation"]:
        template["content"].append(
            yaml.load(
                "{name: "
                + toolname
                + ",type: link, label: 'Documentation : ', href: '"
                + tool["documentation"]
                + "'}"
                , Loader=yaml.FullLoader
            )
        )
    if tool["article"]:
        template["content"].append(
            yaml.load(
                "{name: "
                + toolname
                + ",type: link, label: 'Paper : ', href: 'https://doi.org/"
                + tool["article"]
                + "'}"
                , Loader=yaml.FullLoader
            )
        )

    return template


def generate_workflow_params(pipeline, waw_dir = "./", workflowData = None):
    workflow = workflowData
    template = yaml.load(tool_template, Loader=yaml.FullLoader)

    template["name"] = "global_params"
    template["title"] = "Global parameters :"
    for option in workflow["options"]:
        template["content"].append(option)

    if "input" in workflow:
          for raw in workflow["input"]:
             #print(raw)
             raw_input = read_yaml(waw_dir + "raw_inputs/" + raw + ".yaml")
             for option in raw_input["options"]:
                 template["content"].append(option)

    template["content"].append(
        yaml.load("{name: memo, type: textArea, value: '', label: 'Text area for the user'}", Loader=yaml.FullLoader)
    )

    return template


def generate_sag_yaml(workflow_yaml, template, waw_dir="./"):
    # reading template
    template = yaml.load(template, Loader=yaml.FullLoader)

    config = read_yaml(workflow_yaml)
    pipeline = config["name"]

    #template["App"]["project"] = outdir + pipeline + "/sagApp/"
    template["App"]["general"]["title"] = config["name"]

    # adding global parameters
    # page
    template["App"]["pages"].append(
        yaml.load(
            '{icon: pencil, label: "Global parameters", name: "global_params", boxes: []}' , Loader=yaml.FullLoader
        )
    )
    # predefined boxes

    template["App"]["pages"][0]["boxes"].append(generate_workflow_params(pipeline, waw_dir, config))

    page_number = 1

    # for each step of the pipeline, create panel
    for step in config["steps"]:
        # page

        step_title = ""

        tools = step["tools"]
        # ignore null
        if "null" in tools:
            tools.remove("null")
        # if several choices, keep step title
        if len(tools) > 1:
            step_title = step["title"]
        # else specify with tool
        else:
            step_title = step["title"] + ": " + tools[0]

        template["App"]["pages"].append(
            yaml.load(
                '{icon: pencil, label: "'
                + step_title
                + '", name: "'
                + step["name"]
                + '", default:  "'
                + step["default"]
                + '" ,boxes:[]}'
                , Loader=yaml.FullLoader
            )
        )
        # for each tool in a step, create a box
        for tool in step["tools"]:
            # predefined boxes
            template["App"]["pages"][page_number]["boxes"].append(generate_tool(tool,pipeline,step["name"], waw_dir, config))
        page_number += 1

    return template


def main():

    if len(sys.argv) == 2:
        # generate
        data = generate_sag_yaml(sys.argv[1], app_template)
        print(data)
        #write_yaml("workflows/" + sys.argv[1] + "/sag.yaml", data)

    elif len(sys.argv) > 2:
        # generate
        data = generate_sag_yaml(sys.argv[1], app_template, sys.argv[2])
        print(data)
        #write_yaml(sys.argv[2] + sys.argv[1] + "/sag.yaml", data)

    else:
        exit("""Needs one argument minimum : workflow yaml
        Accepts two parameters: workflow yaml, and waw_dir (needed if the script is not run within)
        """)

if __name__ == "__main__":
    main()

