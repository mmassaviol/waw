#!/bin/bash
# This script will take a tool yaml and check its validity
# Needs 2 arguments: workflow_yaml reference_yaml
# Usage: ./check_workflow_yaml.sh myworkflow_yaml workflows/workflow.schema.yaml

python -c 'from tools import *; validate_yaml("'$1'","'$2'")'

if [[ $? == 0 ]];
then
    echo $1 "is valid"
fi