import os
import re
import sys

def raw_bowtie_index(results_dir, bowtie_index):
    out = dict()
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [join(bowtie_index, f) for f in listdir(bowtie_index) if isfile(join(bowtie_index, f))]
    out["bowtie_index"] = onlyfiles

    return(out)

#print(raw_bowtie_index(sys.argv[1],sys.argv[2])