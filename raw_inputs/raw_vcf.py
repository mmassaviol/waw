import os
import re
import sys

def raw_vcf(results_dir, sample_dir):
    samples = list()
    suffixes = list()
    dicoSamples = dict() # sample_name: file(s)
    
    files = [ f for f in os.listdir(sample_dir) if not f.startswith('.') ]
    regex = re.compile(r"^(.+?)(\.(bcf|vcf).*)(?<!tbi)$")
    for file in files:
        res = re.match(regex, file)
        if res:
            if res.group(1) not in samples:
                samples.append(res.group(1))
                suffixes.append(res.group(2))

            if res.group(1) not in dicoSamples.keys():
                dicoSamples[res.group(1)] = list()

            dicoSamples[res.group(1)].append(file)

    if (len(set(suffixes)) == 1 ):
        suffix = list(set(suffixes))[0]
        if os.path.exists(results_dir):
           with open(results_dir+"/samples.tsv","w") as sampleTab:
               sampleTab.write("sample\tvcf_file")
               for sample in sorted(samples):
                   sampleTab.write("\n"+sample+"\t"+"\t".join(sorted(dicoSamples[sample])))

        out = {'samples': sorted(samples), 'sample_suffix': suffix, 'dico': dicoSamples}

        out ["vcf"] = os.path.join(sample_dir,"{sample}"+suffix)

        return out
    elif len(set(suffixes)) > 1:
        exit("Files have different suffixes:" + ','.join(suffixes))
    else:
        exit("No VCF or BCF file found")

#print(raw_vcf(sys.argv[1],sys.argv[2]))