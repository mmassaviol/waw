import os
import re
import sys

def raw_salmon_index(results_dir, salmon_index):
    out = dict()
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [join(salmon_index, f) for f in listdir(salmon_index) if isfile(join(salmon_index, f))]
    out["salmon_index"] = onlyfiles

    return(out)

#print(salmon_index_index(sys.argv[1],sys.argv[2])