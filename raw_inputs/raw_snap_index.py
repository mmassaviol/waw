import os
import re
import sys

def raw_snap_index(results_dir, snap_index):
    out = dict()
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [join(snap_index, f) for f in listdir(snap_index) if isfile(join(snap_index, f))]
    out["snap_index"] = onlyfiles

    return(out)

#print(raw_snap_index(sys.argv[1],sys.argv[2]) )