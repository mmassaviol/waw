import os
import re
import sys

def raw_bwa_mem_2_index(results_dir, bwa_mem_2_index):
    out = dict()
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [join(bwa_mem_2_index, f) for f in listdir(bwa_mem_2_index) if isfile(join(bwa_mem_2_index, f))]
    out["bwa_mem_2_index"] = onlyfiles

    return(out)

#print(bwa_mem_2_index_index(sys.argv[1],sys.argv[2])