#!/bin/bash
# This script will take a tool yaml and check its validity
# Needs 2 arguments: tool_yaml reference_yaml
# Usage: ./check_tool_yaml.sh mytool_yaml tools/tool.schema.yaml

python -c 'from tools import *; validate_yaml("'$1'","'$2'")'

if [[ $? == 0 ]];
then
    echo $1 "is valid"
fi