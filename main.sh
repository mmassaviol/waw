#!/bin/bash
# This script will take an workflow name and generate workflow files and SAG application
# Needs 5 arguments: workflow yaml, output directory, SAG directory, waw_dir, [local_config]
# waw_dir is by default the current dir
# local_config is a file with some configuration usefull for cluster execution or other type of specificity
# Usage: ./main.sh workflow_yaml output_dir sag_dir local_config
# N.B. output_dir must be  global not relative to the current i.e. /home/toto/test/
# sag_dir is the location where sag generator is installed (from https://gitlab.mbb.univ-montp2.fr/jlopez/sag.git)

WORKFLOW_YAML=$1
OUTPUT_DIR=$2
SAG_DIR=$3
WAW_DIR=$4

if [[ ! $# -ge 4 ]]; then
  echo "Need at least 4 arguments: workflow_yaml output_dir sag_dir waw_dir (local_config)"
  exit
fi

echo "Generate SAG recipe"
python3 generate_sag_yaml.py $WORKFLOW_YAML $WAW_DIR > $OUTPUT_DIR/sag.yaml
echo "Done"

echo "Generate Workflow"
if [[ $# -eq 5 ]]
then
  python3 generate_workflow.py $WORKFLOW_YAML $OUTPUT_DIR $WAW_DIR $5;
else
  python3 generate_workflow.py $WORKFLOW_YAML $OUTPUT_DIR $WAW_DIR
fi
echo "Done"

echo "Generate SAG app"
cd $SAG_DIR &&
Rscript main.R $OUTPUT_DIR/sag.yaml $OUTPUT_DIR/sagApp &&
echo "Done"