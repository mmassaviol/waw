# WAW (Workflow Application Wrapper)

WAW est un framework de génération automatique de workflows. Ce framework permet de générer un workflow ainsi que son environnement d'exécution et de l'utiliser avec ou sans interface graphique sur différentes infrastructures (machine personnelle, un serveur de calcul, un cluster, un cloud, etc).

Ce framework intègre 80 briques d'applications bioinformatiques assemblables pour former une grande variété de workflows d'analyse de données.

Ce framework repose sur plusieurs technologies existantes comme [Snakemake](https://snakemake.readthedocs.io/en/stable/), [Docker](https://www.docker.com/), [R Shiny](https://shiny.rstudio.com/) et [MultiQC](https://multiqc.info/).

Pour plus d'informations voir la documentation : **!! En cours de rédaction !!**