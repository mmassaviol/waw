#!/bin/bash

sudo apt-get update

##### Docker install #####
sudo apt-get install -y docker.io

##### nginx install #####
sudo apt-get install -y nginx

###### Singularity install #######
sudo apt-get install -y build-essential libssl-dev uuid-dev libgpgme11-dev \
     squashfs-tools libseccomp-dev wget pkg-config git cryptsetup debootstrap

#Install Go https://golang.org/ and extract to /usr/local/go
wget https://dl.google.com/go/go1.13.linux-amd64.tar.gz

sudo tar --directory=/usr/local -xzvf go1.13.linux-amd64.tar.gz
export PATH=/usr/local/go/bin:$PATH

#Change version if needed
wget https://github.com/singularityware/singularity/releases/download/v3.8.3/singularity-3.8.3.tar.gz
tar -xzvf singularity-3.8.3.tar.gz

cd singularity-3.8.3
./mconfig
cd builddir
make
sudo make install
 
#You can test your installation
singularity run library://godlovedc/funny/lolcow