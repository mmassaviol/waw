rule <step_name>__nanoplot_fastq:
	input:
		**<step_name>__nanoplot_fastq_inputs(),
	output: 
		nanoplot_stats = config["results_dir"] + "/" + config["<step_name>__nanoplot_fastq_output_dir"] + "/NanoStats.txt",
	params: 
		output_dir = config["results_dir"] + "/" + config["<step_name>__nanoplot_fastq_output_dir"]+ "/",
		command = config["<step_name>__nanoplot_fastq_command"],
		maxlength = config["<step_name>__nanoplot_fastq_maxlength"],
		minlength = config["<step_name>__nanoplot_fastq_minlength"],
		minqual = config["<step_name>__nanoplot_fastq_minqual"],
		in_line = lambda w, input: " ".join([" ".join([input.read[i]]) for i in range(0,len(input.read))])
	log: 
		config["results_dir"] + "/logs/" + config["<step_name>__nanoplot_fastq_output_dir"] + "/nanoplot_fastq_log.txt"
	threads: 
		config["<step_name>__nanoplot_fastq_threads"]
	shell: 
		"{params.command} "
		"-t {threads} "
		"--outdir {params.output_dir} "
		"--fastq '{params.in_line}' "
		"--maxlength {params.maxlength} "
		"--minlength {params.minlength} "
		"--minqual {params.minqual} "
		"|& tee {log}; "
		"for i in $(ls {params.output_dir}/*.png); do  name=$(basename $i .png); mv $i {params.output_dir}/${{name}}_mqc.png ; done"