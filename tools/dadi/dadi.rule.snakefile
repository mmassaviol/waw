rule <step_name>__dadi:
    input:
        **<step_name>__dadi_inputs(),
    output: 
        full = config["results_dir"] + "/" + config["<step_name>__dadi_output_dir"] + "/dadi-full.txt",
        easy = config["results_dir"] + "/" + config["<step_name>__dadi_output_dir"] + "/dadi-easy.txt",
        modelsfs = config["results_dir"] + "/" + config["<step_name>__dadi_output_dir"] + "/dadi-modelsfs.txt",
        img = config["results_dir"] + "/" + config["<step_name>__dadi_output_dir"] + "/dadi_"+ config["<step_name>__dadi_model_list"] +"_mqc.png",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__dadi_output_dir"]+ "/",
        masked = "--masked" if config["<step_name>__dadi_masked"] else "",
        folded = "--folded" if config["<step_name>__dadi_folded"] else "",
        projected = "--projected" if config["<step_name>__dadi_projected"] else "",
        #nameoutput = config["<step_name>__dadi_nameoutput"],
        datatype = config["<step_name>__dadi_datatype"],
        namepop1 = config["<step_name>__dadi_namepop1"],
        namepop2 = config["<step_name>__dadi_namepop2"],
        proj_sample = config["<step_name>__dadi_proj_sample"],
        grid_points = "--grid_points " + config["<step_name>__dadi_grid_points"] if config["<step_name>__dadi_grid_points"]!="" else "",
        model_list = config["<step_name>__dadi_model_list"],
        maxiterGlobal = config["<step_name>__dadi_maxiterGlobal"],
        accept = config["<step_name>__dadi_accept"],
        visit = config["<step_name>__dadi_visit"],
        Tini = config["<step_name>__dadi_Tini"],
        no_local_search = config["<step_name>__dadi_no_local_search"],
        local_method = config["<step_name>__dadi_local_method"],
        maxiterLocal = config["<step_name>__dadi_maxiterLocal"],
        img_out = config["results_dir"] + "/" + config["<step_name>__dadi_output_dir"] + "/dadi_"+ config["<step_name>__dadi_model_list"] +".png",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__dadi_output_dir"] + "/dadi_log.txt"
    threads: 1
    shell: 
        "python3 /sources/dadi_sources/dadi-wrapper/dadi_inference_dualanneal.py "
        "-i {input.fs_file_name} "
        "{params.masked} "
        "{params.folded} "
        "{params.projected} "
        #"--nameoutput {params.nameoutput} "
        #"--fs_file_name {params.fs_file_name} "
        #"--pop_file_name {params.pop_file_name} "
        "--datatype {params.datatype} "
        "--namepop1 {params.namepop1} "
        "--namepop2 {params.namepop2} "
        "--proj_sample {params.proj_sample} "
        "{params.grid_points} "
        "--model_list {params.model_list} "
        "|& tee {log};"
        "mv {params.img_out} {output.img}"
