rule <step_name>__htseq_count:
    input:
        **<step_name>__htseq_count_inputs()
    output:
        stats = config["results_dir"]+'/' + config["<step_name>__htseq_count_output_dir"] + '/{sample}.txt',
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__htseq_count_output_dir"] + '/{sample}_htseq_count_log.txt'
    params:
        command = config["<step_name>__htseq_count_command"],
        htseq_count_minaqual = config["<step_name>__htseq_count_minaqual"],
        htseq_stranded = config["<step_name>__htseq_stranded"],
        htseq_orientation = config["<step_name>__htseq_orientation"],
        output_dir = config["results_dir"]+'/'+config["<step_name>__htseq_count_output_dir"]
    shell:
        "{params.command} -f bam -a  {params.htseq_count_minaqual}  -s {params.htseq_stranded} -r {params.htseq_orientation} {input.bam} {input.gff_file} 2> >(tee -a {log} >&2)  > {output.stats}"
