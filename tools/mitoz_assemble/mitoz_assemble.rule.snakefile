if config["SeOrPe"] == "SE":

    rule <step_name>__mitoz_assemble_SE:
        input:
            **<step_name>__mitoz_assemble_SE_inputs()
        output:
            assemble = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_SE_output_dir"] + "/mitoz_assemble_SE.result/mitoz_assemble_SE.assemble.scafSeq",
            mitogenome = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_SE_output_dir"] + "/mitoz_assemble_SE.result/work71.mitogenome.fa"
        log: config["results_dir"]+'/logs/' + config["<step_name>__mitoz_assemble_SE_output_dir"] + '/mitoz_assemble_log.txt'
        threads: config["<step_name>__mitoz_assemble_threads"]
        params:
            command = config["<step_name>__mitoz_assemble_SE_command"],
            output_dir = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_SE_output_dir"],
            prefix = "mitoz_assemble_SE",
            read_length = config["<step_name>__mitoz_assemble_read_length"],
            clade = config["<step_name>__mitoz_assemble_clade"],
            in_line = lambda w, input: " ".join(["--fastq1 "+fq for fq in input.read])
        shell:
            "mkdir -p {params.output_dir} && "
            "cd {params.output_dir}; "
            "{params.command} "
            "{params.inline} "
            "--fq_size 0 "
            "--thread_number {threads} "
            "--run_mode 2 "
            "--fastq_read_length {params.read_length} "
            "--outprefix {params.prefix} "
            "--clade {params.clade} "
            "|& tee {log}; "
            "mv {params.assemble} {output.assemble}"


elif config["SeOrPe"] == "PE":

    rule <step_name>__mitoz_assemble_PE:
        input:
            **<step_name>__mitoz_assemble_PE_inputs()
        output:
            assemble = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_PE_output_dir"] + "/work71.scafSeq",
            mitogenome = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_PE_output_dir"] + "/mitoz_assemble_PE.result/work71.mitogenome.fa"
        log: config["results_dir"]+'/logs/mitoz_assemble/mitoz_assemble_log.txt'
        threads: config["<step_name>__mitoz_assemble_threads"]
        params:
            command = config["<step_name>__mitoz_assemble_PE_command"],
            output_dir = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_PE_output_dir"],
            prefix = "mitoz_assemble_PE",
            read_length = config["<step_name>__mitoz_assemble_read_length"],
            insert_size = config["<step_name>__mitoz_assemble_insert_size_PE"],
            clade = config["<step_name>__mitoz_assemble_clade"],
            assemble = config["results_dir"] + "/" + config["<step_name>__mitoz_assemble_PE_output_dir"] + "/mitoz_assemble_PE.tmp/mitoz_assemble_PE.assembly/work71.scafSeq",
            in_line = lambda w, input: " ".join([" ".join(["--fastq1 "+input.read[i],"--fastq2 "+input.read2[i]]) for i in range(0,len(input.read))])
        shell:
            "mkdir -p {params.output_dir} && "
            "cd {params.output_dir}; "
            "{params.command} "
            "{params.in_line} "
            "--fq_size 0 "
            "--thread_number {threads} "
            "--run_mode 2 "
            "--fastq_read_length {params.read_length} "
            "--insert_size {params.insert_size} "
            "--outprefix {params.prefix} "
            "--clade {params.clade} "
            "|& tee {log}; "
            "mv {params.assemble} {output.assemble}"
