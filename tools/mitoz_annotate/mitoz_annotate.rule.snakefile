#if config["SeOrPe"] == "SE": #SeOrPe will not be defined if we are using fast5_dir followed by guppy as the source of reads
if "<step_name>__mitoz_annotate_SE_inputs" in globals():
    rule <step_name>__mitoz_annotate_SE:
        input:
            **<step_name>__mitoz_annotate_SE_inputs()
        output:
            annotate = config["results_dir"] + "/" + config["<step_name>__mitoz_annotate_SE_output_dir"] + "/mitoz_annotate_SE.result/mitoz_annotate_SE_mitoscaf.fa.gbf",
            #circos_png = config["results_dir"]+"/mitoz_annotate_SE/circos_mqc.png",
            circos_bam = config["results_dir"] + "/" + config["<step_name>__mitoz_annotate_SE_output_dir"] + "/circos.bam"
        log: config["results_dir"]+'/logs/' + config["<step_name>__mitoz_annotate_SE_output_dir"] + '/mitoz_annotate_log.txt'
        threads: config["<step_name>__mitoz_annotate_threads"]
        params:
            command = config["<step_name>__mitoz_annotate_SE_command"],
            tmp_circos = config["results_dir"]+ "/" + config["<step_name>__mitoz_annotate_SE_output_dir"] + "/mitoz_annotate_SE.tmp/mitoz_annotate_SE.annotation/visualization/circos.sorted.bam",
            circos_dir = config["results_dir"]+ "/" + config["<step_name>__mitoz_annotate_SE_output_dir"] + "/mitoz_annotate_SE.tmp/mitoz_annotate_SE.annotation/visualization/",
            results_dir = config["results_dir"]+ "/" + config["<step_name>__mitoz_annotate_SE_output_dir"] ,
            prefix = "mitoz_annotate_SE",
            clade = config["<step_name>__mitoz_annotate_clade"],
            sample_suffix = config["sample_suffix"]
        shell:
            "cd {params.results_dir}; "
            "cat {input.read} > {params.results_dir}/reads_R1{params.sample_suffix} "
            "&& {params.command} "
            "--fastq1 {params.results_dir}/reads_R1{params.sample_suffix} "
            #"--fq_size 0 "
            "--fastafile {input.fasta} "
            "--thread_number {threads} "
            "--outprefix {params.prefix} "
            "--clade {params.clade} "
            "|& tee {log} && mv {params.tmp_circos} {output.circos_bam} && "
            # multiple mito case
            "cd {params.circos_dir} && "
            "sed -i \"s/circos.png/circos_mqc.png/g\" circos.conf && "
            "sed -i \"s|{params.circos_dir}||g\" circos.conf && "
            "if [ $(grep \"mt[0-9]\{{1,\}}\" circos.fa.gc.txt | cut -f 1 | uniq | wc -l) -gt 1 ] \n"
            "then \n"
            "   for mito in $(grep \"mt[0-9]\{{1,\}}\" circos.fa.gc.txt | cut -f 1 | uniq) \n"
            "   do \n"
            "   cd {params.circos_dir} && "
            "   echo \"Treating mito $mito\" && "
            "   sed \"s/circos/circos$mito/g\" circos.conf > circos$mito.conf && "
            "   grep $mito circos.depth.txt > circos$mito.depth.txt && "
            "   grep $mito circos.karyotype.txt > circos$mito.karyotype.txt && "
            "   grep $mito circos.gene.text.txt > circos$mito.gene.text.txt && "
            "   cat circos.strand.text.txt > circos$mito.strand.text.txt && "
            "   grep $mito circos.fa.gc.txt > circos$mito.fa.gc.txt && "
            "   grep $mito circos.features.txt > circos$mito.features.txt && "
            "   cd .. && "
            "   circos -conf visualization/circos$mito.conf && "
            "   mv visualization/circos${{mito}}_mqc.png {params.results_dir}\n"
            "   done \n"
            "else \n"
            "   mv circos.png circos_mqc.png \n"
            "fi \n"
            "rm {params.results_dir}/reads_R1{params.sample_suffix} "


#elif config["SeOrPe"] == "PE":
elif if "<step_name>__mitoz_annotate_PE_inputs" in globals():
    rule <step_name>__mitoz_annotate_PE:
        input:
            **<step_name>__mitoz_annotate_PE_inputs()
        output:
            annotate = config["results_dir"] + "/" + config["<step_name>__mitoz_annotate_PE_output_dir"] + "/mitoz_annotate_PE.result/mitoz_annotate_PE_mitoscaf.fa.gbf",
            circos_bam = config["results_dir"] + "/" + config["<step_name>__mitoz_annotate_PE_output_dir"] + "/circos.bam",
            #circos_png = config["results_dir"]+"/mitoz_annotate_PE/circos_mqc.png",
        log: config["results_dir"]+'/logs/' + config["<step_name>__mitoz_annotate_PE_output_dir"] + '/mitoz_annotate_log.txt'
        threads: config["<step_name>__mitoz_annotate_threads"]
        params:
            command = config["<step_name>__mitoz_annotate_PE_command"],
            tmp_circos = config["results_dir"] + "/" + config["<step_name>__mitoz_annotate_PE_output_dir"] + "/mitoz_annotate_PE.tmp/mitoz_annotate_PE.annotation/visualization/circos.sorted.bam",
            circos_dir = config["results_dir"] + "/" + config["<step_name>__mitoz_annotate_PE_output_dir"] + "/mitoz_annotate_PE.tmp/mitoz_annotate_PE.annotation/visualization/",
            results_dir = config["results_dir"] + "/" + config["<step_name>__mitoz_annotate_PE_output_dir"],
            prefix = "mitoz_annotate_PE",
            clade = config["<step_name>__mitoz_annotate_clade"],
            sample_suffix = config["sample_suffix"]
        shell:
            "cd {params.results_dir}; "
            "cat {input.read} > {params.results_dir}/reads_R1{params.sample_suffix} "
            "&& cat {input.read2} > {params.results_dir}/reads_R2{params.sample_suffix} "
            "&& {params.command} "
            "--fastq1 {params.results_dir}/reads_R1{params.sample_suffix} "
            "--fastq2 {params.results_dir}/reads_R2{params.sample_suffix} "
            #"--fq_size 0 "
            "--fastafile {input.fasta} "
            "--thread_number {threads} "
            "--outprefix {params.prefix} "
            "--clade {params.clade} "
            "|& tee {log} && mv {params.tmp_circos} {output.circos_bam} && "
            # multiple mito case
            "cd {params.circos_dir} && "
            "sed -i \"s/circos.png/circos_mqc.png/g\" circos.conf && "
            "sed -i \"s|{params.circos_dir}||g\" circos.conf && "
            "if [ $(grep \"mt[0-9]\{{1,\}}\" circos.fa.gc.txt | cut -f 1 | uniq | wc -l) -gt 1 ] \n"
            "then \n"
            "   for mito in $(grep \"mt[0-9]\{{1,\}}\" circos.fa.gc.txt | cut -f 1 | uniq) \n"
            "   do \n"
            "   cd {params.circos_dir} && "
            "   echo \"Treating mito $mito\" && "
            "   sed \"s/circos/circos$mito/g\" circos.conf > circos$mito.conf && "
            "   grep $mito circos.depth.txt > circos$mito.depth.txt && "
            "   grep $mito circos.karyotype.txt > circos$mito.karyotype.txt && "
            "   grep $mito circos.gene.text.txt > circos$mito.gene.text.txt && "
            "   cat circos.strand.text.txt > circos$mito.strand.text.txt && "
            "   grep $mito circos.fa.gc.txt > circos$mito.fa.gc.txt && "
            "   grep $mito circos.features.txt > circos$mito.features.txt && "
            "   cd .. && "
            "   circos -conf visualization/circos$mito.conf && "
            "   mv visualization/circos${{mito}}_mqc.png {params.results_dir}\n"
            "   done \n"
            "else \n"
            "   mv circos.png {params.results_dir}/circos_mqc.png \n"
            "fi \n"
            "rm {params.results_dir}/reads_R1{params.sample_suffix} "
            "&& rm {params.results_dir}/reads_R2{params.sample_suffix} "
