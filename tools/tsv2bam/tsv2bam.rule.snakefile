def link_reads_tsv2bam():
    if (config["SeOrPe"]=="PE"):
        if (config["demultiplexing"]=="process_radtags"):
            command = ""
            prefix_in = os.path.dirname(<step_name>__tsv2bam_inputs()["PE_read2"]) + "/"
        else:
            command = "mkdir -p " + os.path.dirname(<step_name>__tsv2bam_inputs()["PE_read2"]) + " && "
            prefix_in = config["sample_dir"] + "/"
        prefix_out = os.path.dirname(<step_name>__tsv2bam_inputs()["PE_read2"]) + "/"
        command += 'ln -sf '+prefix_in+'{sample}_R1'+config["sample_suffix"]+' '+prefix_out+'{sample}.1.fq.gz && '
        command += 'ln -sf '+prefix_in+'{sample}_R2'+config["sample_suffix"]+' '+prefix_out+'{sample}.2.fq.gz && '
    return command

def link_catalogs_tsv2bam():
    alleles_file = os.path.basename(<step_name>__tsv2bam_inputs()["alleles"])
    snps_file = os.path.basename(<step_name>__tsv2bam_inputs()["snps"])
    tags_file = os.path.basename(<step_name>__tsv2bam_inputs()["tags"])

    sample_alleles_file = os.path.basename(<step_name>__tsv2bam_inputs()["sample_alleles"])
    sample_snps_file = os.path.basename(<step_name>__tsv2bam_inputs()["sample_snps"])
    sample_tags_file = os.path.basename(<step_name>__tsv2bam_inputs()["sample_tags"])

    prefix_out = os.path.dirname(<step_name>__tsv2bam_inputs()["tsv"]) + "/"
    command = 'cd ' + prefix_out + ' && '
    command += 'ln -sf ' + <step_name>__tsv2bam_inputs()["alleles"] + ' ' + alleles_file + ' && '
    command += 'ln -sf ' + <step_name>__tsv2bam_inputs()["snps"] + ' ' + snps_file + ' && '
    command += 'ln -sf ' + <step_name>__tsv2bam_inputs()["tags"] + ' ' + tags_file + ' && '

    command += 'ln -sf ' + <step_name>__tsv2bam_inputs()["sample_alleles"] + ' ' + sample_alleles_file + ' && '
    command += 'ln -sf ' + <step_name>__tsv2bam_inputs()["sample_snps"] + ' ' + sample_snps_file + ' && '
    command += 'ln -sf ' + <step_name>__tsv2bam_inputs()["sample_tags"] + ' ' + sample_tags_file + ' && '
    return command

rule <step_name>__tsv2bam:
    input:
        **<step_name>__tsv2bam_inputs()
    output:
        bam = config["results_dir"]+"/"+config["<step_name>__tsv2bam_output_dir"]+"/{sample}.bam"
    params:
        command = config["<step_name>__tsv2bam_command"],
        sstacks_dir = config["results_dir"] + "/" + os.path.dirname(<step_name>__tsv2bam_inputs()["tsv"]) ,
        pe_reads_dir = "--pe-reads-dir " + os.path.dirname(<step_name>__tsv2bam_inputs()["PE_read2"]) if config["SeOrPe"] == "PE" else "",
        out_name = os.path.dirname(<step_name>__tsv2bam_inputs()["tsv"]) + "/{sample}.matches.bam",
        link_reads = link_reads_tsv2bam(),
        link_catalogs = link_catalogs_tsv2bam()
    threads: 
        config["<step_name>__tsv2bam_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__tsv2bam_output_dir"] + "/{sample}_tsv2bam_log.txt"
    shell:
        "{params.link_reads} "
        "{params.link_catalogs} "
        "{params.command} "
        "-t {threads} "
        "-P {params.sstacks_dir} "
        "-s {wildcards.sample} "
        "{params.pe_reads_dir} "
        "|& tee {log} "
        "&& mv {params.out_name} {output}"