<step_name>__phylip2fasta_fasta_name = os.path.basename(<step_name>__phylip2fasta_inputs()["phylip"]).split(".")[0]

rule <step_name>__phylip2fasta:
    input:
        **<step_name>__phylip2fasta_inputs(),
    output: 
        fasta = config["results_dir"] + "/" + config["<step_name>__phylip2fasta_output_dir"] + "/" + <step_name>__phylip2fasta_fasta_name + ".fasta",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__phylip2fasta_output_dir"]+ "/",
        command = config["<step_name>__phylip2fasta_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__phylip2fasta_output_dir"] + "/phylip2fasta_log.txt"
    script: 
        config["<step_name>__phylip2fasta_script"]