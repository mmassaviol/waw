from Bio import SeqIO

records = SeqIO.parse(snakemake.input.phylip, "phylip")
count = SeqIO.write(records, snakemake.output.fasta, "fasta")