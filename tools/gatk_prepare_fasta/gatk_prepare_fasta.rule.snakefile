rule <step_name>__gatk_prepare_fasta:
    input:
        **<step_name>__gatk_prepare_fasta_inputs(),
    output:
        fasta = config["results_dir"] + "/" + config["<step_name>__gatk_prepare_fasta_output_dir"] + "/ref.fa",
        index = config["results_dir"] + "/" + config["<step_name>__gatk_prepare_fasta_output_dir"] + "/ref.fa.fai",
        fadict = config["results_dir"] + "/" + config["<step_name>__gatk_prepare_fasta_output_dir"] + "/ref.dict"
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__gatk_prepare_fasta_output_dir"] + "/gatk_prepare_fasta_log.txt"
    shell:
        "if [[ {input.genome_fasta} =~ \.gz$ ]]; "
        "then gunzip {input.genome_fasta} -c > {output.fasta} ; "
        "else ln -s {input.genome_fasta} {output.fasta}; "
        "fi; "
        "java -jar /opt/biotools/bin/picard.jar CreateSequenceDictionary "
        "R={output.fasta} "
        "O={output.fadict} && "
        "samtools faidx {output.fasta} -o {output.index}"