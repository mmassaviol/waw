rule <step_name>__Picard_MarkDuplicates:
    input:
        **<step_name>__Picard_MarkDuplicates_inputs(),
    output:
        bam = temp(config["results_dir"]+"/"+config["<step_name>__Picard_MarkDuplicates_output_dir"]+"/{sample}.mapped.dedup.bam"),
        sorted_bam = config["results_dir"]+"/"+config["<step_name>__Picard_MarkDuplicates_output_dir"]+"/{sample}.mapped.dedup.sorted.bam",
        bai = config["results_dir"]+"/"+config["<step_name>__Picard_MarkDuplicates_output_dir"]+"/{sample}.mapped.dedup.sorted.bam.bai",
        metric = config["results_dir"]+"/"+config["<step_name>__Picard_MarkDuplicates_output_dir"]+"/duplicate_metrics_{sample}.txt"
    #shadow:
    #    "full" # allow for the automatic removal of temporary files not removed otherwise
    params:
        command = config["<step_name>__Picard_MarkDuplicates_command"],
        remove_all_duplicates = config["<step_name>__Picard_MarkDuplicates_remove_all_duplicates"],
        m_samtools_sort = config["<step_name>__Picard_MarkDuplicates_samtools_memory"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__Picard_MarkDuplicates_output_dir"] + "/{sample}_Picard_MarkDuplicates_log.txt"
    threads:
        config["<step_name>__Picard_MarkDuplicates_threads"]
    shell:
        "{params.command} "
        "I={input.bam_in} "
        "O={output.bam} "
        "M={output.metric} "
        "REMOVE_DUPLICATES={params.remove_all_duplicates} "
        "|& tee {log} "
        # Sorting BAM
        "&& samtools sort "
        "-m {params.m_samtools_sort}G "
        "-O BAM "
        "-l 1 "
        "-@ {threads} "
        "-o {output.sorted_bam} "
        "{output.bam} "
        "|& tee -a {log} "
        # Indexing BAM
        "&& samtools index "
        "-@ {threads} "
        "{output.sorted_bam} "
        "|& tee -a {log}"
