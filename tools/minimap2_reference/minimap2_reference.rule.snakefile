rule <step_name>__minimap2_reference:
    input:
        **<step_name>__minimap2_reference_inputs(),
    output: 
        reads_mapping = config["results_dir"] + "/" + config["<step_name>__minimap2_reference_output_dir"] + "/reads.paf.gz",
    params:
        command = config["<step_name>__minimap2_reference_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__minimap2_reference_output_dir"]+ "/",
        pacbio_oxfordNanopore = config["<step_name>__minimap2_reference_pacbio_oxfordNanopore"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__minimap2_reference_output_dir"] + "/minimap2_reference_log.txt"
    threads: 
        config["<step_name>__minimap2_reference_threads"]
    shell: 
        "{params.command} map-{params.pacbio_oxfordNanopore} "
        "-t {threads} "
        "{input.fasta} "
        "{input.reads} "
        "| gzip -1 > {output.reads_mapping} "
        "|& tee {log}"
        
