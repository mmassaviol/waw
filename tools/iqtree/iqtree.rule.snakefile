rule <step_name>__iqtree:
    input:
        **<step_name>__iqtree_inputs(),
    output: 
        treefile = config["results_dir"] + "/" + config["<step_name>__iqtree_output_dir"] + "/out.treefile",
        report = config["results_dir"] + "/" + config["<step_name>__iqtree_output_dir"] + "/IQ-TREE_report_mqc.html"
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__iqtree_output_dir"]+ "/",
        command = config["<step_name>__iqtree_command"],
        seed = "-seed " + str(config["<step_name>__iqtree_seed"]) if config["<step_name>__iqtree_seed"] != -1 else "",
        model = config["<step_name>__iqtree_model"],
        bootstrap = config["<step_name>__iqtree_bootstrap"] if config["<step_name>__iqtree_bootstrap"] != "none" else "",
        bootstrap_val = config["<step_name>__iqtree_bootstrap_val"] if config["<step_name>__iqtree_bootstrap"] != "none" else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__iqtree_output_dir"] + "/iqtree_log.txt"
    threads: 
        config["<step_name>__iqtree_threads"]
    shell:
        "cd {params.output_dir} && "
        "ln -sf {input.sequence_file} seq.fa; "
        "{params.command} "
        "-s seq.fa "
        "-m {params.model} "
        "{params.bootstrap} {params.bootstrap_val} "
        "-nt {threads} "
        "-pre out "
        "-redo "
        "{params.seed} "
        "|& tee {log}; "
        "unlink seq.fa; "
        "sed '1 i\<pre>' out.iqtree > {output.report}"


