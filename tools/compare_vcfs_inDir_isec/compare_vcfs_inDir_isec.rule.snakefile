rule <step_name>__compare_vcfs_inDir_isec:
    input:
        **<step_name>__compare_vcfs_inDir_isec_inputs(),
    output:
        sites = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"]+"/sites.txt",
        common = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"]+"/common.txt",
        common_vcf = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"]+"/common_only.vcf.gz",
    params:
        command = config["<step_name>__compare_vcfs_inDir_isec_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_inDir_isec_output_dir"],
        nb_vcfs = len(<step_name>__compare_vcfs_inDir_isec_inputs()["vcfs"]),
        minQUAL = config["<step_name>__compare_vcfs_inDir_QUAL"],
        snponly = ' & TYPE="snp" ' if (config["<step_name>__compare_vcfs_inDir_snponly"]) else '',

    log:
        config["results_dir"]+"/logs/" + config["<step_name>__compare_vcfs_inDir_isec_output_dir"] + "/compare_vcfs_inDir_isec_log.txt"
    run:
        vcf_files = list()
        names = list()
        for vcf_file in input.vcfs:
            dir = os.path.basename(os.path.dirname(vcf_file))
            vcf_files.append(vcf_file)
            #check if tabix index is present
            if not os.path.isfile(vcf_file+".tbi"):
               command = "tabix -p vcf " + vcf_file
               os.system(command)
            #names.append(dir)
            names.append(os.path.basename(vcf_file))

        post_traitement = "awk '"
        post_traitement += "BEGIN {{print \"pos;" + ";".join(names) + "\"}}"
        post_traitement += "{{ n = split($5, t, \"\") ;"
        post_traitement += "x=$1\":\"$2;"
        post_traitement += "for (i = 1; i <= n; i++)  x=x\";\"t[i];"
        post_traitement += "print x"
        post_traitement += "}}' {output.sites}  > {params.output_dir}/snps.txt; "

        shell(
            
            "{params.command} " +
            "-n+1 " +
            " ".join(vcf_files) + " " +
            "-i 'QUAL>={params.minQUAL} {params.snponly}'  " +
            "-o {output.sites} " +
            "-c none " +
            "|& tee {log}; " +
            post_traitement +
            "grep " + len(vcf_files)*'1' + " {output.sites} > {output.common}; " +
            "vcftools --positions {output.common} " +
            "--gzvcf " + vcf_files[0] + " "
            "--recode --stdout | bgzip -c > {output.common_vcf}"
        )
