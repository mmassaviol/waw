<step_name>__vcf2gds_vcf_name = os.path.basename(<step_name>__vcf2gds_inputs()["vcfFile"]).split(".")[0]

rule <step_name>__vcf2gds:
    input:
        **<step_name>__vcf2gds_inputs(),
    output: 
        gds = config["results_dir"] + "/" + config["<step_name>__vcf2gds_output_dir"] + "/" + <step_name>__vcf2gds_vcf_name + ".gds",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__vcf2gds_output_dir"]+ "/",
        vcf2gds_ld_trim =  config["<step_name>__vcf2gds_ld_trim"] ,
        vcf2gds_min_maf =  config["<step_name>__vcf2gds_min_maf"] if (config["<step_name>__vcf2gds_ld_trim"] != "False") else "0",
        vcf2gds_ld_threshold =   config["<step_name>__vcf2gds_ld_threshold"] if (config["<step_name>__vcf2gds_ld_trim"] != "False") else "1",

        command = config["<step_name>__vcf2gds_command"],

    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__vcf2gds_output_dir"] + "/vcf2gds_log.txt"
    script:
        config["<step_name>__vcf2gds_script"]
       