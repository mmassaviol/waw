from Bio import SeqIO

records = SeqIO.parse(snakemake.input.fasta, "fasta")
count = SeqIO.write(records, snakemake.output.phylip, "phylip")