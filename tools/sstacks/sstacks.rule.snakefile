rule <step_name>__sstacks:
    input:
        **<step_name>__sstacks_inputs()
    output:
        matches = config["results_dir"]+"/"+config["<step_name>__sstacks_output_dir"]+"/{sample}.matches.tsv.gz"
    params:
        command = config["<step_name>__sstacks_command"],
        cstacks_dir = os.path.dirname(<step_name>__sstacks_inputs()["tags"]),
        output_dir = config["results_dir"]+"/"+config["<step_name>__sstacks_output_dir"],
        sample = os.path.dirname(<step_name>__sstacks_inputs()["ustacks_tags"])+"/{sample}"
    threads: 
        config["<step_name>__sstacks_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__sstacks_output_dir"] + "/{sample}_sstacks_log.txt"
    shell:
        "{params.command} "
        "-p {threads} "
        "-c {params.cstacks_dir} "
        "-s {params.sample} "
        "-o {params.output_dir} "
        "|& tee {log}"