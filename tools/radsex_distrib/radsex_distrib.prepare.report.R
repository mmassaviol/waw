library(sgtr)
library(yaml)
library(grid)
library(ggplot2)

args = commandArgs(trailingOnly=TRUE)
parameters = read_yaml(args[1])$params

chemin= paste0(parameters$results_dir,"/",parameters$<step_name>__radsex_distrib_output_dir)
fichiers = paste0(chemin, "/distribution.tsv")
output_file = paste(parameters$results_dir,parameters$<step_name>__radsex_distrib_output_dir,"Marker_depth_radsex_distrib_mqc.png",sep = "/")

ttt = read.table(fichiers, skip=1, header=T, nrows=1, check.names=F)
groups = colnames(ttt)[1:2]
tryCatch(
        expr = {
            # Default marker depths plot
            radsex_distrib(fichiers, groups = groups, output_file = output_file)
        },
        error = function(e){
            message('Caught an error!')
            p = ggplot() + ggtitle("Caught an error while calling radsex_distrib") + theme(panel.border = element_rect(colour = "black", fill=NA, size=2))
            png(output_file)
            print(p) 
            dev.off()
        },
        warning = function(w){
            message('Caught an error!')
            p = ggplot() + ggtitle("Caught an error while calling radsex_distrib") + theme(panel.border = element_rect(colour = "black", fill=NA, size=2))
            png(output_file)
            print(p) 
            dev.off()
        },
        finally = {
            message('marker depths plot done, quitting.')
        }
    )    
