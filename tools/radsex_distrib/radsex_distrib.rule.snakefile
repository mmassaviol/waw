rule <step_name>__radsex_distrib:
        input:
            **<step_name>__radsex_distrib_inputs()
        output:
            distribution = config["results_dir"] + "/" + config["<step_name>__radsex_distrib_output_dir"] + "/distribution.tsv"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__radsex_distrib_output_dir"] + '/radsex_log.txt'
        # threads:
        #     config["<step_name>__radsex_threads"]
        params:
            command = config["<step_name>__radsex_distrib_command"],
            min_depth = config["<step_name>__radsex_distrib_min_depth"],
            signif_threshold = config["<step_name>__radsex_distrib_signi_threshold"],

        shell:
            "{params.command} "+
            "--markers-table {input.markers_table} "+
            "--popmap {input.popmap_file} "+
            "-o {output} "+
            "-d {params.min_depth} "+
            "--signif-threshold {params.signif_threshold} "
            #"--groups F,M"
            