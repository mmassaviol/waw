rule <step_name>__gstacks:
    input:
        **<step_name>__gstacks_inputs(),
        popmap = config["<step_name>__gstacks_population_tsv"]
    output:
        catalog_calls = config["results_dir"]+"/"+config["<step_name>__gstacks_output_dir"]+"/catalog.calls",
        catalog_fa = config["results_dir"]+"/"+config["<step_name>__gstacks_output_dir"]+"/catalog.fa.gz",
        stats = config["results_dir"]+"/"+config["<step_name>__gstacks_output_dir"]+"/gstacks.log.distribs"
    params:
        command = config["<step_name>__gstacks_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__gstacks_output_dir"],
        bam_dir = lambda w, input: os.path.dirname(input.bams[0]),
        model = config["<step_name>__gstacks_model"],
        var_alpha = config["<step_name>__gstacks_var_alpha"],
        gt_alpha = config["<step_name>__gstacks_gt_alpha"],
        min_mapq = config["<step_name>__gstacks_min_mapq"],
        max_clipped = config["<step_name>__gstacks_max_clipped"]
    threads:
        config["<step_name>__gstacks_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__gstacks_output_dir"] + "/gstacks_log.txt"
    shell: """
        nonemptySamples='';
        emptycount=0
        for i in `ls {params.bam_dir}/*.bam`; do [ -s $i ] || emptycount=$((emptycount + 1)) ;  done;
        if [[ ${{emptycount}} -gt 0 ]];
        then 
                # here the bam file must include RG and SM
                for i in `ls {params.bam_dir}/*.bam`; 
                do 
                 if [[ -s $i ]]; 
                 then 
                  countRG=`samtools view -H $i | grep -c @RG` 
                  if [[ ${{countRG}} -eq 0 ]];
                  then
                    sample=`basename $i .bam`;
                    samtools addreplacerg -r ID:$sample -r SM:$sample -o tmp.bam $i
                    mv tmp.bam $i
                  fi
                  nonemptySamples=${{nonemptySamples}}" -B "$i ;  
                 fi 
                done;
                gstacks -t {threads} $nonemptySamples  --model {params.model}  \
                --var-alpha {params.var_alpha} --gt-alpha {params.gt_alpha} --min-mapq {params.min_mapq}  \
                --max-clipped {params.max_clipped} -O {params.output_dir}  |& tee {log}
        else
                gstacks -t {threads} -I {params.bam_dir} -M {input.popmap} --model {params.model} \
                --var-alpha {params.var_alpha} --gt-alpha {params.gt_alpha} --min-mapq {params.min_mapq} \
                --max-clipped {params.max_clipped} -O {params.output_dir} |& tee {log}
        fi
       """
       