rule <step_name>__miniasm:
    input:
        **<step_name>__miniasm_inputs(),
    output: 
        assembly_gfa = config["results_dir"] + "/" + config["<step_name>__miniasm_output_dir"] + "/reads.gfa",
        assembly_fasta = config["results_dir"] + "/" + config["<step_name>__miniasm_output_dir"] + "/reads.fasta",
    params:
        command = config["<step_name>__miniasm_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__miniasm_output_dir"]+ "/",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__miniasm_output_dir"] + "/miniasm_log.txt"
    shell: 
        "{params.command} "
        "-f {input.reads} "
        "{input.paf} "
        "> {output.assembly_gfa} "
        "|& tee {log} "
        "&& awk '$1 ~/S/ {{print \">\"$2\"\\n\"$3}}' {output.assembly_gfa} > {output.assembly_fasta} "
