rule <step_name>__bcftools_vcfFile_stats:
    input:
        **<step_name>__bcftools_vcfFile_stats_inputs(),
    output: 
        stats = config["results_dir"] + "/" + config["<step_name>__bcftools_vcfFile_stats_output_dir"] + "/vcf_stats.txt",
        stats_image = config["results_dir"] + "/" + config["<step_name>__bcftools_vcfFile_stats_output_dir"] + "/bcftools_stats_mqc.png",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__bcftools_vcfFile_stats_output_dir"],
        command = config["<step_name>__bcftools_vcfFile_stats_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__bcftools_vcfFile_stats_output_dir"] + "/bcftools_vcfFile_stats_log.txt"
    shell: 
        "{params.command} "
        "-s - "
        "{input.vcf} "
        "> {output.stats} "
        "2> {log}; "
        "plot-vcfstats "
        "-p {params.output_dir} "
        "-P " # no pdf
        "{output.stats}; "
        # Merge images
        "convert -size 1200x1000 \( {params.output_dir}/depth.0.png  {params.output_dir}/substitutions.0.png {params.output_dir}/hwe.0.png +smush +400 \) "
        "\( {params.output_dir}/indels_by_sample.0.png {params.output_dir}/snps_by_sample.0.png +smush +150 \) "
        "\( {params.output_dir}/dp_by_sample.0.png {params.output_dir}/singletons_by_sample.0.png +smush +150 \) "
        "\( {params.output_dir}/hets_by_sample.0.png {params.output_dir}/tstv_by_sample.0.png +smush +150 \) -append {output.stats_image}"