rule <step_name>__trinity:
    input:
        **<step_name>__trinity_inputs(),
    output:
        assembly = config["results_dir"] + "/" + config["<step_name>__trinity_output_dir"] + "/Trinity.fasta"
    params:
        output_dir = config["results_dir"]+"/"+config["<step_name>__trinity_output_dir"]
    threads:
        config["<step_name>__trinity_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__trinity_output_dir"] + '/trinity_log.txt'
    shell:
        "Trinity "
        "--seqType fq "
        "--left {input.read} "
        "--right {input.read2} "
        "--output {params.output_dir} "
        "--CPU {threads} "
        "|& tee > {log}"