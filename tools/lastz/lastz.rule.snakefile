rule <step_name>__lastz:
    input:
        **<step_name>__lastz_inputs(),
        reference_fasta = config["<step_name>__lastz_reference_fasta"],
    output: 
        align_on_ref = config["results_dir"] + "/" + config["<step_name>__lastz_output_dir"] + "/{sample}.vs_probes_default.sam",
        filtered = config["results_dir"] + "/" + config["<step_name>__lastz_output_dir"] + "/filtered/{sample}.vs_probes_default.filtered.sam",
        stats = config["results_dir"] + "/" + config["<step_name>__lastz_output_dir"] + "/stats/{sample}_lastz_sam_stats.tsv",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__lastz_output_dir"]+ "/",
        command = config["<step_name>__lastz_command"]
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__lastz_output_dir"] + "/{sample}_lastz_log.txt"
    shell: 
        "{params.command} "
        "{input.reference_fasta}[multiple,unmask] "
        "{input.contigs}[unmask] "
        "--gapped "
        "--format=softsam "
        "--ambiguous=iupac "
        "--output='{output.align_on_ref}' "
        "|& tee {log}; "

        # stats
        "NB_ENTRIES=$(samtools view {output.align_on_ref} | wc -l); "

        "if [ $NB_ENTRIES -gt 0 ]; then "

        "NB_LOCUS=$(samtools view {output.align_on_ref} | cut -f 3 | sort -u | wc -l); "
        "LIST_MULTI_LOCUS_MAPPED=$(samtools view {output.align_on_ref} | cut -f 1,3 | uniq | awk 'BEGIN {{ FS=OFS=SUBSEP=\"\\t\"; RS=\"\\n\"}}{{arr[$2]+=1 }}END {{for (i in arr) if (arr[i] > 1) print i}}' -); "
        "NB_MULTI_LOCUS_MAPPED=$(echo $LIST_MULTI_LOCUS_MAPPED | wc -w); "
        "LIST_MULTI_CLUSTER_MAPPED=$(samtools view {output.align_on_ref} | cut -f 1,3 | uniq | awk 'BEGIN {{ FS=OFS=SUBSEP=\"\\t\"; RS=\"\\n\"}}{{arr[$1]+=1 }}END {{for (i in arr) if (arr[i] > 1) print i}}' -); "
        "NB_MULTI_CLUSTER_MAPPED=$(echo $LIST_MULTI_CLUSTER_MAPPED | wc -w); "
        "printf '%s\\n' \"${{LIST_MULTI_CLUSTER_MAPPED[@]}}\" > /tmp/{wildcards.sample}_lastz.tmp; "
        "printf '%s\\n' \"${{LIST_MULTI_LOCUS_MAPPED[@]}}\" >> /tmp/{wildcards.sample}_lastz.tmp; "
        "sed -i '/^$/d' /tmp/{wildcards.sample}_lastz.tmp; "

        # filter locus and clusters multimapped
        "samtools view -h {output.align_on_ref} | grep -v -f /tmp/{wildcards.sample}_lastz.tmp - > {output.filtered}; "

        "else "
        "NB_LOCUS=0;"
        "NB_MULTI_LOCUS_MAPPED=0;"
        "NB_MULTI_CLUSTER_MAPPED=0;"
        "touch {output.filtered}; "
        "fi;\n"

        "NB_ENTRIES_AFTER_FILTER=$(samtools view {output.filtered} | wc -l); "
        
        "echo -e NB_ENTRIES'\\t'NB_LOCUS'\\t'NB_MULTI_LOCUS_MAPPED'\\t'NB_MULTI_CLUSTER_MAPPED'\\t'NB_ENTRIES_AFTER_FILTER'\\n'$NB_ENTRIES'\\t'$NB_LOCUS'\\t'$NB_MULTI_LOCUS_MAPPED'\\t'$NB_MULTI_CLUSTER_MAPPED'\\t'$NB_ENTRIES_AFTER_FILTER > {output.stats}; "