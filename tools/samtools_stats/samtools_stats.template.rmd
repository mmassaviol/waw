```{r echo=FALSE, message=FALSE, warning=FALSE, fig.width=10, eval=samtools_stats}
library(DT)
library(ggplot2)
library(reshape2)
dirs = list.dirs(path = paste(parameters$results_dir,parameters$samtools_stats_output_dir,sep="/"))[-1]

# Summary Numbers
SN = data.frame(read.delim(paste0(dirs[1],"/SN.txt"),sep=":",header = FALSE,comment.char = "#",row.names = 1,col.names = c("infos",tail(strsplit(dirs[1],"/")[[1]],n=1))))
for (i in seq(2,length(dirs))){
  SN[tail(strsplit(dirs[i],"/")[[1]],n=1)] = read.delim(paste0(dirs[i],"/SN.txt"),sep=":",header = FALSE,comment.char = "#")[2]
}
SN = data.frame(t(SN))
SN["Samples"] = rownames(SN)
#datatable(SN, options = list(scrollX = '300px'), caption = "General Bam stats",colnames = c("Samples",colnames(SN)))

to_plot = melt(SN,measure.vars = c("reads.mapped","reads.unmapped"),id.vars = "Samples")

ggplot(to_plot,aes(x=Samples,y=value, fill=variable)) + geom_col() + coord_flip() + ylab("Reads") + ggtitle("Proportion of mapped and unmapped reads per sample")


# # Coverage
# COV = data.frame(read.delim(paste0(dirs[1],"/COV.txt"),sep="\t",header = FALSE))
# 
# plot = ggplot() + scale_x_log10()
# 
# 
# binnedSamples <- cut( COV$V2, breaks = c(0, 2, 10, 20, 30, 40, 50, 60, 80, 150 ,300 ,1000) )
# COV = data.frame(tapply( COV$V3, binnedSamples, sum ))
# names(COV) = tail(strsplit(dirs[1],"/")[[1]],n=1)
# #plot = plot + geom_line(aes(x=COV$V2,y=COV$V3))
# 
# for (i in seq(2,length(dirs))){
#   #name = paste0("tmp.",i)
#   tmp = data.frame(read.delim(paste0(dirs[i],"/COV.txt"),sep="\t",header = FALSE))
#   #plot = plot + geom_line(aes(x=get(eval(parse(text="name")))$V2,y=get(eval(parse(text="name")))$V3))
#   binnedSamples <- cut( tmp$V2, breaks = c(0, 2, 10, 20, 30, 40, 50, 60, 80, 150 ,300 ,1000) )
#   COV[tail(strsplit(dirs[i],"/")[[1]],n=1)] = tapply( tmp$V3, binnedSamples, sum )
# }
# 
# COV=data.frame(t(COV))
# COV["Samples"] = rownames(COV)
# 
# a = melt(COV,measure.vars = colnames(COV)[1:length(colnames(COV))-1] ,id.vars="Samples")
# 
# ggplot(a,aes(x=variable,y=value)) + geom_boxplot()

```