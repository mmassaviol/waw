rule <step_name>__flye:
    input:
        **<step_name>__flye_inputs(),
    output: 
        assembly_fasta = config["results_dir"] + "/" + config["<step_name>__flye_output_dir"] + "/assembly.fasta",
        assembly_info = config["results_dir"] + "/" + config["<step_name>__flye_output_dir"] + "/assembly_info.txt",
    params:
        command = config["<step_name>__flye_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__flye_output_dir"]+ "/",
        pacbio_oxfordNanopore = config["<step_name>__flye_pacbio_oxfordNanopore"],
        scaffold = "--scaffold" if config["<step_name>__flye_scaffold"] else "", 
        keep_haplotypes = "--keep-haplotypes" if config["<step_name>__flye_keep_haplotypes"] else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__flye_output_dir"] + "/flye_log.txt"
    threads: 
        config["<step_name>__flye_threads"]
    shell: 
        "{params.command} {params.pacbio_oxfordNanopore} "
        "{input.read} "
        "--out-dir {params.output_dir} "
        "-t {threads} "
        "{params.scaffold} {params.keep_haplotypes} |& tee {log} ;"
        "awk '/^>/ {{printf(\"\\n%s\\n\",$0);next; }} {{ printf(\"%s\",$0);}}  END {{printf(\"\\n\");}}' < {params.output_dir}/assembly.fasta > {params.output_dir}/assembly.fa; "
        "tail -n +2 {params.output_dir}/assembly.fa > {params.output_dir}/assembly.fasta; "
        "rm -f {params.output_dir}/assembly.fa"