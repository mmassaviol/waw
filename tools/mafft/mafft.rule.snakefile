rule <step_name>__mafft:
    input:
        **<step_name>__mafft_inputs(),
    output: 
        aligned_fasta = config["results_dir"] + "/" + config["<step_name>__mafft_output_dir"] + "/output.afa",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__mafft_output_dir"]+ "/",
        command = config["<step_name>__mafft_command"],
        matrix = config["<step_name>__mafft_matrix"] if config["<step_name>__mafft_matrix"] != "none" else "" ,
        gap_open = config["<step_name>__mafft_gap_open"],
        gap_extension = config["<step_name>__mafft_gap_extension"],
        order = config["<step_name>__mafft_order"],
        retree = config["<step_name>__mafft_retree"],
        treeout = "--treeout" if config["<step_name>__mafft_treeout"] else "",
        maxiterate = config["<step_name>__mafft_maxiterate"],
        ffts = config["<step_name>__mafft_ffts"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__mafft_output_dir"] + "/mafft_log.txt"
    threads: 
        config["<step_name>__mafft_threads"]
    shell: 
        "{params.command} "
        "{params.matrix} "
        "--op {params.gap_open} "
        "--ep {params.gap_extension} "
        "{params.order} "
        "--retree {params.retree} "
        "{params.treeout} "
        "--maxiterate {params.maxiterate} "
        "{params.ffts} "
        "--thread {threads} "
        "{input.fasta} "
        "> {output.aligned_fasta} "
        "2> {log}"
