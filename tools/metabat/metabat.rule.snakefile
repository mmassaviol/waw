rule <step_name>__metabat:
    input:
        **<step_name>__metabat_inputs(),
    output: 
        fasta_bins = directory(config["results_dir"] + "/" + config["<step_name>__metabat_output_dir"] + "/"),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__metabat_output_dir"]+ "/",
        command = config["<step_name>__metabat_command"],
        mincontig = config["<step_name>__metabat_mincontig"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__metabat_output_dir"] + "/metabat_log.txt"
    threads: 
        config["<step_name>__metabat_threads"]
    shell: 
        "{params.command} "
        "-i {input.contigs} "
        "--outFile {params.output_dir}/out "
        "--minContig {params.mincontig} "
        "-t {threads} "
        "|& tee {log}"
