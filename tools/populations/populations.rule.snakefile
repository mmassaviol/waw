rule <step_name>__populations:
    input:
        **<step_name>__populations_inputs(),
    output:
        log = config["results_dir"]+"/"+config["<step_name>__populations_output_dir"]+"/populations.log"
    params:
        command = config["<step_name>__populations_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__populations_output_dir"],
        gstacks_dir = os.path.dirname(<step_name>__populations_inputs()["catalog_calls"]),
        r = config["<step_name>__populations_r"],
        max_obs_het = config["<step_name>__populations_max_obs_het"],
        min_maf = config["<step_name>__populations_min_maf"],
        p = config["<step_name>__populations_p"]
    threads:
        config["<step_name>__populations_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__populations_output_dir"] + "/populations_log.txt"
    shell:
        "{params.command} "
        "-t {threads} "
        "-P {params.gstacks_dir} "
        "-M {input.popmap} "
        "-O {params.output_dir} "
        "-r {params.r} "
        "--max-obs-het {params.max_obs_het} "
        "--min-maf {params.min_maf} "
        "-p {params.p} "
        "--vcf "
        "--fstats "
        "--genepop "
        "--structure "
        "--radpainter "
        "--plink "
        "--phylip "
        "|& tee {log} "
        "&& gzip -f {params.output_dir}/populations.snps.vcf"
