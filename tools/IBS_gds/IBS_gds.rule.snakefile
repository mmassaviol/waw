<step_name>__IBS_gds_gds_name = os.path.basename(<step_name>__IBS_gds_inputs()["gds"]).split(".")[0]

rule <step_name>__IBS_gds:
    input:
        **<step_name>__IBS_gds_inputs(),
    output: 
        ibs = config["results_dir"] + "/" + config["<step_name>__IBS_gds_output_dir"] + "/" + <step_name>__IBS_gds_gds_name + "_ibs.txt",
        ibs_dendro_png = config["results_dir"] + "/" + config["<step_name>__IBS_gds_output_dir"] + "/ibs_dendro_plot_mqc.png",
        ibs_heatmap_png = config["results_dir"] + "/" + config["<step_name>__IBS_gds_output_dir"] + "/ibs_heatmap_mqc.png",
        ibs_mdscale_png = config["results_dir"] + "/" + config["<step_name>__IBS_gds_output_dir"] + "/ibs_mdscale_mqc.png",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__IBS_gds_output_dir"]+ "/",
        command = config["<step_name>__IBS_gds_command"],
    threads:
            config["<step_name>__IBS_gds_threads"]           
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__IBS_gds_output_dir"] + "/IBS_gds_log.txt"
    script:
        config["<step_name>__IBS_gds_script"]    
    
