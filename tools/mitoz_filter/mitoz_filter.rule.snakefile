if config["SeOrPe"] == "SE":

    rule <step_name>__mitoz_filter_SE:
        input:
            **<step_name>__mitoz_filter_SE_inputs()
        output:
            reads_filtered = config["results_dir"]+ "/" + config["<step_name>__mitoz_filter_SE_output_dir"] + '/{sample}.fq.gz',
        log: config["results_dir"]+'/logs/' + config["<step_name>__mitoz_filter_SE_output_dir"] + '/{sample}_mitoz_filter_log.txt'
        threads: config["<step_name>__mitoz_filter_threads"]
        params:
            command = config["<step_name>__mitoz_filter_SE_command"],
            output_dir = config["<step_name>__mitoz_filter_SE_output_dir"],
            prefix = "mitoz_filter_SE",
            output_file = "{sample}.fq.gz",
            adapter_list1 = "--adapter1 "+config["<step_name>__mitoz_adapter_list1"] if config["<step_name>__mitoz_adapter_list1"] != "" else "",
            Ns_number = config["<step_name>__mitoz_Ns_number_SE"],
            clade = config["<step_name>__mitoz_filter_clade"],
            reads_filtered = config["results_dir"]+ "/" +config["filter__mitoz_filter_SE_output_dir"] + "/mitoz_filter_SE.tmp/mitoz_filter_SE.cleandata/{sample}.fq.gz",
        shell:
            "mkdir -p {params.output_dir}/mitoz_filter_SE.result && "
            "cd {params.output_dir}; "
            "{params.command} "
            "--fastq1 {input.read} "
            "--fastq3 {params.output_file} "
            "--fq_size 0 "
            "--thread_number {threads} "
            "--outprefix {params.prefix} "
            "--Ns {params.Ns_number} "
            "{params.adapter_list1} "
            #"--clade {params.clade} "
            "|& tee {log}; "
            "mv {params.reads_filtered} {output.reads_filtered}"


elif config["SeOrPe"] == "PE":

    rule <step_name>__mitoz_filter_PE:
        input:
            **<step_name>__mitoz_filter_PE_inputs()
        output:
            read_filtered = config["results_dir"]+ "/" +config["<step_name>__mitoz_filter_PE_output_dir"] + '/{sample}_R1.fq.gz',
            read2_filtered = config["results_dir"]+ "/" + config["<step_name>__mitoz_filter_PE_output_dir"] +'/{sample}_R2.fq.gz',
        log: config["results_dir"]+'/logs/' + config["<step_name>__mitoz_filter_PE_output_dir"] + '/{sample}_mitoz_filter_log.txt'
        threads: config["<step_name>__mitoz_filter_threads"]
        params:
            command = config["<step_name>__mitoz_filter_PE_command"],
            output_dir = config["<step_name>__mitoz_filter_PE_output_dir"],
            prefix = "{sample}",
            output_file_R1 = "{sample}_R1.fq.gz",
            output_file_R2 = "{sample}_R2.fq.gz",
            adapter_list1 = "--adapter1 "+config["<step_name>__mitoz_adapter_list1"] if config["<step_name>__mitoz_adapter_list1"] != "" else "",
            adapter_list2 = "--adapter2 "+config["<step_name>__mitoz_adapter_list2_PE"] if config["<step_name>__mitoz_adapter_list2_PE"] != "" else "",
            remove_duplicates = "--duplication" if config["<step_name>__mitoz_remove_duplicates_PE"] else "",
            clade = config["<step_name>__mitoz_filter_clade"],
            # results path (to move from)
            tmpdir = config["results_dir"]+ "/" +config["filter__mitoz_filter_PE_output_dir"] + "/{sample}.tmp",
            resdir = config["results_dir"]+ "/" +config["filter__mitoz_filter_PE_output_dir"] + "/{sample}.result",
            read_filtered = config["results_dir"]+ "/" +config["filter__mitoz_filter_PE_output_dir"] + "/{sample}.tmp/{sample}.cleandata/{sample}_R1.fq.gz",
            read2_filtered = config["results_dir"]+ "/" +config["filter__mitoz_filter_PE_output_dir"] + "/{sample}.tmp/{sample}.cleandata/{sample}_R2.fq.gz",
        shell:
            "mkdir -p {params.output_dir}/{wildcards.sample}.result && "
            "cd {params.output_dir}; "
            "{params.command} "
            "--fastq1 {input.read} "
            "--fastq2 {input.read2} "
            "--fastq3 {params.output_file_R1} "
            "--fastq4 {params.output_file_R2} "
            "--fq_size 0 "
            "--thread_number {threads} "
            "--outprefix {params.prefix} "
            "{params.adapter_list1} "
            "{params.adapter_list2} "
            "{params.remove_duplicates} "
            #"--clade {params.clade} "
            "|& tee {log}; "
            "mv {params.read_filtered} {output.read_filtered}; "
            "mv {params.read2_filtered} {output.read2_filtered}; "
            # remove .tmp and .result dir to allow rerun
            "rm -r {params.tmpdir} {params.resdir}"
