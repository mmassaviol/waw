{
  id: mitoz_filter,
  name: MitoZ filter,
  article: 10.1093/nar/gkz173,
  website: "https://github.com/linzhi2013/MitoZ",
  git: "https://github.com/linzhi2013/MitoZ",
  description: "A toolkit for assembly, annotation, and visualization of animal mitochondrial genomes",
  version: "2.4-alpha",
  documentation: "https://github.com/linzhi2013/MitoZ",
  multiqc: "custom",
  commands:
    [
      {
        name: mitoz_filter_SE,
        cname: "Mitoz filter SE",
        command: /opt/biotools/release_MitoZ_v2.4-alpha/MitoZ.py filter,
        category: "quality",
        output_dir: mitoz_filter_SE,
        inputs: [{ name: read, type: "reads" }],
        outputs:
          [
            { name: read_filtered, type: "fq.gz", file: "{sample}.fq.gz", description: "Reads filtered" },
          ],
        options:
          [
            {
              name: mitoz_filter_threads,
              prefix: --thread_number,
              type: numeric,
              value: 4,
              min: 1,
              max: NA,
              step: 1,
              label: "Number of threads to use",
            },
            {
              name: "mitoz_filter_clade",
              type: "radio",
              value: "Arthropoda",
              choices: [
                Arthropoda: Arthropoda,
                Chordata: Chordata, 
                Echinodermata: Echinodermata,
                Annelida-segmented-worms: Annelida-segmented-worms,
                Bryozoa: Bryozoa,
                Mollusca: Mollusca,
                Nematoda: Nematoda,
                Nemertea-ribbon-worms: Nemertea-ribbon-worms,
                Porifera-sponges: Porifera-sponges
              ],
              label: "Taxa group for MitoZ : ",
            },
            {
              name: mitoz_adapter_list1,
              type: input_file,
              prefix: --adapter1,
              value: "",
              label: "File with adapter list (optional)",
            },
            {
              name: mitoz_Ns_number_SE,
              prefix: --Ns,
              type: numeric,
              value: 10,
              min: 0,
              max: NA,
              step: 1,
              label: "Number of threads to use",
            },
          ],
      },
      {
        name: mitoz_filter_PE,
        cname: "Mitoz filter PE",
        command: /opt/biotools/release_MitoZ_v2.4-alpha/MitoZ.py filter,
        category: "quality",
        output_dir: mitoz_filter_PE,
        inputs: [{ name: read, type: "reads" }, { name: read2, type: "reads" }],
        outputs:
          [
            { name: read_filtered, type: "fq.gz", file: "{sample}_R1.fq.gz", description: "Reads forward filtered" },
            { name: read2_filtered, type: "fq.gz", file: "{sample}_R2.fq.gz", description: "Reads reverse filtered" },
          ],
        options:
          [
            {
              name: mitoz_filter_threads,
              prefix: --thread_number,
              type: numeric,
              value: 4,
              min: 1,
              max: NA,
              step: 1,
              label: "Number of threads to use",
            },
            {
              name: "mitoz_filter_clade",
              type: "radio",
              value: "Arthropoda",
              choices: [
                Arthropoda: Arthropoda,
                Chordata: Chordata, 
                Echinodermata: Echinodermata,
                Annelida-segmented-worms: Annelida-segmented-worms,
                Bryozoa: Bryozoa,
                Mollusca: Mollusca,
                Nematoda: Nematoda,
                Nemertea-ribbon-worms: Nemertea-ribbon-worms,
                Porifera-sponges: Porifera-sponges
              ],
              label: "Taxa group for MitoZ : ",
            },
            {
              name: mitoz_adapter_list1,
              type: input_file,
              prefix: --adapter1,
              value: "",
              label: "File with adapter list (optional)",
            },
            {
              name: mitoz_adapter_list2_PE,
              type: input_file,
              prefix: --adapter2,
              value: "",
              label: "File 2 with adapter list (optional)",
            },
            {
              name: mitoz_remove_duplicates_PE,
              prefix: --duplication,
              type: checkbox,
              value: TRUE,
              label: "filter duplications (caused by PCRs After adapter ligation, the directions of read1 & read2 of duplications should be completely identical)",
            },
          ],
      },
    ],
  install: {
    biopython: [
      "pip3 install biopython==1.69"
    ],
    ete: [
      "pip3 install ete3==3.1.2"
    ],
    scipy: [
      "pip3 install scipy"
    ],
    bioperl: [
      "apt-get install -y bioperl"
    ],
    circos: [
      "apt-get install -y circos",
      "cd /opt/biotools && wget http://circos.ca/distribution/circos-current.tgz",
      "tar -xzf circos-current.tgz",
      "rm -rf circos-current.tgz",
      "mv circos* circos_current",
      "ENV PATH /opt/biotools/circos_current/bin:$PATH",
      "sed -i 's/max_points_per_track.*/max_points_per_track = 40000/' /opt/biotools/circos_current/etc/housekeeping.conf"
    ],
    bwa: [
      "cd /opt/biotools",
      "wget https://github.com/lh3/bwa/releases/download/v0.7.17/bwa-0.7.17.tar.bz2",
      "tar -xvjf bwa-0.7.17.tar.bz2",
      "cd bwa-0.7.17",
      "make -j 10",
      "mv bwa ../bin/",
      "cd ..",
      "rm -r bwa-0.7.17 bwa-0.7.17.tar.bz2"
    ],
    samtools: [
      "cd /opt/biotools",
      "wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2",
      "tar -xvjf samtools-1.9.tar.bz2",
      "cd samtools-1.9",
      "./configure && make",
      "cd ..",
      "mv samtools-1.9/samtools bin/samtools",
      "rm -r samtools-1.9 samtools-1.9.tar.bz2"
    ],
    mitoz: [
      "cd /opt/biotools",
      "wget -c https://github.com/linzhi2013/MitoZ/raw/master/version_2.4-alpha/release_MitoZ_v2.4-alpha.tar.bz2",
      "tar -jxvf release_MitoZ_v2.4-alpha.tar.bz2",
      "rm release_MitoZ_v2.4-alpha.tar.bz2",
      "sed -i 's/choices=\\[\"Chordata\", \"Arthropoda\"\\]/choices=\\[\"Chordata\", \"Arthropoda\", \"Echinodermata\", \"Annelida-segmented-worms\", \"Bryozoa\", \"Mollusca\", \"Nematoda\", \"Nemertea-ribbon-worms\", \"Porifera-sponges\"\\]/' /opt/biotools/release_MitoZ_v2.4-alpha/MitoZ.py",
      "ENV PATH /opt/biotools/release_MitoZ_v2.4-alpha:$PATH",
      "python3 -c \"from ete3 import NCBITaxa;ncbi = NCBITaxa();ncbi.update_taxonomy_database()\"",
      "sed -i 's/^        yield rec/        try:\n            yield rec/' /opt/biotools/release_MitoZ_v2.4-alpha/bin/annotate/gene_feature.py ",
      "sed -i 's/^        raise StopIteration/        except:\n            raise StopIteration/' /opt/biotools/release_MitoZ_v2.4-alpha/bin/annotate/gene_feature.py ",
      "# see https://docs.python.org/3/whatsnew/3.7.html#changes-in-python-behavior",
      "sed 's/#return re.sub(pos_old, pos_new, str(pos))/return pos_new/' /opt/biotools/release_MitoZ_v2.4-alpha/bin/common/genbank_gene_stat_v2.py"

    ]
  },
  citations:  {
    mitoz: [
      "Guanliang Meng, Yiyuan Li, Chentao Yang, Shanlin Liu, MitoZ: a toolkit for animal mitochondrial genome assembly, annotation and visualization, Nucleic Acids Research, , gkz173, https://doi.org/10.1093/nar/gkz173"
    ],
    circos: [
      "Krzywinski, M., Schein, J., Birol, I., Connors, J., Gascoyne, R., Horsman, D., Jones, S. J., & Marra, M. A. (2009). Circos: An information aesthetic for comparative genomics. Genome Research, 19(9), 1639‑1645. https://doi.org/10.1101/gr.092759.109"
    ],
    bwa: [
      "Heng Li, Richard Durbin, Fast and accurate short read alignment with Burrows-Wheeler transform, Bioinformatics, Volume 25, Issue 14, 15 July 2009, Pages 1754-1760, https://doi.org/10.1093/bioinformatics/btp324"
    ],
    samtools: [
      "Heng Li, Bob Handsaker, Alec Wysoker, Tim Fennell, Jue Ruan, Nils Homer, Gabor Marth, Goncalo Abecasis, Richard Durbin, 1000 Genome Project Data Processing Subgroup, The Sequence Alignment/Map format and SAMtools, Bioinformatics, Volume 25, Issue 16, 15 August 2009, Pages 2078-2079, https://doi.org/10.1093/bioinformatics/btp352"
    ]
  }
}