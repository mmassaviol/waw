rule <step_name>__compare_vcfs:
    input:
        **<step_name>__compare_vcfs_inputs(),
        truth_vcf_file = config["<step_name>__compare_vcfs_truth_file"] if config["<step_name>__compare_vcfs_truth"] else ""
    output:
        stats = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"]+"/{sample}+"/stats.txt",
        bench_to_truth = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"]+"/{sample}+"/VCF_compare_to_truth_mqc.tsv" if config["<step_name>__compare_vcfs_truth"] else ""
    params:
        command = config["<step_name>__compare_vcfs_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__compare_vcfs_output_dir"]+"/{sample}",
        nb_vcfs = nb_vcfs = len(<step_name>__compare_vcfs_inputs()["vcfsinDir"]),
        truth_vcf = config["<step_name>__compare_vcfs_truth"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__compare_vcfs_output_dir"]+"/{sample} + "/compare_vcfs_log.txt"
    run:
        cp = ""
        tmp_tocompare = list()
        for vcf_file in input.vcfs:
            dir = os.path.basename(os.path.dirname(vcf_file))
            if os.path.splitext(vcf_file)[1] == ".gz":
                suffix = ".vcf.gz"
            else:
                suffix = ".vcf"
            shutil.copyfile(vcf_file,"/tmp/" + dir + suffix)
            tmp_tocompare.append("/tmp/" + dir + suffix)

        to_png = ""
        if params.nb_vcfs < 4:
            to_png += "pdftoppm -png venn" +str(params.nb_vcfs)+ ".positions.pdf > Venn_positions_mqc.png; "
            to_png += "pdftoppm -png venn" +str(params.nb_vcfs)+ ".snps.pdf > Venn_snps_mqc.png; "
        else:
            to_png += "pdftoppm -png venn" +str(params.nb_vcfs)+ ".pdf > Venn_mqc.png; "

        to_truth = ""
        stats_truth = dict()
        if params.truth_vcf:
            for vcf in tmp_tocompare:
                a = vcf.split('.')[0].split('/')
                dir = a[len(a)-1]
                to_truth += "mkdir -p {params.output_dir}/" + dir + " && cd $_ &&"
                to_truth += "vcftoolz compare --truth " + input.truth_vcf_file + " " + vcf + " > {params.output_dir}/" + dir + "/stats.txt; "
                stats_truth[dir] = "{params.output_dir}/" + dir + "/stats.txt"
        
        summary_bench_truth = "echo \"# id: compare_to_truth\" > {output.bench_to_truth}; "
        summary_bench_truth += "echo \"# section_name: 'Compare VCFs to truth'\" >> {output.bench_to_truth}; "
        #summary_bench_truth += "echo \"# description: ''\" > {output.bench_to_truth}; "
        summary_bench_truth += "echo \"# format: 'tsv'\" >> {output.bench_to_truth}; "
        summary_bench_truth += "echo \"# plot_type: 'table'\" >> {output.bench_to_truth}; "

        summary_bench_truth += "echo -n 'Variant caller\tPrecision\tRecall\tF1 score\tTrue positive calls\tFalse negative calls\tFalse positive calls\n' >> {output.bench_to_truth}; "
        for variant_caller in stats_truth.keys():
            summary_bench_truth += "echo -n '" + variant_caller + "\t' >> {output.bench_to_truth}; "
            summary_bench_truth += "grep 'Precision' " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> {output.bench_to_truth}; "
            summary_bench_truth += "grep 'Recall' " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> {output.bench_to_truth}; "
            summary_bench_truth += "grep 'F1 score' " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> {output.bench_to_truth}; "
            summary_bench_truth += "grep 'True positive calls' " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> {output.bench_to_truth}; "
            summary_bench_truth += "grep 'False negative calls' -m 1 " + stats_truth[variant_caller] + " | awk '{{printf $1\"\t\"}}' >> {output.bench_to_truth}; "
            summary_bench_truth += "grep 'False positive calls' " + stats_truth[variant_caller] + " | awk '{{print $1}}' >> {output.bench_to_truth}; "

        shell(
            "cd {params.output_dir}; "
            "{params.command} " +
            " ".join(tmp_tocompare) +
            "> {output.stats} " +
            "2> >(tee {log} >&2); " +
            to_png +
            to_truth +
            summary_bench_truth
        )
