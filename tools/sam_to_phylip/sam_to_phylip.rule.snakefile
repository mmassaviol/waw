rule <step_name>__sam_to_phylip:
    input:
        **<step_name>__sam_to_phylip_inputs(),
    output: 
        phylips = directory(config["results_dir"] + "/" + config["<step_name>__sam_to_phylip_output_dir"] + "/"),
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__sam_to_phylip_output_dir"]+ "/",
        input_dir = lambda w, input: os.path.dirname(input.sams[0])
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__sam_to_phylip_output_dir"] + "/sam_to_phylip_log.txt"
    threads: 1
    shell: 
        "for file in $(ls {params.input_dir}/*.sam);\n"
        "do\n"
        "    NB_ENTRIES=$(samtools view $file | wc -l);\n"
            "    if [ $NB_ENTRIES -gt 0 ]; then\n"
        "        sample=$(basename $file | cut -f 1 -d '.');\n"
                # Create sam header for each locus if not done yet (needed for samtools fasta)
        "        grep \"^@SQ\" $file | awk '{{split($2,a,\":\"); if(system(\"test -f {params.output_dir}/\"a[2]\".sam\")!=0) {{print $0 > \"{params.output_dir}/\"a[2]\".sam\"}} close(\"{params.output_dir}/\"a[2])\".sam\"}}';\n"
                # Group entries by locus
        "        samtools view $file | awk -v sample=\"$sample\" 'BEGIN {{OFS=\"\t\"}} {{$1=sample; print $0 >> \"{params.output_dir}/\"$3\".sam\"}} close(\"{params.output_dir}/\"$3\".sam\")';\n"
        "    fi;\n"
        "done;\n"

        "echo 'Group by reference done';\n"

        # sam to fasta
        "for file in $(ls {params.output_dir}/*.sam);\n"
        "do\n"
        "    NB_ENTRIES=$(samtools view $file | wc -l);\n"
            "    if [ $NB_ENTRIES -gt 0 ]; then\n"
        "        outname=$(echo $file | cut -f 1 -d '.');\n"
        "        samtools fasta $file > ${{outname}}.fasta;\n"
        "    fi;\n"
        "done;"

        "echo 'Turn sam into fasta done';\n"

        # fasta to phylip
        "for file in $(ls {params.output_dir}/*.fasta);\n"
        "do\n"
        "    outname=$(echo $file | cut -f 1 -d '.');\n"
        "    seqret -sequence $file -osf phylip3 -outseq ${{outname}}.phylip;\n"
        "done;"

        "echo 'Turn fasta into phylip done';\n"
