def ustacks_rename_outputs():
    command = ""
    if (config["SeOrPe"]=="PE"):
        prefix_in = config["results_dir"]+"/"+config["<step_name>__ustacks_output_dir"]+"/"
        prefix_out = config["results_dir"]+"/"+config["<step_name>__ustacks_output_dir"]+"/"
        command += ' && mv '+prefix_in+'{sample}_R1.tags.tsv.gz '+prefix_out+'{sample}.tags.tsv.gz'
        command += ' && mv '+prefix_in+'{sample}_R1.snps.tsv.gz '+prefix_out+'{sample}.snps.tsv.gz'
        command += ' && mv '+prefix_in+'{sample}_R1.alleles.tsv.gz '+prefix_out+'{sample}.alleles.tsv.gz'
    return command

rule <step_name>__ustacks:
    input:
        **<step_name>__ustacks_inputs(),
    output:
        tags = config["results_dir"]+"/"+config["<step_name>__ustacks_output_dir"]+"/{sample}.tags.tsv.gz",
        snps = config["results_dir"]+"/"+config["<step_name>__ustacks_output_dir"]+"/{sample}.snps.tsv.gz",
        alleles = config["results_dir"]+"/"+config["<step_name>__ustacks_output_dir"]+"/{sample}.alleles.tsv.gz"
    params:
        command = config["<step_name>__ustacks_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__ustacks_output_dir"],
        m = config["<step_name>__ustacks_m"],
        M = config["<step_name>__ustacks_M"],
        N = config["<step_name>__ustacks_N"],
        i = lambda w: individus.index(w.sample),
        move = ustacks_rename_outputs(),
    threads:
        config["<step_name>__ustacks_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__ustacks_output_dir"] + "/{sample}_ustacks_log.txt"
    shell:
        "{params.command} "
        "-f {input.read} "
        "-p {threads} "
        "-i {params.i} "
        "-m {params.m} "
        "-M {params.M} "
        "-N {params.N} "
        "-o {params.output_dir} "
        "|& tee {log}"
        "{params.move}"
