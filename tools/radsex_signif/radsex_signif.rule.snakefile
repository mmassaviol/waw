rule <step_name>__radsex_signif:
        input:
            **<step_name>__radsex_signif_inputs()
        output:
            significant_markers = config["results_dir"] + "/" + config["<step_name>__radsex_signif_output_dir"] + "/significant_markers_mqc.tsv",
            significant_sequences = config["results_dir"] + "/" + config["<step_name>__radsex_signif_output_dir"] + "/significant_markers_sequences.fa",
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__radsex_signif_output_dir"] + '/radsex_log.txt'
        # threads:
        #     config["<step_name>__radsex_threads"]
        params:
            command = config["<step_name>__radsex_signif_command"],
            min_depth = config["<step_name>__radsex_signif_min_depth"],
            signif_threshold = config["<step_name>__radsex_signif_threshold"],
        shell:
            "{params.command} "+
            "--markers-table {input.markers_table} "+
            "--popmap {input.popmap_file} "+
            "-o {output.significant_markers} "+
            "-d {params.min_depth} " +
            "--signif-threshold {params.signif_threshold}; " +
            "awk -F\"\\t\" '{{ if (NR > 2) print \">\"$1\"\\n\"$2 ;}}' {output.significant_markers} > {output.significant_sequences};" +
            "awk -F\"\\t\" '{{for(i=1; i<= NF;i++) if (i != 2) printf(\"%s\\t\", $i)  ; print $2 ;}}' {output.significant_markers} > {output.significant_markers}.tmp ;" +
            "mv {output.significant_markers}.tmp {output.significant_markers}; "
