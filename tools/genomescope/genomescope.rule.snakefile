rule <step_name>__genomescope:
    input:
        **<step_name>__genomescope_inputs()
    output:
        GenomeScope_Profile = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"] + "/GenomeScope_Profile_mqc.png",
        GenomeScope_Profile_log_scale = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"] + "/GenomeScope_Profile_log_scale_mqc.png",
        Summary = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"] + "/GenomeScope_Summary_mqc.csv"
    params:
        command = config["<step_name>__genomescope_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__genomescope_output_dir"],
        kmer_len = config["<step_name>__genomescope_kmer_len"],
        reads_len = config["<step_name>__genomescope_reads_len"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__genomescope_output_dir"] + '/genomescope_log.txt'
    shell:
        "{params.command} "
        "{input.kmer_histo} "
        "{params.kmer_len} "
        "{params.reads_len} "
        "{params.output_dir} "
        "|& tee {log};"
        # prepare mqc custom content
        "mv {params.output_dir}/plot.png {params.output_dir}/GenomeScope_Profile_mqc.png && "
        "mv {params.output_dir}/plot.log.png {params.output_dir}/GenomeScope_Profile_log_scale_mqc.png && "
        "tail -n +4 {params.output_dir}/summary.txt | sed 's/ \{{2,\}}/\t/g' | sed 's/\t$//g' > {params.output_dir}/GenomeScope_Summary_mqc.csv"


