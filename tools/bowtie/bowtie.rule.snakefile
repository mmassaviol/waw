if config["SeOrPe"] == "PE":

    rule <step_name>__bowtie_PE:
        input:
            **<step_name>__bowtie_PE_inputs()
        output:
             bam = config["results_dir"]+"/"+config["<step_name>__bowtie_PE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__bowtie_PE_output_dir"] + "/{sample}_bowtie_log.txt"
        threads:
            config["<step_name>__bowtie_threads"]
        params:
            command = config["<step_name>__bowtie_PE_command"],
            #indexPrefix = config["<step_name>__bowtie_index_output_dir"]+"/index",
            #here we have index files like : first discard index.rev.?.ebwt to get only index.?.ebwt so we have two remove the ext twice
            indexPrefix = lambda w, input: os.path.splitext(os.path.splitext([x for x in input.index if not 'rev' in x][0])[0])[0],
            minins = config["<step_name>__bowtie_minins_PE"],
            maxins = config["<step_name>__bowtie_maxins_PE"],
            orientation = config["<step_name>__bowtie_orientation_PE"],
            mult_align_limit = config["<step_name>__bowtie_mult_align_limit"],
            best = "--best" if (config["<step_name>__bowtie_best"]) else "",
            strata = "--strata" if (config["<step_name>__bowtie_strata"]) else ""
        shell:
            "{params.command} "
            "--threads {threads} "
            "{params.indexPrefix} "
            "-I {params.minins} "
            "-X {params.maxins} "
            "{params.orientation} "
            "-m {params.mult_align_limit} "
            "{params.best} "
            "{params.strata} "
            "-S "#output in sam format
            "-1 {input.read} "
            "-2 {input.read2} 2> {log} "
            "| samtools view -b 2>> {log} "
            "| samtools addreplacerg -r 'ID:WAW\tSM:{wildcards.sample}\tPL:ILLUMINA\tPI:200\tLB:LIB-1' -O BAM - "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"

elif config["SeOrPe"] == "SE":

    rule <step_name>__bowtie_SE:
        input:
            **<step_name>__bowtie_SE_inputs()
        output:
             bam = config["results_dir"]+"/"+config["<step_name>__bowtie_SE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__bowtie_SE_output_dir"] + "/{sample}_bowtie_log.txt"
        threads:
            config["<step_name>__bowtie_threads"]
        params:
            command = config["<step_name>__bowtie_SE_command"],
            #indexPrefix = config["<step_name>__bowtie_index_output_dir"]+"/index",
            indexPrefix = lambda w, input: os.path.splitext(os.path.splitext([x for x in input.index if not 'rev' in x][0])[0])[0],
            mult_align_limit = config["<step_name>__bowtie_mult_align_limit"],
            best = "--best" if (config["<step_name>__bowtie_best"]) else "",
            strata = "--strata" if (config["<step_name>__bowtie_strata"]) else ""
        shell:
            "{params.command} "
            "--threads {threads} "
            "{params.indexPrefix} "
            "-m {params.mult_align_limit} "
            "{params.best} "
            "{params.strata} "
            "-S "#output in sam format
            "{input.read} 2> {log} "
            "| samtools view -b 2>> {log} "
            "| samtools addreplacerg -r 'ID:WAW\tSM:{wildcards.sample}\tPL:ILLUMINA\tPI:200\tLB:LIB-1' -O BAM - "
            "| samtools sort -@ {threads} > {output. bam} 2>> {log} &&"
            "samtools index -@ {threads} {output. bam} 2>> {log}"
