rule <step_name>__jmodeltest2:
    input:
        **<step_name>__jmodeltest2_inputs(),
    output: 
        model = config["results_dir"] + "/" + config["<step_name>__jmodeltest2_output_dir"] + "/jmodeltest_mqc.html",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__jmodeltest2_output_dir"]+ "/",
        command = config["<step_name>__jmodeltest2_command"],
        AIC = "-AIC" if config["<step_name>__jmodeltest2_AIC"] else "",
        AICc = "-AICc" if config["<step_name>__jmodeltest2_AICc"] else "",
        BIC = "-BIC" if config["<step_name>__jmodeltest2_BIC"] else "",
        DT = "-DT" if config["<step_name>__jmodeltest2_DT"] else "",
        confidence_interval = config["<step_name>__jmodeltest2_confidence_interval"],
        cat_number = config["<step_name>__jmodeltest2_cat_number"],
        hLRTs_confidence_interval = config["<step_name>__jmodeltest2_hLRTs_confidence_interval"],
        hLRT = "-hLRT" if config["<step_name>__jmodeltest2_hLRT"] else "",
        invar = "-i" if config["<step_name>__jmodeltest2_invar"] else "",
        #seed = config["<step_name>__jmodeltest2_seed"],#
        treetopsearch = config["<step_name>__jmodeltest2_treetopsearch"],
        basetree = config["<step_name>__jmodeltest2_basetree"],
        averaging = "-v" if config["<step_name>__jmodeltest2_averaging"] else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__jmodeltest2_output_dir"] + "/jmodeltest2_log.txt"
    threads: 
        config["<step_name>__jmodeltest2_threads"]
    shell:
        "{params.command} "
        "-d {input.sequence_file} "
        "{params.AIC} "
        "{params.AICc} "
        "{params.BIC} "
        "{params.DT} "
        "-c {params.confidence_interval} "
        "-g {params.cat_number} "
        "-h {params.hLRTs_confidence_interval} "
        "{params.hLRT} "
        "{params.invar} "
        #"-seed {params.seed} "#
        "-S {params.treetopsearch} "
        "-t {params.basetree} "
        "{params.averaging} "
        "-tr {threads} "
        "-o {output.model} "
        "|& tee {log}"
        "&& sed -i '1 i\<pre>' {output.model}"