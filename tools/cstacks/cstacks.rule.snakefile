rule <step_name>__cstacks:
    input:
        **<step_name>__cstacks_inputs(),
        popmap = config["<step_name>__cstacks_population_tsv"]
    output:
        tags = config["results_dir"]+"/"+config["<step_name>__cstacks_output_dir"]+"/catalog.tags.tsv.gz",
        snps = config["results_dir"]+"/"+config["<step_name>__cstacks_output_dir"]+"/catalog.snps.tsv.gz",
        alleles = config["results_dir"]+"/"+config["<step_name>__cstacks_output_dir"]+"/catalog.alleles.tsv.gz"
    params:
        command = config["<step_name>__cstacks_command"],
        n = config["<step_name>__cstacks_n"],
        input_dir = lambda w, input: os.path.dirname(input.tags[0]),
        output_dir = config["results_dir"]+"/"+config["<step_name>__cstacks_output_dir"],
    threads: 
        config["<step_name>__cstacks_threads"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__cstacks_output_dir"] + "/cstacks_log.txt"
    shell:
        "{params.command} "
        "-p {threads} "
        "-n {params.n} "
        "-P {params.input_dir} "
        "-M {input.popmap} "
        #"-o {params.output_dir} "
        "|& tee {log}; "
        "mv {params.input_dir}/catalog.tags.tsv.gz {output.tags} && "
        "mv {params.input_dir}/catalog.snps.tsv.gz {output.snps} && "
        "mv {params.input_dir}/catalog.alleles.tsv.gz {output.alleles} "