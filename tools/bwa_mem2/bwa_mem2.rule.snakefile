if config["SeOrPe"] == "PE":

    rule <step_name>__bwa_mem2_PE:
        input:
            **<step_name>__bwa_mem2_PE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__bwa_mem2_PE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__bwa_mem2_PE_output_dir"] + "/{sample}_bwa_mem2_log.txt"
        threads:
            config["<step_name>__bwa_mem2_threads"]
        params:
            command = config["<step_name>__bwa_mem2_PE_command"],
            #indexPrefix =  config["<step_name>__bwa_mem2_index_output_dir"]+"/index",
            #avoid index.bwt.2bit.64 index.bwt.8bit.32
            indexPrefix = lambda w, input: os.path.splitext(os.path.splitext([x for x in input.index if not 'bit' in x][0])[0])[0],
            quality0_multimapping = """| awk '{{pos = index("XA:Z",$0); if (pos>0) $5=0;print}}'""" if (config["<step_name>__bwa_mem2_quality0_multimapping"] == True) else ""
        shell:
            "{params.command} "
            "-t {threads} "
            "{params.indexPrefix} "
            "-R '@RG\\tID:WAW\\tSM:{wildcards.sample}' "
            "{input.read} "
            "{input.read2} 2> {log} "
            "{params.quality0_multimapping} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"


elif config["SeOrPe"] == "SE":

    rule <step_name>__bwa_mem2_SE:
        input:
            **<step_name>__bwa_mem2_SE_inputs()
        output:
            bam = config["results_dir"]+"/"+config["<step_name>__bwa_mem2_SE_output_dir"]+"/{sample}.bam"
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__bwa_mem2_SE_output_dir"] + "/{sample}_bwa_mem2_log.txt"
        threads:
            config["<step_name>__bwa_mem2_threads"]
        params:
            command = config["<step_name>__bwa_mem2_PE_command"],
            #indexPrefix =  config["<step_name>__bwa_mem2_index_output_dir"]+"/index",
            indexPrefix = lambda w, input: os.path.splitext(os.path.splitext([x for x in input.index if not 'bit' in x][0])[0])[0],
            quality0_multimapping = """| awk '{{pos = index("XA:Z",$0); if (pos>0) $5=0;print}}'""" if (config["<step_name>__bwa_mem2_quality0_multimapping"] == True) else ""
        shell:
            "{params.command} "
            "-t {threads} "
            "{params.indexPrefix} "
            "-R '@RG\\tID:WAW\\tSM:{wildcards.sample}' "
            "{input.read} 2> {log} "
            "{params.quality0_multimapping} "
            "| samtools view -b 2>> {log} "
            "| samtools sort -@ {threads} > {output.bam} 2>> {log} &&"
            "samtools index -@ {threads} {output.bam} 2>> {log}"
