import oyaml as yaml
import sys
import os
from collections import OrderedDict

config = dict()
with open(sys.argv[1], 'r') as paramfile:
    config = yaml.load(paramfile, Loader=yaml.FullLoader)

config = config["params"]

out ="""# description: 'moments results'
# section_name: 'moments_results'
# plot_type: 'html'
<pre>
"""

with open(os.path.join(config["results_dir"],config["<step_name>__moments_output_dir"],"moments-full.txt"), "r") as moments_full:
    out += ''.join(moments_full.readlines())

out += "</pre>"

with open(os.path.join(config["results_dir"],config["<step_name>__moments_output_dir"],"moments-full_mqc.txt"), "w") as outfile:
    outfile.write(out)