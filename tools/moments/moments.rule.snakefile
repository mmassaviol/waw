rule <step_name>__moments:
    input:
        **<step_name>__moments_inputs(),
    output: 
        full = config["results_dir"] + "/" + config["<step_name>__moments_output_dir"] + "/moments-full.txt",
        easy = config["results_dir"] + "/" + config["<step_name>__moments_output_dir"] + "/moments-easy.txt",
        modelsfs = config["results_dir"] + "/" + config["<step_name>__moments_output_dir"] + "/moments-modelsfs.txt",
        img = config["results_dir"] + "/" + config["<step_name>__moments_output_dir"] + "/moments_"+ config["<step_name>__moments_model_list"] +"_mqc.png",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__moments_output_dir"]+ "/",
        masked = "--masked" if config["<step_name>__moments_masked"] else "",
        folded = "--folded" if config["<step_name>__moments_folded"] else "",
        projected = "--projected" if config["<step_name>__moments_projected"] else "",
        #nameoutput = config["<step_name>__moments_nameoutput"],
        datatype = config["<step_name>__moments_datatype"],
        namepop1 = config["<step_name>__moments_namepop1"],
        namepop2 = config["<step_name>__moments_namepop2"],
        proj_sample = config["<step_name>__moments_proj_sample"],
        model_list = config["<step_name>__moments_model_list"],
        maxiterGlobal = config["<step_name>__moments_maxiterGlobal"],
        accept = config["<step_name>__moments_accept"],
        visit = config["<step_name>__moments_visit"],
        Tini = config["<step_name>__moments_Tini"],
        no_local_search = config["<step_name>__moments_no_local_search"],
        local_method = config["<step_name>__moments_local_method"],
        maxiterLocal = config["<step_name>__moments_maxiterLocal"],
        img_out = config["results_dir"] + "/" + config["<step_name>__moments_output_dir"] + "/moments_"+ config["<step_name>__moments_model_list"] +".png",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__moments_output_dir"] + "/moments_log.txt"
    threads: 1
    shell: 
        "python3 /sources/moments_sources/moments-wrapper/moments_inference_dualanneal.py "
        "-i {input.fs_file_name} "
        "{params.masked} "
        "{params.folded} "
        "{params.projected} "
        #"--nameoutput {params.nameoutput} "
        #"--fs_file_name {params.fs_file_name} "
        #"--pop_file_name {params.pop_file_name} "
        "--datatype {params.datatype} "
        "--namepop1 {params.namepop1} "
        "--namepop2 {params.namepop2} "
        "--proj_sample {params.proj_sample} "
        "--model_list {params.model_list} "
        "|& tee {log};"
        "mv {params.img_out} {output.img}"
