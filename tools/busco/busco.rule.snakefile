rule <step_name>__busco:
    input:
        **<step_name>__busco_inputs(),
    output: 
        log = config["results_dir"] + "/" + config["<step_name>__busco_output_dir"] + "/busco_res/logs/busco.log",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__busco_output_dir"],
        command = config["<step_name>__busco_command"],
        mode = config["<step_name>__busco_mode"],
        lineage = config["<step_name>__busco_lineage"] if "auto" in config["<step_name>__busco_lineage"] else "-l "+config["<step_name>__busco_lineage"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__busco_output_dir"] + "/busco_log.txt"
    threads: 
        config["<step_name>__busco_threads"] 
    shell:
        "cd {params.output_dir} &&"
        "{params.command} -f "
        "-i {input.fasta_seq} "
        "{params.lineage} "
        "--out_path {params.output_dir} "
        "--out busco_res "
        "-m {params.mode} "
        "|& tee {log}"
