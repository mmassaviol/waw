rule <step_name>__velvet:
    input:
        **<step_name>__velvet_inputs(),
    output: 
        contigs = config["results_dir"] + "/" + config["<step_name>__velvet_output_dir"] + "/{sample}/contigs.fa",
        stats = config["results_dir"] + "/" + config["<step_name>__velvet_output_dir"] + "/{sample}/stats.txt",
        LastGraph = config["results_dir"] + "/" + config["<step_name>__velvet_output_dir"] + "/{sample}/LastGraph",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__velvet_output_dir"]+ "/{sample}",
        command = config["<step_name>__velvet_command"],
        min_contig_lgth = config["<step_name>__velvet_min_contig_lgth"],
        hash_length = config["<step_name>__velvet_hash_length"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__velvet_output_dir"] + "/{sample}_velvet_log.txt"
    shell: 
        # VELVETH
        "{params.command}h "
        "{params.output_dir} "
        "{params.hash_length} "
        "-fastq "
        "{input.merged_reads} "
        "|& tee {log}; "
        # VELVETG
        "{params.command}g "
        "{params.output_dir} "
        "-min_contig_lgth {params.min_contig_lgth} "
        "|& tee -a {log}"
