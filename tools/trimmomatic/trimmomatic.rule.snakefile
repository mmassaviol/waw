if config["SeOrPe"] == "PE":

    rule <step_name>__trimmomatic_PE:
        input:
            **<step_name>__trimmomatic_PE_inputs()
        output:
            readFP = config["results_dir"]+"/"+config["<step_name>__trimmomatic_PE_output_dir"]+"/{sample}_paired_R1.fq.gz",
            readFU = config["results_dir"]+"/"+config["<step_name>__trimmomatic_PE_output_dir"]+"/{sample}_forward_unpaired.fq.gz",
            readRP = config["results_dir"]+"/"+config["<step_name>__trimmomatic_PE_output_dir"]+"/{sample}_paired_R2.fq.gz",
            readRU = config["results_dir"]+"/"+config["<step_name>__trimmomatic_PE_output_dir"]+"/{sample}_reverse_unpaired.fq.gz",
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__trimmomatic_PE_output_dir"] + '/{sample}_trimmomatic_log.txt'
        params:
            command = config["<step_name>__trimmomatic_PE_command"],
            qc_score = config["<step_name>__trimmomatic_qc_score"],
            ILLUMINACLIP = "ILLUMINACLIP:" + config["<step_name>__trimmomatic_fastaWithAdapters"] + ":" + config["<step_name>___PE"] if (config["<step_name>__trimmomatic_fastaWithAdapters"] != "") else "ILLUMINACLIP:/opt/biotools/Trimmomatic-0.38/adapters/" + config["<step_name>__trimmomatic_illuminaAdapters_PE"] +"PE-2.fa" + config["<step_name>__trimmomatic_illuminaclip_PE"],
            MINLEN = "MINLEN:" + str(config["<step_name>__trimmomatic_MINLEN"]),
            otherparams = config["<step_name>__trimmomatic_otherparams_PE"]
        threads:
            config["<step_name>__trimmomatic_threads"]
        shell:
            "{params.command} "
            "{params.qc_score} "
            "-threads {threads} "
            "{input.read} "
            "{input.read2} "
            "{output.readFP} "
            "{output.readFU} "
            "{output.readRP} "
            "{output.readRU} "
            "{params.ILLUMINACLIP} "
            "{params.MINLEN} "
            "{params.otherparams} "
            "|& tee {log}"


elif config["SeOrPe"] == "SE":

    rule <step_name>__trimmomatic_SE:
        input:
            **<step_name>__trimmomatic_SE_inputs()
        output:
            read = config["results_dir"]+"/"+config["<step_name>__trimmomatic_SE_output_dir"]+"/{sample}_trimmed.fq.gz",
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__trimmomatic_PE_output_dir"] + '/{sample}_trimmomatic_log.txt'
        params:
            command = config["<step_name>__trimmomatic_SE_command"],
            qc_score = config["<step_name>__trimmomatic_qc_score"],
            ILLUMINACLIP = "ILLUMINACLIP:" + config["<step_name>__trimmomatic_fastaWithAdapters"] + ":" + config["<step_name>__trimmomatic_illuminaclip_SE"] if (config["<step_name>__trimmomatic_fastaWithAdapters"] != "") else "ILLUMINACLIP:/opt/biotools/Trimmomatic-0.38/adapters/" + config["<step_name>__trimmomatic_illuminaAdapters_SE"]+ "SE.fa"  + config["<step_name>__trimmomatic_illuminaclip_SE"],
            MINLEN = "MINLEN:" + str(config["<step_name>__trimmomatic_MINLEN"]),
            otherparams = config["<step_name>__trimmomatic_otherparams_SE"]
        threads:
            config["<step_name>__trimmomatic_threads"]
        shell:
            "{params.command} "
            "{params.qc_score} "
            "-threads {threads} "
            "{input} "
            "{output} "
            "{params.ILLUMINACLIP} "
            "{params.MINLEN} "  
            "{params.otherparams} "
            "|& tee {log}"
