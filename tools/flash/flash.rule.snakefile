rule <step_name>__flash:
    input:
        **<step_name>__flash_inputs()
    output:
        extendedFrags = config["results_dir"] + "/" +  config["<step_name>__flash_output_dir"] + "/{sample}.extendedFrags.fastq",
        notCombined_1 = config["results_dir"] + "/" +  config["<step_name>__flash_output_dir"] + "/{sample}.notCombined_1.fastq",
        notCombined_2 = config["results_dir"] + "/" +  config["<step_name>__flash_output_dir"] + "/{sample}.notCombined_2.fastq",
        hist = config["results_dir"] + "/" +  config["<step_name>__flash_output_dir"] + "/{sample}.flash.hist",
        histogram = config["results_dir"] + "/" +  config["<step_name>__flash_output_dir"] + "/{sample}.histogram",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__flash_output_dir"],
        command = config["<step_name>__flash_command"],
        min_overlap = config["<step_name>__flash_min_overlap"],
        max_overlap = config["<step_name>__flash_max_overlap"],
        max_mismatch_density = config["<step_name>__flash_max_mismatch_density"],
        tmp_hist = config["results_dir"] + "/" +  config["<step_name>__flash_output_dir"] + "/{sample}.hist", # output from flash to be renamed for multiqc detection
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__flash_output_dir"] + "/{sample}_flash_log.txt"
    threads: config["<step_name>__flash_threads"]
    shell:
        "{params.command} "
        "-x {params.max_mismatch_density} "
        "-m {params.min_overlap} "
        "-M {params.max_overlap} "
        "-d {params.output_dir} "
        "-o {wildcards.sample} "
        "-t {threads} "
        "{input.read} "
        "{input.read2} "
        "|& tee {log}; "
        "mv {params.tmp_hist} {output.hist}" # with flash in hist name it is detected by multiqc
