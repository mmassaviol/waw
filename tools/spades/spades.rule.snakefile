import time

if config["SeOrPe"] == "SE":

    rule <step_name>__spades_SE:
        input:
            **<step_name>__spades_SE_inputs()
        output:
            contigs = config["results_dir"] + "/" + config["<step_name>__spades_SE_output_dir"] + "/contigs.fasta"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__spades_SE_output_dir"] + '/spades_log.txt'
        threads:
            config["<step_name>__spades_threads"]
        params:
            command = config["<step_name>__spades_SE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__spades_SE_output_dir"],
        run:
            readline = ""
            for r1 in input.read:
                readline += "-r "+r1+" "
            shell(
            "{params.command} "+
            readline+
            "-t {threads} "+
            "-o {params.output_dir} "
            #"--k-min 39 "+
            #"--k-max 79 "+
            )


elif config["SeOrPe"] == "PE":

    rule <step_name>__spades_PE:
        input:
            **<step_name>__spades_PE_inputs()
        output:
            contigs = config["results_dir"] + "/" + config["<step_name>__spades_PE_output_dir"] + "/contigs.fasta"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__spades_PE_output_dir"] + '/spades_log.txt'
        threads:
            config["<step_name>__spades_threads"]
        params:
            command = config["<step_name>__spades_PE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__spades_PE_output_dir"],
        run:
            readline = ""
            for r1,r2 in zip(input.read,input.read2):
                readline += "-1 "+r1+" -2 "+r2+" "
            shell(
            "{params.command} "+
            readline+
            "-t {threads} "+
            "-o {params.output_dir} "
            #"--k-min 39 "+
            #"--k-max 79 "+
            )
