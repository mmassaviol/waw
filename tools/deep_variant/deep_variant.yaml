{
  id: deep_variant,
  name: DeepVariant,
  article: 10.1038/nbt.4235,
  website: "https://github.com/google/deepvariant",
  git: "https://github.com/google/deepvariant",
  description: "DeepVariant is an analysis pipeline that uses a deep neural network to call genetic variants from next-generation DNA sequencing data.",
  version: "0.9.0",
  documentation: "https://github.com/google/deepvariant/blob/r0.9/docs/README.md",
  multiqc: "custom",
  commands:
    [
      {
        name: deep_variant,
        command: python /opt/deepvariant/bin/run_deepvariant.py,
        category: variant_calling,
        output_dir: deep_variant,
        inputs: [{ name: genome_fasta, type: "contigs" , description: "Fasta reference", from: "parameter"}, { name: bams, type: "bams" }],
        outputs: [
          { name: vcf, type: "vcf", file: "{sample}_output.vcf.gz", description: "Variant file" },
          { name: gvcf, type: "gvcf", file: "{sample}_output.g.vcf.gz", description: "Genomic variant file" },
          { name: html_report, type: "html", file: "{sample}_output.visual_report.html", description: "Visual html report" }
        ],
        options:
          [
            {
              name: deep_variant_genome_fasta,
              type: input_file,
              value: "",
              label: "Path to reference genome fasta file",
            },
            {
              name: deep_variant_threads,
              prefix: --threads,
              type: numeric,
              value: 4,
              min: 1,
              max: NA,
              step: 1,
              label: "Number of threads to use",
            },
            {
              name: "deep_variant_model_type",
              type: "radio",
              value: "wgs",
              choices: [wgs: wgs, wes: wes],
              label: "Model type",
            }
          ]
      },
    ],
  install: {
    mini-conda: [
      "cd /opt/biotools/bin && wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh",
      "bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/biotools/conda ",
      "ENV PATH $PATH:/opt/biotools/conda/bin" ,
      "echo 'export PATH=/opt/biotools/conda/bin:$PATH' >> /etc/environment"
    ],
    deepvariant: [
      "conda create  -y -c conda-forge -c bioconda  -n deepvariant deepvariant==1.1.0 "
    ],
    samtools: [
      "cd /opt/biotools",
      "wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2",
      "tar -xvjf samtools-1.9.tar.bz2",
      "cd samtools-1.9",
      "./configure && make",
      "cd ..",
      "mv samtools-1.9/samtools bin/samtools",
      "rm -r samtools-1.9 samtools-1.9.tar.bz2"
    ],
    mbb_mqc_plugin:
    [
      "cd /opt/biotools",
      "git clone https://gitlab.mbb.univ-montp2.fr/mmassaviol/mbb_mqc_plugin.git",
      "cd mbb_mqc_plugin",
      "python3 setup.py install"
    ]
  },
  citations:  {
    deepvariant: [
      "A universal SNP and small-indel variant caller using deep neural networks. Nature Biotechnology 36, 983-987 (2018). Ryan Poplin, Pi-Chuan Chang, David Alexander, Scott Schwartz, Thomas Colthurst, Alexander Ku, Dan Newburger, Jojo Dijamco, Nam Nguyen, Pegah T. Afshar, Sam S. Gross, Lizzie Dorfman, Cory Y. McLean, Mark A. DePristo, doi: https://doi.org/10.1038/nbt.4235"
    ],
    samtools: [
      "Heng Li, Bob Handsaker, Alec Wysoker, Tim Fennell, Jue Ruan, Nils Homer, Gabor Marth, Goncalo Abecasis, Richard Durbin, 1000 Genome Project Data Processing Subgroup, The Sequence Alignment/Map format and SAMtools, Bioinformatics, Volume 25, Issue 16, 15 August 2009, Pages 2078-2079, https://doi.org/10.1093/bioinformatics/btp352"
    ]
  }
}