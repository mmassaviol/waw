rule <step_name>__minimap2_overlap:
    input:
        **<step_name>__minimap2_overlap_inputs(),
        file_1 = config["<step_name>__minimap2_overlap_file_1"],
        file_2 = config["<step_name>__minimap2_overlap_file_2"],
    output: 
        reads_overlaps = config["results_dir"] + "/" + config["<step_name>__minimap2_overlap_output_dir"] + "/reads.paf.gz",
    params:
        command = config["<step_name>__minimap2_overlap_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__minimap2_overlap_output_dir"]+ "/",
        pacbio_oxfordNanopore = config["<step_name>__minimap2_overlap_pacbio_oxfordNanopore"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__minimap2_overlap_output_dir"] + "/minimap2_overlap_log.txt"
    threads: 
        config["<step_name>__minimap2_overlap_threads"]
    shell: 
        "{params.command} ava-{params.pacbio_oxfordNanopore} "
        "-t {threads} "
        "{input.file1} "
        "{input.file2} "
        "| gzip -1 > {output.reads_overlaps} "
        "|& tee {log}"
