rule <step_name>__gatk_haplotype_caller:
    input:
        **<step_name>__gatk_haplotype_caller_inputs()
    output:
        vcf = config["results_dir"]+"/"+config["<step_name>__gatk_haplotype_caller_output_dir"]+"/{sample}_variants.vcf.gz",
        stats = config["results_dir"]+"/"+config["<step_name>__gatk_haplotype_caller_output_dir"]+"/{sample}_vcf_stats.txt"
    threads:
        config["<step_name>__gatk_haplotype_caller_threads"]
    params:
        command = config["<step_name>__gatk_haplotype_caller_command"],
        #bams = list(map("-I {}".format, input.bams)),
        vcf_out = config["results_dir"]+"/"+config["<step_name>__gatk_haplotype_caller_output_dir"]+"/{sample}_variants.vcf",
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__gatk_haplotype_caller_output_dir"] + "/{sample}_gatk_haplotype_caller_log.txt"
    shell:
        "{params.command} "
        "-T HaplotypeCaller "
        "-R {input.genome_fasta} "
        #"{params.bams} "
        "-I {input.bams} "
        "-o {params.vcf_out} "
        "-nct {threads} "
        "|& tee {log} "
        "&& bcftools stats "
        "-F {input.genome_fasta} "
        "-s - "
        "{params.vcf_out} > {output.stats} "
        "|& tee -a {log} ; "
        "bgzip {params.vcf_out} && "
        "tabix -p vcf {output.vcf} "
