rule <step_name>__igv_visualize:
    input:
        **<step_name>__igv_visualize_inputs()
    output:
        done = config["results_dir"]+"/"+config["<step_name>__igv_visualize_output_dir"]+"/igv_visualize.done"
    log: config["results_dir"]+'/logs/' + config["<step_name>__igv_visualize_output_dir"] + '/igv_visualize_log.txt'
    threads: config["<step_name>__igv_visualize_threads"]
    params:
        command = config["<step_name>__igv_visualize_command"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__igv_visualize_output_dir"],
    shell:
        #plot IGV mapping res
        "cd {params.output_dir}; "
        "if [ -f /root/igv/prefs.properties ]; then sed -i 's|DEFAULT_GENOME_KEY=.*|DEFAULT_GENOME_KEY={input.genome}|' /root/igv/prefs.properties fi; "
        "cat <<-EOF > igv.batch\n"
        "new\n"
        "genome {input.genome}\n"
        "load {input.circos_bam}\n"
        "snapshotDirectory .\n"
        "expand\n"
        "maxPanelHeight 2500\n"
        "EOF\n"
        "for i in $(grep '>'  {input.genome} | sed 's/>//' | awk '{{print $1}}')\n"
        "do\n"
        "echo \"goto $i:1-17000\" >> igv.batch\n"
        "echo \"sort quality \" >> igv.batch\n"
        "name=$(echo $i | cut -f1 -d-)\n"
        "echo \"snapshot ${{name}}_mqc.png\" >> igv.batch\n"
        "done\n"
        "echo \"exit \" >> igv.batch\n"
        #"Xvfb :10 -nolisten tcp -fp /etc/X11/fonts/misc/ &\n"
        #"export DISPLAY=:10\n"
        "samtools index {input.circos_bam} -@ {threads}\n"
        "xvfb-run -s '-screen 0 1920x1080x24' {params.command} -b igv.batch ; "
        "touch {output.done}"