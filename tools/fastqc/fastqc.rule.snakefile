if config["SeOrPe"] == "SE":

    rule <step_name>__fastqc_SE:
        input:
            **<step_name>__fastqc_SE_inputs()
        output:
            #html = config["results_dir"]+'/'+config["<step_name>__fastqc_SE_output_dir"]+'/{sample}'+config["sample_suffix"].split(".")[0]+'_fastqc.html',
            #zip = config["results_dir"]+'/'+config["<step_name>__fastqc_SE_output_dir"]+'/{sample}'+config["sample_suffix"].split(".")[0]+'_fastqc.zip',
            logFile = config["results_dir"]+'/'+config["<step_name>__fastqc_SE_output_dir"] + '/{sample}_fastqc_log.txt'
        #log: config["results_dir"]+'/logs/' + config["<step_name>__fastqc_SE_output_dir"] + '/{sample}_fastqc_log.txt'
        threads: 2
        params:
            command = config["<step_name>__fastqc_SE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__fastqc_SE_output_dir"]
        shell:
            "{params.command} "
            "{input.read} "
            "-t {threads} "
            "-o {params.output_dir} "
            "--quiet "
            "|& tee {output.logFile}"

elif config["SeOrPe"] == "PE":

    rule <step_name>__fastqc_PE:
        input:
            **<step_name>__fastqc_PE_inputs()
        output:
        #PE_Mark is "_" or "_R" see raw_reads
            #html1 = config["results_dir"]+'/'+config["<step_name>__fastqc_PE_output_dir"]+'/{sample}'+PE_Mark +'1'+config["sample_suffix"].split(".")[0]+'_fastqc.html',
            #zip1 = config["results_dir"]+'/'+config["<step_name>__fastqc_PE_output_dir"]+'/{sample}'+PE_Mark +'1'+config["sample_suffix"].split(".")[0]+'_fastqc.zip',
            #html2 = config["results_dir"]+'/'+config["<step_name>__fastqc_PE_output_dir"]+'/{sample}'+PE_Mark +'2'+config["sample_suffix"].split(".")[0]+'_fastqc.html',
            #zip2 = config["results_dir"]+'/'+config["<step_name>__fastqc_PE_output_dir"]+'/{sample}'+PE_Mark +'2'+config["sample_suffix"].split(".")[0]+'_fastqc.zip',
            logFile = config["results_dir"]+'/'+config["<step_name>__fastqc_PE_output_dir"] + '/{sample}_fastqc_log.txt'
        #log: config["results_dir"]+'/logs/' + config["<step_name>__fastqc_PE_output_dir"] + '/{sample}_fastqc_log.txt'
        threads: 4
        params:
            command = config["<step_name>__fastqc_PE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__fastqc_PE_output_dir"]
        shell:
            "{params.command} "
            "{input.read} "
            "{input.read2} "
            "-t {threads} "
            "-o {params.output_dir} "
            "--quiet "
            "|& tee {output.logFile}"
