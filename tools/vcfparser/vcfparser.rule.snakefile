rule <step_name>__vcfparser:
    input:
        **<step_name>__vcfparser_inputs()
    output:
        vcf_stats = config["results_dir"]+"/"+config["<step_name>__vcfparser_output_dir"]+"/{sample}_<step_name>_vcfstats.txt",
        vcf_png = config["results_dir"]+"/"+config["<step_name>__vcfparser_output_dir"]+"/{sample}_<step_name>_vcf_plots_mqc.png",
    params:
        output_dir = config["results_dir"],
        command = config["<step_name>__vcfparser_command"],
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__vcfparser_output_dir"] + "/{sample}_<step_name>_vcfparser_log.txt"
    shell:
        "{params.command} "
        "{input.vcf} "
        "{output.vcf_stats} "
        "|& tee -a {log} ;"
        "fic={output.vcf_stats} ;" 
        "samplePos=1 ;"
        "fig={output.vcf_png};" 
        "scriptsDir=`dirname $(echo {params.command} | sed 's/bash //')`; "
        "Rscript -e \"source(paste0('$scriptsDir','/Draw_fonction_MultiSamples.R')); png(file='$fig', width = 1200, height = 1000); Draw('$fic', $samplePos, NULL); dev.off()\""
