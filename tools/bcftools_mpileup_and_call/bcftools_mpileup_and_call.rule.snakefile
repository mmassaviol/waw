rule <step_name>__bcftools_mpileup_and_call:
    input:
        **<step_name>__bcftools_mpileup_and_call_inputs()
    output:
        vcf = config["results_dir"]+"/"+config["<step_name>__bcftools_mpileup_and_call_output_dir"]+"/{sample}_variants.vcf.gz",
        stats = config["results_dir"]+"/"+config["<step_name>__bcftools_mpileup_and_call_output_dir"]+"/{sample}_vcf_stats.txt"
    params:
        command = config["<step_name>__bcftools_mpileup_and_call_command"],
        #bams = " ".join(input.bams)
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__bcftools_mpileup_and_call_output_dir"] + "/{sample}_bcftools_mpileup_and_call_log.txt"
    threads:
        config["<step_name>__bcftools_mpileup_and_call_threads"]
    shell:
        # prepare fasta index (transform gzip to bgzip then index)
        "if gzip -t '{input.genome_fasta}'; then "
        "mv {input.genome_fasta} tmp_genome.gz; "
        "zcat tmp_genome.gz | bgzip -c > {input.genome_fasta}; "
        "rm tmp_genome.gz; "
        "fi; "
        "if [ ! -f '{input.genome_fasta}.fai' ] ; then "
        "samtools faidx {input.genome_fasta}; "
        "fi; "

        "{params.command} mpileup "
        "-Ou " #uncompressed bcf
        "-a FORMAT/AD,FORMAT/DP "
        "-f {input.genome_fasta} "
        "--threads {threads} "
        #"{params.bams} "
        "{input.bam} "
        "| {params.command} call "
        "-vmO z "
        "-o {output.vcf} "
        "|& tee {log} "
        "&& tabix -p vcf {output.vcf} "
        "|& tee -a {log} "
        "&& {params.command} stats "
        "-F {input.genome_fasta} "
        "-s - "
        "{output.vcf} > {output.stats}"
        "|& tee -a {log} "