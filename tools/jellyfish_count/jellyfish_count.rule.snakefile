rule <step_name>__jellyfish_count:
    input:
        **<step_name>__jellyfish_count_inputs(),
    output:
        kmer_counts = config["results_dir"] + "/" + config["<step_name>__jellyfish_count_output_dir"] + "/counts.jf",
    params:
        command = config["<step_name>__jellyfish_count_command"],
        canonical_kmer = "-C" if config["<step_name>__jellyfish_count_canonical_kmer"] else "",
        kmer_len = config["<step_name>__jellyfish_count_kmer_len"],
        hash_size = config["<step_name>__jellyfish_count_hash_size"]
    threads:
        config["<step_name>__jellyfish_count_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__jellyfish_count_output_dir"] + '/jellyfish_count_log.txt'
    run:
        files = ""
        if (config["SeOrPe"] == "PE"):
            for r1,r2 in zip(input.read,input.read2):
                files += "<(zcat -f "+r1+") <(zcat -f "+r2+") "
        else:
            for r in input.read:
                files += "<(zcat -f "+r+") "
        shell(
        "{params.command} "+
        "{params.canonical_kmer} "+
        "-m {params.kmer_len} "+
        "-s {params.hash_size} "+
        "-t {threads} "+
        "-o {output.kmer_counts} "+
        files+
        "|& tee {log}"
        )
