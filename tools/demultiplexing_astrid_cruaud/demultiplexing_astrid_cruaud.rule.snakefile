
rule <step_name>__demultiplexing_astrid_cruaud:
    input:
        **<step_name>__demultiplexing_astrid_cruaud_inputs()
    output:
        demultiplexed = expand(config["results_dir"] + "/" + config["<step_name>__demultiplexing_astrid_cruaud_output_dir"] + "/{sample}_fq", sample=INDIVIDUS),
        stats = config["results_dir"] + "/" + config["<step_name>__demultiplexing_astrid_cruaud_output_dir"] + "/stat_demultiplexing_mqc.csv",
    params:
        output_dir = config["results_dir"] + "/" + config["<step_name>__demultiplexing_astrid_cruaud_output_dir"],
        barcodes = config["<step_name>__demultiplexing_astrid_cruaud_barcodes"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__demultiplexing_astrid_cruaud_output_dir"] + "/demultiplexing_astrid_cruaud_log.txt"
    threads: config["<step_name>__demultiplexing_astrid_cruaud_threads"]
    shell:
        "cd {params.output_dir} && "
        #"touch {output.demultiplexed} && "
        # tail to remove header (sample barcode_P1 barcode_P2 barcode_P2_rev term)
        "tail -n +2 {params.barcodes} | parallel --jobs {threads} --colsep '\\t' \"grep -B 1 --no-group-separator -i '^{{2}}.*{{4}}$' {input.extended_frags} > {{1}}'_fq'\" |& tee {log} ; " # test if --no-group-separator works with the next parts of the workflow
        # nb reads/sample :
        "echo -e \"# description: 'Number of reads for each individu after demultiplexing'\\n# section_name: 'Demultiplexing stats'\\n# plot_type: 'table'\\n# pconfig:\\n#  format: '{{:,.0f}}'\\nIndividu\\tNumber of reads\" > {output.stats}; "
        "for i in $(ls *_fq); do echo -e $i'\\t'$(grep -ic '@' $i); done >> {output.stats} 2>> {log}; " # count with grep @ because no risk of @ in quality sequence as it is no more present in the file
        # remove barcodes from beginning / end of reads
        "tail -n +2 {params.barcodes} | parallel --jobs {threads} --colsep '\\t' \"sed -i 's/^{{2}}//g;s/{{4}}$//g' {{1}}'_fq'\" |& tee -a {log}"
