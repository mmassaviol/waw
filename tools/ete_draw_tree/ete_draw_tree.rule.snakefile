rule <step_name>__ete_draw_tree:
    input:
        **<step_name>__ete_draw_tree_inputs(),
    output: 
        tree_image = config["results_dir"] + "/" + config["<step_name>__ete_draw_tree_output_dir"] + "/Tree_visualisation_mqc.png",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__ete_draw_tree_output_dir"]+ "/",
        command = config["<step_name>__ete_draw_tree_command"],
        alg = "--alg "+<step_name>__ete_draw_tree_inputs()["align_file"] if "align_file" in <step_name>__ete_draw_tree_inputs().keys() else "",
        alg_type = "--alg_type compactseq " if "align_file" in <step_name>__ete_draw_tree_inputs().keys() else ""
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__ete_draw_tree_output_dir"] + "/ete_draw_tree_log.txt"
    shell: 
        "{params.command} "
        "-t {input.tree_file} "
        "{params.alg} "
        "{params.alg_type} "
        "--ss "
        "--image {output.tree_image} "
        "|& tee {log}"
