if config["SeOrPe"] == "SE":

    rule <step_name>__process_radtags_SE:
        input:
            **<step_name>__process_radtags_inputs(),
            barcode_file = config["<step_name>__process_radtags_barcode_file"]
        output:
            temp(expand(config["results_dir"]+"/"+config["<step_name>__process_radtags_SE_output_dir"]+"/{{sample}}"+"/{id}"+config["sample_suffix"],id=individus)) # fastq par individu pour un sample
        params:
            command = config["<step_name>__process_radtags_SE_command"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__process_radtags_SE_output_dir"]+"/{sample}",
            reads_dir = lambda w, input: os.path.dirname(input.read[0]),
            barcode_type = config["<step_name>__process_radtags_barcode_type"],
            enzyme_SE = config["<step_name>__process_radtags_enzyme_SE"]
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__process_radtags_SE_output_dir"] + "/process_radtags_{sample}_log.txt"
        shell:
            "{params.command} "
            "-f {input.read} "
            "-b {input.barcode_file} "
            "-o {params.output_dir} "
            "{params.barcode_type} "
            "-e {params.enzyme_SE} "
            "--clean "
            #"--quality "
            "--rescue "
            "|& tee {log}"

    rule <step_name>__process_radtags_SE_merge:
        input:
            read = expand(config["results_dir"]+"/"+config["<step_name>__process_radtags_SE_output_dir"]+"/{sample}/{{id}}"+config["sample_suffix"],sample=SAMPLES)
        output:
            read = config["results_dir"]+"/"+config["<step_name>__process_radtags_SE_output_dir"]+"/{id}"+config["sample_suffix"]
        run:
            fqs = ' '.join(input.read)
            out = "{output.read}"
            shell("cat "+fqs+" > "+out)


elif config["SeOrPe"] == "PE":

    rule <step_name>__process_radtags_PE:
        input:
            **<step_name>__process_radtags_inputs(),
            barcode_file = config["<step_name>__process_radtags_barcode_file"]
        output:
            temp(expand(config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"]+"/{{sample}}"+"/{id}.1.fq.gz",id=individus)), # fastq par individu pour un sample
            temp(expand(config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"]+"/{{sample}}"+"/{id}.2.fq.gz",id=individus)) # fastq par individu pour un sample
        params:
            command = config["<step_name>__process_radtags_PE_command"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"]+"/{sample}",
            reads_dir = lambda w, input: os.path.dirname(input.read[0]),
            barcode_type = config["<step_name>__process_radtags_barcode_type"],
            enzyme_1_PE = "--renz_1 " + config["<step_name>__process_radtags_enzyme_1_PE"],
            enzyme_2_PE = "" if (config["<step_name>__process_radtags_enzyme_2_PE"] == "") else "--renz_2 " + config["<step_name>__process_radtags_enzyme_2_PE"]
        log:
            config["results_dir"]+"/logs/" + config["<step_name>__process_radtags_PE_output_dir"] + "/process_radtags_{sample}_log.txt"
        shell:
            "{params.command} "
            "-1 {input.read} "
            "-2 {input.read2} "
            "-b {input.barcode_file} "
            "-o {params.output_dir} "
            "{params.barcode_type} "
            "{params.enzyme_1_PE} "
            "{params.enzyme_2_PE} "
            "--clean "
            #"--quality "
            "--rescue "
            "|& tee {log}"

    rule <step_name>__process_radtags_PE_merge:
        input:
            read = expand(config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"]+"/{sample}/{{id}}.1.fq.gz",sample=SAMPLES),
            read2 = expand(config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"]+"/{sample}/{{id}}.2.fq.gz",sample=SAMPLES)
        output:
            read = config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"]+"/{id}_R1"+config["sample_suffix"],
            read2 = config["results_dir"]+"/"+config["<step_name>__process_radtags_PE_output_dir"]+"/{id}_R2"+config["sample_suffix"]
        run:
            fq1 = ' '.join(input.read)
            fq2 = ' '.join(input.read2)
            out1 = "{output.read}"
            out2 = "{output.read2}"
            shell("cat "+fq1+" > "+out1)
            shell("cat "+fq2+" > "+out2)
