rule <step_name>__interop:
    input:
        **<step_name>__interop_inputs()
    output:
        summary = config["results_dir"]+"/"+config["<step_name>__interop_output_dir"]+"/summary.csv",
        index_summary = config["results_dir"]+"/"+config["<step_name>__interop_output_dir"]+"/index_summary.csv",
        dump_text = config["results_dir"]+"/"+config["<step_name>__interop_output_dir"]+"/dump_text.csv",
    log: config["results_dir"]+'/logs/' + config["<step_name>__interop_output_dir"] + '/interop_log.txt'
    params:
        #illumina_dir = config["<step_name>__interop_illumina_dir"],
        output_dir = config["results_dir"]+"/"+config["<step_name>__interop_output_dir"],
    shell: 
        "cd {params.output_dir}; "
        "summary {input.illumina_dir} | sed 's/ *,/,/g' > summary.csv " #remove unneeded whitespaces
        "&& index-summary {input.illumina_dir} | sed 's/ \{{2,\}}/,/g' | sed 's/ CV/,CV/' | sed 's/^ //g' | sed 's/,$//g' > index_summary.csv " # format as a csv for multiqc
        "&& dumptext {input.illumina_dir} > dump_text.csv "
        "&& imaging_table {input.illumina_dir} | sed 's/;/,/g' > imaging_table.csv "
        "&& plot_qscore_heatmap {input.illumina_dir} | sed \"s/set output '.*'/set output 'Qscore_heatmap_mqc.png'/\" | gnuplot "
        "&& plot_qscore_histogram {input.illumina_dir} | sed \"s/set output '.*'/set output 'Qscore_histogram_mqc.png'/\" | gnuplot "
        "&& plot_by_cycle {input.illumina_dir} | sed \"s/set output '.*'/set output 'Intensity_by_cycle_mqc.png'/\" | gnuplot "
        "&& plot_by_lane {input.illumina_dir} | sed \"s/set output '.*'/set output 'Cluster_count_by_lane_mqc.png'/\" | gnuplot "
        "&& plot_flowcell {input.illumina_dir} | sed \"s/set output '.*'/set output 'Flowcell_intensity_mqc.png'/\" | gnuplot "
        "&& plot_sample_qc  {input.illumina_dir} | sed \"s/set output '.*'/set output 'Sample_qc_mqc.png'/\" | gnuplot "  
        