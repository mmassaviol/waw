rule <step_name>__bowtie2_index:
    input:
        **<step_name>__bowtie2_index_inputs()
    output:
        index = (
            expand(config["<step_name>__bowtie2_index_output_dir"]+"/index.{num}.bt2",num=[1,2,3,4]),
            expand(config["<step_name>__bowtie2_index_output_dir"]+"/index.rev.{num}.bt2",num=[1,2]),
        )
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__bowtie2_index_output_dir"] + "/index.log"
    threads:
        config["<step_name>__bowtie2_index_threads"]
    params:
        command = config["<step_name>__bowtie2_index_command"],
        output_prefix = config["<step_name>__bowtie2_index_output_dir"]+"/index"
    shell:
        "{params.command} "
        "{input.genome_fasta} "
        "{params.output_prefix} "
        "--threads {threads} "
        "|& tee {log}"
