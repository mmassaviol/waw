rule <step_name>__simul_coal:
    input:
    output: 
        msout = config["results_dir"] + "/" + config["<step_name>__simul_coal_output_dir"]+ "/msout"
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__simul_coal_output_dir"]+ "/",
        #argfile = config["<step_name>__simul_coal_argfile"],
        pop1 = config["<step_name>__simul_coal_pop1"],
        pop2 = config["<step_name>__simul_coal_pop2"],
        outgroup = config["<step_name>__simul_coal_outgroup"],
        samp1 = config["<step_name>__simul_coal_samp1"],
        samp2 = config["<step_name>__simul_coal_samp2"],
        nlocus = config["<step_name>__simul_coal_nlocus"],
        mlength = config["<step_name>__simul_coal_mlength"],
        sdlength = config["<step_name>__simul_coal_sdlength"],
        Nref = config["<step_name>__simul_coal_Nref"],
        mu = config["<step_name>__simul_coal_mu"],
        seed = config["<step_name>__simul_coal_seed"],
        model = config["<step_name>__simul_coal_model"],
        nreps = config["<step_name>__simul_coal_nreps"],
        spec_model = config["<step_name>__simul_coal_spec_model"],
        mig = config["<step_name>__simul_coal_mig"],
        dir = config["<step_name>__simul_coal_dir"],
        n1 = config["<step_name>__simul_coal_n1"],
        n2 = config["<step_name>__simul_coal_n2"],
        na = config["<step_name>__simul_coal_na"],
        tau = config["<step_name>__simul_coal_tau"],
        tausmall = config["<step_name>__simul_coal_tausmall"],
        nm1 = config["<step_name>__simul_coal_nm1"],
        nm2 = config["<step_name>__simul_coal_nm2"],
        prop = config["<step_name>__simul_coal_prop"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__simul_coal_output_dir"] + "/simul_coal_log.txt"
    threads: 1
    shell: 
        "cd {params.output_dir}; "
        "myNames='echo {params.pop1} {params.pop2} {params.outgroup}';"
        "python3 /sources/simul_coal_sources/tobpfile.py -c \"${{myNames}}\" "
        "-l {params.samp1} "
        "-L {params.samp2} "
        "-N {params.Nref} "
        "-M {params.mu} "
        "-n {params.nlocus} "
        "-m {params.mlength} "
        "-d {params.sdlength} |& tee {log}; "

        "/sources/simul_coal_sources/priorgen_linux "
        "--bpfile bpfile "
        #"--arg-file ${argfile} "
        "--prior prior "
        "--seed {params.seed} "
        "--model island "
        "--mig homo "
        "--dir asym "
        "--nreps 1 "
        "--n1 1 1 "
        "--n2 2 2 "
        "--na 3 3 "
        "--tau 6 6 "
        "--tausmall 0 0 "
        "--nm1 2 2 "
        "--nm2 0.2 0.2 "
        "--prop 0 0 "
        "| /sources/simul_coal_sources/msnsam_linux tbs {params.nlocus} "
        "-t tbs "
        "-r tbs tbs "
        "-I 2 tbs tbs 0 "
        "-m 1 2 tbs "
        "-m 2 1 tbs "
        "-n 1 tbs "
        "-n 2 tbs "
        "-ej tbs 2 1 "
        "-eN tbs tbs > {output.msout} || true;"

        ##### Modify msout so DaDi can extract the sample sizes
        "sleep 2;"
        "sed -i \"s/-I 2 tbs tbs/-I 2 {params.samp1} {params.samp2}/g\" {output.msout}"
