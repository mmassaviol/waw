rule <step_name>__abricate:
    input:
        **<step_name>__abricate_inputs()
    output: 
        tsv = config["results_dir"] + "/" + config["<step_name>__abricate_output_dir"] + "/output.tsv",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__abricate_output_dir"]+ "/",
        command = config["<step_name>__abricate_command"],
        database = config["<step_name>__abricate_database"],
        minid = config["<step_name>__abricate_minid"],
        mincov = config["<step_name>__abricate_mincov"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__abricate_output_dir"] + "/abricate_log.txt"
    threads: 
        config["<step_name>__abricate_threads"]
    shell: 
        "{params.command} "
        "{input.contigs} "
        "--db {params.database} "
        "--minid {params.minid} "
        "--mincov {params.mincov} "
        "{input.contigs} "
        "> {output.tsv} "
        "2> >(tee {log} >&2)"


