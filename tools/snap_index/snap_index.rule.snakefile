rule <step_name>__snap_index:
    input:
        **<step_name>__snap_index_inputs()
    output:
       index = (
            config["<step_name>__snap_index_output_dir"]+"/Genome",
            config["<step_name>__snap_index_output_dir"]+"/GenomeIndex",
            config["<step_name>__snap_index_output_dir"]+"/GenomeIndexHash",
            config["<step_name>__snap_index_output_dir"]+"/OverflowTable",
        ),
    threads:
            config["<step_name>__snap_index_threads"]    
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__snap_index_output_dir"] + "/index.log"
    params:
        command = config["<step_name>__snap_index_command"],
        snap_index_seed_size = config["<step_name>__snap_index_seed_size"],
        out_dir = config["<step_name>__snap_index_output_dir"]
    shell:
        "{params.command} "
        "{input.genome_fasta} "   
        "{params.out_dir} "     
        "-s  {params.snap_index_seed_size} "        
        "-t{threads} |& tee {log};" #no space after -t
       
