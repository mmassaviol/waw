rule <step_name>__transrate:
    input:
        **<step_name>__transrate_inputs(),
    output:
        qualityForward = config["results_dir"] + "/" + config["<step_name>__transrate_output_dir"]
    params:
        useReads = config["<step_name>__transrate_useReads"]
        output_dir = config["results_dir"]+"/"+config["<step_name>__transrate_output_dir"]
    threads:
        config["<step_name>__transrate_threads"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__transrate_output_dir"] + '/transrate_log.txt'
    run:
        if (config["<step_name>__transrate_useReads"]):
            shell("""
            transrate \
            --assembly {input.assembly} \
            --left {input.read} \
            --right {input.read2} \
            --output {params.output_dir} \
            --threads {threads} \
            |& tee > {log}
            """)
        else:
            shell("""
            transrate \
            --assembly {input.assembly} \
            --output {params.output_dir} \
            --threads {threads} \
            |& tee > {log}
            """)