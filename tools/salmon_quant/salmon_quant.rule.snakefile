if config["SeOrPe"] == "PE":

    rule <step_name>__salmon_quant_PE:
        input:
            **<step_name>__salmon_quant_PE_inputs()
        output:
            counts = config["results_dir"]+"/"+config["<step_name>__salmon_quant_SE_output_dir"]+"/{sample}/quant.sf",
            lib_counts = config["results_dir"]+"/"+config["<step_name>__salmon_quant_SE_output_dir"]+"/{sample}/lib_format_counts.json",
            run_info = config["results_dir"]+"/"+config["<step_name>__salmon_quant_SE_output_dir"]+"/{sample}/cmd_info.json"
        log: config["results_dir"]+'/logs/' + config["<step_name>__salmon_quant_SE_output_dir"] + '/{sample}_salmon_quant_log.txt'
        params:
            command = config["<step_name>__salmon_quant_PE_command"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__salmon_quant_PE_output_dir"]+'/{sample}',
            #index_dir = lambda w, input: os.path.dirname(input.index)
            index_dir = lambda w, input: os.path.dirname(input.index[0]),
        threads: config["<step_name>__salmon_quant_threads"]
        shell:
            "{params.command} "
            "-i {params.index_dir} "
            "-l A "
            "-1 {input.read} "
            "-2 {input.read2} "
            "-o {params.output_dir} "
            "-p {threads} |& tee {log}"


elif config["SeOrPe"] == "SE":

    rule <step_name>__salmon_quant_SE:
        input:
            **<step_name>__salmon_quant_SE_inputs()
        output:
            counts = config["results_dir"]+"/"+config["<step_name>__salmon_quant_SE_output_dir"]+"/{sample}/quant.sf",
            lib_counts = config["results_dir"]+"/"+config["<step_name>__salmon_quant_SE_output_dir"]+"/{sample}/lib_format_counts.json",
            run_info = config["results_dir"]+"/"+config["<step_name>__salmon_quant_SE_output_dir"]+"/{sample}/cmd_info.json"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__salmon_quant_SE_output_dir"] + '/{sample}_salmon_quant_log.txt'
        params:
            command = config["<step_name>__salmon_quant_PE_command"],
            output_dir = config["results_dir"]+"/"+config["<step_name>__salmon_quant_SE_output_dir"]+'/{sample}',
            index_dir = lambda w, input: os.path.dirname(input.index[0]),
        threads:
            config["<step_name>__salmon_quant_threads"]
        shell:
            "{params.command} "
            "-i {param.index_dir} "
            "-l A "
            "-r {input.read} "
            "-o {params.output_dir} "
            "-p {threads} |& tee {log}"
