rule <step_name>__medaka:
    input:
        **<step_name>__medaka_inputs(),
    output: 
        consensus_assembly = config["results_dir"] + "/" + config["<step_name>__medaka_output_dir"] + "/consensus.fasta",
    params:
        command = config["<step_name>__medaka_command"],
        output_dir = config["results_dir"] + "/" + config["<step_name>__medaka_output_dir"]+ "/",
        model = config["<step_name>__medaka_model"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__medaka_output_dir"] + "/medaka_log.txt"
    threads: 
        config["<step_name>__medaka_threads"]
    shell: 
        "{params.command} "
        "-i {input.reads} "
        "-d {input.assembly_fasta} "
        "-o {params.output_dir} "
        "-t {threads} "
        "-m {params.model} "
        "|& tee {log}"
