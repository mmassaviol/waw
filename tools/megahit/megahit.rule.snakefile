import time

if config["SeOrPe"] == "SE":

    rule <step_name>__megahit_SE:
        input:
            **<step_name>__megahit_SE_inputs()
        output:
            contigs = config["results_dir"] + "/" + config["<step_name>__megahit_SE_output_dir"] + "/assembly.contigs.fa"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__megahit_SE_output_dir"] + '/megahit_log.txt'
        threads:
            config["<step_name>__megahit_threads"]
        params:
            command = config["<step_name>__megahit_SE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__megahit_SE_output_dir"],
            min_contig_len = config["<step_name>__megahit_min_contig_len"],
            k_min = config["<step_name>__megahit_minKmer"],
            k_max = config["<step_name>__megahit_maxKmer"],
            k_step = config["<step_name>__megahit_Kstep"]
        run:
            readline = ""
            for r1 in input.read:
                readline += "-r "+r1+" "
            shell(
            "{params.command} "+
            readline+
            "-t {threads} "+
            "-o {params.output_dir}.tmp "+
            "--out-prefix assembly "+
            "--min-contig-len {params.min_contig_len} "+
            "--k-min {params.k_min} "+
            "--k-max {params.k_max} "+
            "--k-step {params.k_step} "+
            "; mv {params.output_dir}.tmp/* {params.output_dir}/ && rm -rf {params.output_dir}.tmp/ "+
            "|& tee {log}; "
            "tail -n 2 {params.output_dir}/assembly.log | head -n 1 > {params.output_dir}/stats.txt"
            )
            # prepare report assembly stats
            time.sleep(5) # wait for stat file
            with open(params.output_dir+'/stats.txt',"r") as stats_file:
                stats = stats_file.readline()

            matches = re.match(r"^[\d -:]*- (\d+) contigs, total (\d+) bp, min (\d+) bp, max (\d+) bp, avg (\d+) bp, N50 (\d+) bp$",stats)
            with open(params.output_dir+'/Assembly_stats_mqc.tsv',"w") as staTab:
                staTab.write("# id: 'assembly_stats'\n")
                staTab.write("# section_name: 'Megahit Assembly Stats'\n")
                staTab.write("# plot_type: 'table'\n")
                staTab.write("Stat\tValue\n")
                staTab.write("Nombre de contigs\t"+matches.group(1)+"\n")
                staTab.write("Nombre de pb total\t"+matches.group(2)+"\n")
                staTab.write("Min pb\t"+matches.group(3)+"\n")
                staTab.write("Max pb\t"+matches.group(4)+"\n")
                staTab.write("Moyenne pb\t"+matches.group(5)+"\n")
                staTab.write("N50\t"+matches.group(6))


elif config["SeOrPe"] == "PE":

    rule <step_name>__megahit_PE:
        input:
            **<step_name>__megahit_PE_inputs()
        output:
            contigs = config["results_dir"] + "/" + config["<step_name>__megahit_PE_output_dir"] + "/assembly.contigs.fa"
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__megahit_PE_output_dir"] + '/megahit_log.txt'
        threads:
            config["<step_name>__megahit_threads"]
        params:
            command = config["<step_name>__megahit_PE_command"],
            output_dir = config["results_dir"]+'/'+config["<step_name>__megahit_PE_output_dir"],
            min_contig_len = config["<step_name>__megahit_min_contig_len"],
            k_min = config["<step_name>__megahit_minKmer"],
            k_max = config["<step_name>__megahit_maxKmer"],
            k_step = config["<step_name>__megahit_Kstep"]
        run:
            readline = ""
            for r1,r2 in zip(input.read,input.read2):
                readline += "-1 "+r1+" -2 "+r2+" "
            shell(
            "{params.command} "+
            readline+
            "-t {threads} "+
            "-o {params.output_dir}.tmp "+
            "--out-prefix assembly "+
            "--min-contig-len {params.min_contig_len} "+
            "--k-min {params.k_min} "+
            "--k-max {params.k_max} "+
            "--k-step {params.k_step} "+
            "; mv {params.output_dir}.tmp/* {params.output_dir}/ && rm -rf {params.output_dir}.tmp/ "+
            "|& tee {log}; "
            "tail -n 2 {params.output_dir}/assembly.log | head -n 1 > {params.output_dir}/stats.txt"
            )
            # prepare report assembly stats
            time.sleep(5) # wait for stat file
            with open(params.output_dir+'/stats.txt',"r") as stats_file:
                stats = stats_file.readline()

            matches = re.match(r"^[\d -:]*- (\d+) contigs, total (\d+) bp, min (\d+) bp, max (\d+) bp, avg (\d+) bp, N50 (\d+) bp$",stats)
            with open(params.output_dir+'/Assembly_stats_mqc.tsv',"w") as staTab:
                staTab.write("# id: 'assembly_stats'\n")
                staTab.write("# section_name: 'Megahit Assembly Stats'\n")
                staTab.write("# plot_type: 'table'\n")
                staTab.write("Stat\tValue\n")
                staTab.write("Nombre de contigs\t"+matches.group(1)+"\n")
                staTab.write("Nombre de pb total\t"+matches.group(2)+"\n")
                staTab.write("Min pb\t"+matches.group(3)+"\n")
                staTab.write("Max pb\t"+matches.group(4)+"\n")
                staTab.write("Moyenne pb\t"+matches.group(5)+"\n")
                staTab.write("N50\t"+matches.group(6))
