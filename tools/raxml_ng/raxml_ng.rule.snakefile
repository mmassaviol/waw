rule <step_name>__raxml_ng:
    input:
        **<step_name>__raxml_ng_inputs(),
    output: 
        best_tree = config["results_dir"] + "/" + config["<step_name>__raxml_ng_output_dir"] + "/run.raxml.bestTree",
        log = config["results_dir"] + "/" + config["<step_name>__raxml_ng_output_dir"] + "/run.raxml.log",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__raxml_ng_output_dir"]+ "/",
        command = config["<step_name>__raxml_ng_command"],
        model_name = config["<step_name>__raxml_ng_model_name"],
        gamma = "+G"+str(config["<step_name>__raxml_ng_gamma"]) if (config["<step_name>__raxml_ng_gamma"] > 0) else "",
        spr_radius = "" if (config["<step_name>__raxml_ng_spr_radius"] == "") else "--spr-radius "+config["<step_name>__raxml_ng_spr_radius"],
        spr_cutoff = config["<step_name>__raxml_ng_spr_cutoff"],
        prefix = config["results_dir"] + "/" + config["<step_name>__raxml_ng_output_dir"]+ "/run",
        seed = config["<step_name>__raxml_ng_seed"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__raxml_ng_output_dir"] + "/raxml_ng_log.txt"
    threads: 
        config["<step_name>__raxml_ng_threads"]
    shell: 
        "{params.command} "
        "--redo "
        "--msa {input.aligned_seqs} "
        "--model {params.model_name}{params.gamma} "
        "{params.spr_radius} "
        "--spr-cutoff {params.spr_cutoff} "
        "--prefix {params.prefix} "
        "--threads {threads} "
        "--seed {params.seed} "
        "|& tee {log}"
