def <step_name>__trimal_output_suffix():
    if config["<step_name>__trimal_out_format"] == "":
        return os.path.splitext(<step_name>__trimal_inputs()["sequences_in"])[1]
    if config["<step_name>__trimal_out_format"] == "--fasta":
        return ".fa"
    if config["<step_name>__trimal_out_format"] == "--phylip":
        return ".phylip"

rule <step_name>__trimal:
    input:
        **<step_name>__trimal_inputs(),
    output: 
        clean_seqs = config["results_dir"] + "/" + config["<step_name>__trimal_output_dir"] + "/out" + <step_name>__trimal_output_suffix(),
        report = config["results_dir"] + "/" + config["<step_name>__trimal_output_dir"] + "/trimAl_report_mqc.html",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__trimal_output_dir"]+ "/",
        command = config["<step_name>__trimal_command"],
        out_format = config["<step_name>__trimal_out_format"],
        ignorestopcodon = "-ignorestopcodon" if config["<step_name>__trimal_ignorestopcodon"] else "",
        splitbystopcodon = "-splitbystopcodon" if config["<step_name>__trimal_splitbystopcodon"] else "",
        colnumbering = "-colnumbering" if config["<step_name>__trimal_colnumbering"] else "",
        gapthreshold = config["<step_name>__trimal_gapthreshold"],
        simthreshold = config["<step_name>__trimal_simthreshold"],
        cons = config["<step_name>__trimal_cons"],
        nogaps = "-nogaps" if config["<step_name>__trimal_nogaps"] else "",
        noallgaps = "-noallgaps" if config["<step_name>__trimal_noallgaps"] else "",
        keepseqs = "-keepseqs" if config["<step_name>__trimal_keepseqs"] else "",
        block = "-block " + str(config["<step_name>__trimal_block"]) if (config["<step_name>__trimal_block"] > 0) else "",
        resoverlap = "-resoverlap " + str(config["<step_name>__trimal_resoverlap"]) if(config["<step_name>__trimal_resoverlap"] > 0) else "",
        seqoverlap = "-seqoverlap " + str(config["<step_name>__trimal_seqoverlap"]) if (config["<step_name>__trimal_seqoverlap"] > 0) else "",
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__trimal_output_dir"] + "/trimal_log.txt"
    shell: 
        "{params.command} "
        "-in {input.sequences_in} "
        "{params.out_format} "
        "-out {output.clean_seqs} "
        "-htmlout {output.report} "
        "{params.ignorestopcodon} "
        "{params.splitbystopcodon} "
        "{params.colnumbering} "
        "-gapthreshold {params.gapthreshold} "
        "-simthreshold {params.simthreshold} "
        "-cons {params.cons} "
        "{params.nogaps} "
        "{params.noallgaps} "
        "{params.keepseqs} "
        "{params.block} "
        "{params.resoverlap} "
        " {params.seqoverlap} "
        "|& tee {log}"
