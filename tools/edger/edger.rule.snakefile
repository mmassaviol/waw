rule <step_name>__edger:
    input:
        **<step_name>__edger_inputs()
    output:
        de_table = config["results_dir"]+'/'+config["<step_name>__edger_output_dir"]+'/de_table.csv',
        r_data = config["results_dir"]+'/'+config["<step_name>__edger_output_dir"]+'/data.RData',
        PCA = config["results_dir"]+'/'+config["<step_name>__edger_output_dir"]+'/PCA_mqc.png',
        MD = config["results_dir"]+'/'+config["<step_name>__edger_output_dir"]+'/MD_mqc.png',
        Top_genes = config["results_dir"]+'/'+config["<step_name>__edger_output_dir"]+'/Top_genes_mqc.tsv',
        Volcano_plot = config["results_dir"]+'/'+config["<step_name>__edger_output_dir"]+'/Volcano_plot_mqc.png',
        Heatmap = config["results_dir"]+'/'+config["<step_name>__edger_output_dir"]+'/Heatmap_mqc.png',
    log: config["results_dir"]+'/logs/' + config["<step_name>__edger_output_dir"] + '/edger_log.txt'
    params:
        script = config["<step_name>__edger_command"],
        x2gene = config["<step_name>__edger_tx2gene"],
        annotations = config["<step_name>__edger_annotations"],
        normfact = config["<step_name>__edger_normfact"],
        dispersion = config["<step_name>__edger_dispersion"],
        testType = config["<step_name>__edger_testType"],
    script:
        config["<step_name>__edger_script"]