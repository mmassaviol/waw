rule <step_name>__gatk_IndelRealigner_target_creator:
    input:
        **<step_name>__gatk_IndelRealigner_inputs()
    output:
        target_intervals = config["results_dir"]+"/"+config["<step_name>__gatk_IndelRealigner_output_dir"]+"/{sample}_forIndelRealigner.intervals"
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__gatk_IndelRealigner_output_dir"] + "/{sample}_gatk_RealignerTargetCreator_log.txt"
    params:
        command = config["<step_name>__gatk_IndelRealigner_command"]
    shell:
        "{params.command} "
        "-T RealignerTargetCreator "
        "-R {input.genome_fasta} "
        "-I {input.bam} "
        "-o {output.target_intervals} "
        "|& tee {log}"

rule <step_name>__gatk_IndelRealigner:
    input:
        **<step_name>__gatk_IndelRealigner_inputs(),
        target_intervals = config["results_dir"]+"/"+config["<step_name>__gatk_IndelRealigner_output_dir"]+"/{sample}_forIndelRealigner.intervals",
    output:
        bam = temp(config["results_dir"]+"/"+config["<step_name>__gatk_IndelRealigner_output_dir"]+"/{sample}.mapped.dedup.realigned.bam"),
        sorted_bam = config["results_dir"]+"/"+config["<step_name>__gatk_IndelRealigner_output_dir"]+"/{sample}.mapped.dedup.realigned.sorted.bam",
        bai = config["results_dir"]+"/"+config["<step_name>__gatk_IndelRealigner_output_dir"]+"/{sample}.mapped.dedup.realigned.sorted.bam.bai"
    params:
        command = config["<step_name>__gatk_IndelRealigner_command"],
        m_samtools_sort = config["<step_name>__gatk_IndelRealigner_samtools_memory"]
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__gatk_IndelRealigner_output_dir"] + "/{sample}_gatk_IndelRealigner_log.txt"
    threads:
        config["<step_name>__gatk_IndelRealigner_threads"]
    shell:
        "{params.command} "
        "-T IndelRealigner "
        "-R {input.genome_fasta} "
        "-I {input.bam} "
        "-targetIntervals {input.target_intervals} "
        "-o {output.bam} "
        #"-nct {threads} "
        "|& tee {log} "
        # Sorting BAM
        "&& samtools sort "
        "-m {params.m_samtools_sort}G "
        "-O BAM "
        "-l 1 "
        "-@ {threads} "
        "-o {output.sorted_bam} "
        "{output.bam} "
        "|& tee -a {log} "
        # Indexing BAM
        "&& samtools index "
        "-@ {threads} "
        "{output.sorted_bam} "
        "|& tee -a {log}"
