rule <step_name>__blastn_ncbi:
    input:
        **<step_name>__blastn_ncbi_inputs(),
    output:
        blastout = config["results_dir"] + "/" + config["<step_name>__blastn_ncbi_output_dir"] + "/blastout_" + config["<step_name>__blastn_ncbi_remote_db"] + ".tsv",
        selected_contigs = config["results_dir"] + "/" + config["<step_name>__blastn_ncbi_output_dir"] + "/selected_contigs.fasta",
    params:
        command = config["<step_name>__blastn_ncbi_command"],
        evalue = config["<step_name>__blastn_ncbi_evalue"],
        max_target_sequences =  config["<step_name>__blastn_ncbi_max_target_sequences"],
        max_hsps =  config["<step_name>__blastn_ncbi_max_hsps"],
        min_len = config["<step_name>__blastn_ncbi_min_len"],
        db = config["<step_name>__blastn_ncbi_remote_db"]
    log:
        config["results_dir"]+'/logs/' + config["<step_name>__blastn_ncbi_output_dir"] + '/blastn_ncbi_log.txt'
    shell:
        # output header
        "echo -e 'qseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\tqlen' > {output.blastout}; "
        # command
        "{params.command} "
        "-query {input.query} "
        "-remote "
        "-db {params.db} "
        "-evalue {params.evalue} "
        "-max_target_seqs {params.max_target_sequences} "
        "-max_hsps {params.max_hsps} "
        "-outfmt '6 std qlen' "
        "2> {log} | "
        "awk \"{{if (\$4 >= {params.min_len}) print }}\""
        ">> {output.blastout} ;"

        "if [ $(wc -l < {output.blastout} ) -ge 2 ]; then "
            "head -n +2 {output.blastout} | cut -f 1 > /tmp/qids.txt "
            "&& grep --no-group-separator -f /tmp/qids.txt -A 1 {input.query} > {output.selected_contigs};"
        "else "
        "touch {output.selected_contigs};"
        "fi"
