rule <step_name>__guppy:
        input:
            **<step_name>__guppy_inputs()
        output:
           sequencing_summary =   config["results_dir"] + "/" + config["<step_name>__guppy_output_dir"]+ "/sequencing_summary.txt" , 
           readPass = config["results_dir"] + "/" + config["<step_name>__guppy_output_dir"]+ "/guppy_pass.fq.gz",
           readFail = config["results_dir"] + "/" + config["<step_name>__guppy_output_dir"]+ "/guppy_fail.fq.gz",
        params:
            output_dir = config["results_dir"] + "/" + config["<step_name>__guppy_output_dir"]+ "/",
            command = config["<step_name>__guppy_command"],
            gpu_num_callers =  config["<step_name>__guppy_gpu_num_callers"],
            gpu_runners_per_device =  config["<step_name>__guppy_gpu_runners_per_device"],
            config_file = config["<step_name>__guppy_config_file"],
        log:
            config["results_dir"]+'/logs/' + config["<step_name>__guppy_output_dir"] + '/guppy_log.txt',
        shell:
            "nbGPUs=$(nvidia-smi --query-gpu=count --format=csv,noheader); echo found $nbGPUs GPU cards; "
            "{params.command} "+ # Attention, guppy demande un dossier de fast5 et pas une liste de fast5
            "--compress_fastq " +
            "-x 'auto' " + # -x "cuda:0"
            "-i {input.fast5_dir} "+
            "-s {params.output_dir} "+
            "-c /opt/biotools/ont-guppy/data/{params.config_file} " +
            "--num_callers {params.gpu_num_callers} " +
            "--gpu_runners_per_device {params.gpu_runners_per_device} ; "
            "cat {params.output_dir}/pass/*.fastq.gz > {params.output_dir}/guppy_pass.fq.gz ; "
            "rm -f {params.output_dir}/pass/*.fastq.gz; "
            "cat {params.output_dir}/fail/*.fastq.gz > {params.output_dir}/guppy_fail.fq.gz ; "            
            "rm -f  {params.output_dir}/fail/*.fastq.gz; "
            "mkdir {params.output_dir}/logs && mv {params.output_dir}/*.log {params.output_dir}/logs"

