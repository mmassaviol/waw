rule <step_name>__abyss_PE:
	input:
		**<step_name>__abyss_PE_inputs(),
	output: 
		contigs = config["results_dir"] + "/" + config["<step_name>__abyss_PE_output_dir"] + "/out-contigs.fa",
		mqc_stats = config["results_dir"] + "/" + config["<step_name>__abyss_PE_output_dir"] + "/ABySS_stats_mqc.csv",
	params: 
		output_dir = config["results_dir"] + "/" + config["<step_name>__abyss_PE_output_dir"]+ "/",
		command = config["<step_name>__abyss_PE_command"],
		k = config["<step_name>__abyss_PE_k"],
		l = config["<step_name>__abyss_PE_l"],
		q = config["<step_name>__abyss_PE_q"],
		n = config["<step_name>__abyss_PE_n"],
		other_options = config["<step_name>__abyss_PE_other_options"],
		in_line = lambda w, input: " ".join([" ".join([input.read[i],input.read2[i]]) for i in range(0,len(input.read))])
	log: 
		config["results_dir"] + "/logs/" + config["<step_name>__abyss_PE_output_dir"] + "/abyss_PE_log.txt"
	threads: 
		config["<step_name>__abyss_PE_threads"]
	shell: 
		"{params.command} "
		"--directory {params.output_dir} "
		"name='out' "
		"in='{params.in_line}' "
		"k={params.k} "
		"l={params.l} "
		"q={params.q} "
		"n={params.n} "
		"|& tee {log};"
		"echo \"# plot_type: 'table'\" > {output.mqc_stats}; "
		"awk ' BEGIN {{ FS = \",\" ;OFS = \"\\t\" }}{{ t=$1; $1=$11; $11=t; print }}' {params.output_dir}/out-stats.csv >> {output.mqc_stats}"
