rule <step_name>__muscle:
    input:
        **<step_name>__muscle_inputs(),
    output: 
        aligned_fasta = config["results_dir"] + "/" + config["<step_name>__muscle_output_dir"] + "/output.afa",
    params: 
        output_dir = config["results_dir"] + "/" + config["<step_name>__muscle_output_dir"]+ "/",
        command = config["<step_name>__muscle_command"],
    log: 
        config["results_dir"] + "/logs/" + config["<step_name>__muscle_output_dir"] + "/muscle_log.txt"
    shell: 
        "{params.command} "
        "-in {input.fasta} "
        "-out {output.aligned_fasta} "
        "-log {log} "
