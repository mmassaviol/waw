rule <step_name>__bwa_mem2_index:
    input:
        **<step_name>__bwa_mem2_index_inputs()
    output:
        index = (
            config["<step_name>__bwa_mem2_index_output_dir"]+"/index.0123",
            config["<step_name>__bwa_mem2_index_output_dir"]+"/index.amb",
            config["<step_name>__bwa_mem2_index_output_dir"]+"/index.ann",
            config["<step_name>__bwa_mem2_index_output_dir"]+"/index.bwt.2bit.64",
            config["<step_name>__bwa_mem2_index_output_dir"]+"/index.bwt.8bit.32",
            config["<step_name>__bwa_mem2_index_output_dir"]+"/index.pac"
        )
    log:
        config["results_dir"]+"/logs/" + config["<step_name>__bwa_mem2_index_output_dir"] + "/index.log"
    params:
        command = config["<step_name>__bwa_mem2_index_command"],
        output_prefix = config["<step_name>__bwa_mem2_index_output_dir"]+"/index"
    shell:
        "{params.command} "
        "-p {params.output_prefix} "
        "{input.genome_fasta} |& tee {log} "
